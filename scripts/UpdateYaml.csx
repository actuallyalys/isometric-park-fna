
#load "Shared.csx"


var image_meta = LoadImageMeta();

var images = new HashSet<String>(Directory.GetFiles("isometric-park-fna/Content/Portraits/"));

var image_meta_names = image_meta.Select((item) => "isometric-park-fna/Content/Portraits/" + item.Filename);
var missing_images = images.Except(image_meta_names);

foreach (String name in missing_images) 
{
    Console.WriteLine(String.Format("- Description: TODO\n  Subject: TODO\n  Source: TODO\n  Filename: {0}\n  Subjects: TODO\n", name));
}

