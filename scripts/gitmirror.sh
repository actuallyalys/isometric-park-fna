#!/bin/bash

NEWDIR=`mktemp --directory --suffix .gitmirror`

hg convert . $NEWDIR --filemap gitfiles.map

cd $NEWDIR

BRANCHES=`hg branches`

# for BRANCH in ${BRANCHES[@]}; do
# 	if [[ ! "$BRANCH" =~ ^([0-9]+:)|(inactive)|(closed) ]]; then          
# 		echo $BRANCH;
# 		`hg bookmark -r "${BRANCH}" "${BRANCH}_bookmark"`
# 	fi
# done

hg bookmark -r default main

hg push https://gitlab.com/actuallyalys/isometric-park-fna.git


echo "run 'rm -r $NEWDIR' to clear directory."
echo "or 'rm -r /tmp/*.gitmirror' to clear all directories."

