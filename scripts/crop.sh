#!/bin/sh

#C/O facedetect documentation (with some help from shellcheck)

IMAGES="isometric-park-fna/Content/images/"

mkdir "$IMAGES/crops"

for file in $IMAGES/*.jpg; do
  name=$(basename "$file")
  i=0
  facedetect "$file" | while read -r x y w h; do
  x=$(( x - w))
  y=$(( y - h))
  w=$(( w * 3))
  h=$(( w * 4 / 3 ))
    convert "$file" -crop "${w}x${h}+${x}+${y}" "$IMAGES/crops/${name%.*}_${i}.${name##*.}"
    i=$((i + 1))
  done
done
