#r "YamlDotNet.dll"
using YamlDotNet.Serialization;

public enum OrganizationType
    {
        Unspecified,
        Family,
        LargeCorporation,
        Cooperative
    }

public struct ImageMetadata 
{
        public string Description;
        public string Subject;
        public string Source;
        public string URL;
        public string Filename;
        public OrganizationType[] OrganizationType;
        public int Subjects;
    }

public struct NewsItem
    {
        public string hed;
        public string contents;
        public string source;
        public Dictionary<string, string> variables;
    }

public List<ImageMetadata> LoadImageMeta() 
{

var sr = new StreamReader(@"isometric-park-fna/Content/portraits.yaml");

var input = new StringReader(sr.ReadToEnd());
var deserializer = new DeserializerBuilder()
    .Build();

    return deserializer.Deserialize<List<ImageMetadata>>(input);

}
