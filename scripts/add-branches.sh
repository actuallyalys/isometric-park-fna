#!/bin/bash

BRANCHES=`hg branches`
for BRANCH in ${BRANCHES[@]}; do
	if [[ ! "$BRANCH" =~ ^([0-9]+:)|(inactive)|(closed) ]]; then          
		echo $BRANCH;
		`hg bookmark -r "${BRANCH}" "${BRANCH}_bookmark"`
	fi
done
