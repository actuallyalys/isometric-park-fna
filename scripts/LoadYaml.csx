#r "YamlDotNet.dll"
#load "Shared.csx"


using YamlDotNet.Serialization;
using System.Linq;
using System.Collections.Generic;



Console.WriteLine("Hello World");
var sr = new StreamReader(@"isometric-park-fna/Content/news_items.yaml");
var input = new StringReader(sr.ReadToEnd());
var deserializer = new DeserializerBuilder()
    .Build();

var items = deserializer.Deserialize<List<NewsItem>>(input);

Console.WriteLine("news_items.yaml loaded.");


sr = new StreamReader(@"isometric-park-fna/Content/news_items_pregenerated.yaml");
input = new StringReader(sr.ReadToEnd());
deserializer = new DeserializerBuilder()
    .Build();

items = deserializer.Deserialize<List<NewsItem>>(input);


Console.WriteLine("news_items_pregenerated.yaml loaded.");


var image_meta = LoadImageMeta();

var images = new HashSet<String>(Directory.GetFiles("isometric-park-fna/Content/Portraits/"));

if (images.Count() != image_meta.Count()) {
    Console.WriteLine(String.Format("Image metadata ({0}) doesn't match images ({1})", image_meta.Count(), images.Count()));

    Console.WriteLine("Missing Images:");
    var image_meta_names = image_meta.Select((item) => "isometric-park-fna/Content/Portraits/" + item.Filename);
    var missing_images = images.Except(image_meta_names);

    foreach (String name in missing_images) 
    {
        Console.WriteLine(name);
    }
    System.Environment.Exit(1);
}



Console.WriteLine(String.Format("Items ({0}) portraits.yaml loaded.", image_meta.Count()));
