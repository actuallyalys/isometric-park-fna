

CORE_SOLUTION = ./isometric-park-fna/isometric-park-fna-core.csproj #./isometric-park-fna-core.sln
FRAMEWORK_SOLUTION = ./isometric-park-fna.sln

DEFAULT_RUN = framework-debug

##

#Update nuget with nuget update -self
packages:
	nuget restore isometric-park-fna/packages.config -PackagesDirectory packages/

#BUILD

# While I frequently build and run using make, the convention is that running
# 'make' by itself merely builds the software:
default: $(DEFAULT_RUN)

core-debug: validate ink clean-package portraits
	dotnet build ${CORE_SOLUTION} -f netcoreapp3.1

core-release: validate ink clean-package portraits
	dotnet build ${CORE_SOLUTION} -f netcoreapp3.1 -c Release

framework-release: validate ink portraits
	msbuild -restore:True ${FRAMEWORK_SOLUTION} -p:Configuration=Release

framework-debug: validate ink clean-obj portraits
	msbuild -restore:True ${FRAMEWORK_SOLUTION} -p:Configuration=Debug


#PIPELINE

##Paks

##I do some dubious things here to adopt Make to a workflow that generates multiple output files per input file, which
##is not Make's strong suit.

isometric-park-fna/Content/Pak64/split/%.png: isometric-park-fna/Content/Pak64/%.png 
	convert $< -transparent '#e7ffff'  -crop 64x64 isometric-park-fna/Content/Pak64/split/$*"%02d.png"
	cp isometric-park-fna/Content/Pak64/split/$*00.png $@ 

isometric-park-fna/Content/pak64_tileset.png:   $(shell find -ipath "*isometric-park-fna/Content/Pak64/*.png" -and -not -ipath "*split*"  | sed "s:Pak64/:Pak64/split/:" |sort)
	rm $^
	montage isometric-park-fna/Content/Pak64/split/* -background transparent -tile 5 -geometry +0+0 -gravity north PNG8:$@
	touch $^


pak: isometric-park-fna/Content/pak64_tileset.png


## Merged

isometric-park-fna/Content/merged_tileset.png: isometric-park-fna/Content/pak64_tileset.png
	montage isometric-park-fna/Content/part4_tileset.png $^ isometric-park-fna/Content/remos_trees2.png isometric-park-fna/Content/64x64_Objects_2.png isometric-park-fna/Content/rubberduck_parts_tower.png  -background transparent -geometry +0+0 -tile 1x PNG:isometric-park-fna/Content/merged_tileset.png

merged: isometric-park-fna/Content/merged_tileset.png

##Portraits

isometric-park-fna/Content/Portraits/converted/%.png: isometric-park-fna/Content/Portraits/%.jpg 
	convert $< -resize 500x500\>  -dither FloydSteinberg  -colors 8 -colorspace gray $@

isometric-park-fna/Content/portraits.png:  $(shell find -ipath "*isometric-park-fna/Content/Portraits/*.jpg" | sed "s/.jpg/.png/" | sed "s:Portraits/:Portraits/converted/:" |sort)
# isometric-park-fna/Content/Portraits/converted/%.png
	montage $^ -background transparent -geometry 300x400  -gravity north PNG8:isometric-park-fna/Content/portraits.png

portraits:  isometric-park-fna/Content/portraits.png

isometric-park-fna/Content/%.json: $(shell find -ipath "*isometric-park-fna/Content/*.ink")
	inklecate -o $@ $<

ink: isometric-park-fna/Content/dialog.json


#RUN

run-core-debug:
	cd isometric-park-fna/bin/Debug/netcoreapp3.1; LD_LIBRARY_PATH="../../../../fnalibs/lib64" DYLD_LIBRARY_PATH="../../../../fnalibs/osx" dotnet ./isometric-park-fna.dll

run-core-release:
	cd isometric-park-fna/bin/Release/netcoreapp3.1; LD_LIBRARY_PATH="../../../../fnalibs/lib64" DYLD_LIBRARY_PATH="../../../../fnalibs/osx" dotnet ./isometric-park-fna.dll

run-core-release-xvfb:
	cd isometric-park-fna/bin/Release/netcoreapp3.1; LD_LIBRARY_PATH="../../../../fnalibs/lib64" DYLD_LIBRARY_PATH="../../../../fnalibs/osx" xvfb-run -a -s "-screen 0 1400x900x24 +extension RANDR  +extension GLX +extension RENDER" --  dotnet ./isometric-park-fna.dll

run-framework-release:
	cd isometric-park-fna/bin/Release/; LD_LIBRARY_PATH="../../../fnalibs/lib64" DYLD_LIBRARY_PATH="../../../fnalibs/osx" mono isometric-park-fna.exe

run-framework-debug:
	cd isometric-park-fna/bin/Debug/; LD_LIBRARY_PATH="../../../fnalibs/lib64" DYLD_LIBRARY_PATH="../../../fnalibs/osx" mono --debug isometric-park-fna.exe

# lint:
# 	yamllint -d relaxed isometric-park-fna/Content/news_items.yaml
count:
	tokei -e isometric-park-fna/packages -e isometric-park-fna/bin -e isometric-park-fna/build_log.txt -e isometric-park-fna/obj scripts/ isometric-park-fna/

clean-obj:
	#If this folder doesn't exist and we get an error, that's okay
	-rm -r isometric-park-fna/obj/

clean-package:
	#If this folder doesn't exist, that's okay
	-rm  -r isometric-park-fna/packages/* packages/*


# VALIDATE

validate-yaml:
	csi scripts/LoadYaml.csx

validate-ink:
	csi scripts/LoadInk.csx


validate: validate-yaml validate-ink

#CONVENIENCE

# Just using framework for releases since I know it works for now:
release: framework-release

run: $(DEFAULT_RUN) run-$(DEFAULT_RUN)
