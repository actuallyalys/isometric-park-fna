


* Tree species from Wikipedia
* Flowers come from Darius K's corpora][dariusk]
* Art "isms" come from Darius K's corpora][dariusk]
* Nationalities come the UK Government's list of nationalities via [Darius K's corpora][dariusk-nationalities]
* Sports come from Wikipedia via [Darius K's corpora][dariusk-sport]
* Household objects come from [Darius K's corpora][dariusk-objects]
* Feelings came from [Darius K's corpora][dariusk-mood].
* Nouns came from [Darius K's corpora][dariusk-nouns].
* Evaluation adjectives come from the [original Tracery source][tracery-src]
* Degrees comes from [UW-Madison's list][uwmad]
* Admin areas and subreagions come from [geonames.org](geonames.org)


The following were written by me: 
* artist_kind
* assistantNames
* book_descriptor
* book_serious
* book_title
* city
* large_number
* occupation
* many
* problem

[dariusk-sport]: https://github.com/dariusk/corpora/blob/master/data/sports/sports.json
[dariusk-objects]: https://github.com/dariusk/corpora/blob/master/data/objects/objects.json
[dariusk-mood]: https://github.com/dariusk/corpora/blob/master/data/humans/moods.json
[tracery-src]: https://github.com/galaxykate/tracery/blob/master/js/grammars.js
[uwmad]: https://grad.wisc.edu/academic-programs/

The GPT-2 model was fine-tuned using [gpt-2-simple][simple] on data from WikiNews.

[simple]: https://github.com/minimaxir/gpt-2-simple

