using System;
using System.Collections.Generic;
using System.Linq;

using Encompass;
using ImGuiNET;

using isometricparkfna.Messages;
using isometricparkfna.Components;
using isometricparkfna.UI;

namespace isometricparkfna.Engines
{

    [Sends(typeof(ToggleWindowMessage),
           typeof(ToggleWindowTypeMessage),
           typeof(ChangeContractStatusMessage),
           typeof(SelectMessage),
           typeof(JumpCameraMessage),
           typeof(GameStateMessage),
           typeof(GameRateMessage),
           typeof(SetResolutionMessage),
           typeof(SetFontMessage),
           typeof(SetTrespassingPolicyMessage),
           typeof(SpawnGameMessage),
           typeof(SetTextVariableMessage),
           typeof(SetDialogMessage),
           typeof(DialogChoiceMessage),
           typeof(SetOptionMessage),
           typeof(QuitGameMessage),
		   typeof(ToggleToolMessage),
           typeof(PlaySoundMessage),
           typeof(DebugAlterTreesMessage))]
    [Reads(typeof(VisibilityComponent),
           typeof(WindowTypeComponent),
           typeof(TrespassingPolicyComponent),
           typeof(ContractStatusComponent),
           typeof(RelatedOrganizationComponent),
           typeof(NameAndDescriptionComponent),
           typeof(OptionsComponent),
		   typeof(ToolComponent),
		   typeof(SelectedComponent))]
    [Writes(typeof(OptionsComponent))]
    public class ImGuiWindowBridgeEngine : Engine
    {
        public List<ToggleWindowMessage> messages;
        public List<ToggleWindowTypeMessage> typeMessages;
        public List<ChangeContractStatusMessage> contractStatusMessages;
        public List<SelectMessage> selectedMessages;
        public List<JumpCameraMessage> jumpCameraMessages;
        public List<GameStateMessage> gameStateMessages;
        public List<SetResolutionMessage> resolutionMessages;
        public List<SetFontMessage> fontMessages;
        public List<SetTrespassingPolicyMessage> trespassingPolicyMessages;
        public List<SpawnGameMessage> spawnGameMessages;
        public List<SetTextVariableMessage> setTextVariableMessages;
        public List<SetDialogMessage> setDialogMessages;
        public List<DialogChoiceMessage> dialogChoiceMessages;
        public List<SetOptionMessage> setOptionMessages;
        public List<GameRateMessage> gameRateMessages;
        public List<QuitGameMessage> quitGameMessages;
		public List<ToggleToolMessage> toggleToolMessages;
		public List<PlaySoundMessage> playSoundMessages;
		public List<DebugAlterTreesMessage> debugAlterTreesMessages;

        bool showBudget { get; }
        bool showForest { get; }
        bool showNews { get; }
        bool showGrid { get; }
        bool showTrees { get; }

        public Dictionary<Window, bool> windowStatuses { get; }
        public Dictionary<Tool, bool> toolStatuses { get; }

        public bool showContractIndicator;
        public List<string> contracts;

        public bool showBudgetLow;
        public bool showBudgetNegative;

        public ImFontPtr font;
        public ImFontPtr italicFont;
        private DebugWindow debugWindow;

        private Simulation Simulation;

        public DateTime DateTime
        {
            get
            {
                return this.Simulation.DateTime;
            }
        }

        public ImGuiWindowBridgeEngine(DebugWindow debugWindow,
                                       ImFontPtr font, ImFontPtr italicFont,
                                       Simulation simulation)
        {
            this.messages = new List<ToggleWindowMessage>();
            this.typeMessages = new List<ToggleWindowTypeMessage>();
            this.contractStatusMessages = new List<ChangeContractStatusMessage>();
            this.selectedMessages = new List<SelectMessage>();
            this.jumpCameraMessages = new List<JumpCameraMessage>();
            this.gameStateMessages = new List<GameStateMessage>();
            this.resolutionMessages = new List<SetResolutionMessage>();
            this.fontMessages = new List<SetFontMessage>();
            this.trespassingPolicyMessages = new List<SetTrespassingPolicyMessage>();
            this.spawnGameMessages = new List<SpawnGameMessage>();
            this.setTextVariableMessages = new List<SetTextVariableMessage>();
            this.setDialogMessages = new List<SetDialogMessage>();
            this.dialogChoiceMessages = new List<DialogChoiceMessage>();
            this.setOptionMessages = new List<SetOptionMessage>();
            this.gameRateMessages = new List<GameRateMessage>();
            this.quitGameMessages = new List<QuitGameMessage>();
			this.toggleToolMessages = new List<ToggleToolMessage>();
            this.playSoundMessages = new List<PlaySoundMessage>();
            this.debugAlterTreesMessages = new List<DebugAlterTreesMessage>();

            this.windowStatuses = new Dictionary<Window, bool>();
            this.toolStatuses = new Dictionary<Tool, bool>();

            this.showContractIndicator = false;
            this.showBudgetLow = false;
            this.showBudgetNegative = false;

            this.font = font;
            this.italicFont = italicFont;
            this.debugWindow = debugWindow;
            this.Simulation = simulation;


            this.contracts = new List<string>();


            //Prepopulate:
            foreach (var type in System.Enum.GetValues(typeof(Window)))
            {
                windowStatuses.Add((Window)type, false);
            }

            foreach (var type in System.Enum.GetValues(typeof(Tool)))
            {
                toolStatuses.Add((Tool)type, false);
            }
        }

        public override void Update(double dt)
        {
            foreach (var message in this.messages)
            {
                SendMessage(message);
            }
            foreach (var message in this.typeMessages)
            {
                SendMessage(message);
            }
            foreach (var message in this.contractStatusMessages)
            {
                SendMessage(message);
            }
            foreach (var message in this.selectedMessages)
            {
                SendMessage(message);
            }
            foreach (var message in this.jumpCameraMessages)
            {
                SendMessage(message);
            }

            //
            foreach (var message in this.setTextVariableMessages)
            {
                SendMessage(message);
            }
            foreach (var message in this.gameStateMessages)
            {
                SendMessage(message);
            }
            foreach (var message in this.resolutionMessages)
            {
                SendMessage(message);
            }
            foreach (var message in this.fontMessages)
            {
                this.font = debugWindow.addFont(message.fontName,
                                                message.fontSize, false);

                debugWindow.setMonoFont(this.font);

                this.italicFont = debugWindow.addFont(message.fontName,
                                                      message.fontSize, true);

                debugWindow.setItalicFont(this.italicFont);
                SendMessage(message);

                OptionsWindow.setFont(message.fontName,
                                      message.fontSize);
            }

            foreach (var message in this.trespassingPolicyMessages)
            {
                SendMessage(message);
            }

            foreach (var message in this.spawnGameMessages)
            {
                SendMessage(message);
            }


            foreach (var message in this.setDialogMessages)
            {
                SendMessage(message);
            }
            foreach (var message in this.dialogChoiceMessages)
            {
                SendMessage(message);
            }

            foreach (var message in this.gameRateMessages)
            {
                SendMessage(message);
            }

            foreach (var message in this.quitGameMessages)
            {
                SendMessage(message);
            }

            foreach (var message in this.toggleToolMessages)
            {
                SendMessage(message);
            }

            foreach (var message in this.setOptionMessages)
            {
                foreach (var entity in ReadEntities<OptionsComponent>())
                {

                    SetComponent(entity, new OptionsComponent
                    {
                        ProfanitySetting = message.NewProfanitySetting,
                        SoundEffectVolume = message.NewSoundEffectVolume,
                        SoundEffectMuted = message.NewSoundEffectMuted
                    });
                }

            }

            foreach (var message in this.playSoundMessages) {
                SendMessage(message);
            }

            foreach (var message in this.debugAlterTreesMessages) {
                SendMessage(message);
            }

            foreach (var entity in ReadEntities<WindowTypeComponent>())
            {
                var type = GetComponent<WindowTypeComponent>(entity).type;
                var visibility = HasComponent<VisibilityComponent>(entity) ? GetComponent<VisibilityComponent>(entity).visible : false;
                windowStatuses[type] = visibility;
            }

			foreach (var entity in ReadEntities<ToolComponent>()) {
				var tool = GetComponent<ToolComponent>(entity).Tool;
				var selected = HasComponent<SelectedComponent>(entity) ? GetComponent<SelectedComponent>(entity).selected : false;
				toolStatuses[tool] = selected;
			}

            //reset
            this.showContractIndicator = false;
            this.contracts.Clear();
            foreach (var entity in ReadEntities<ContractStatusComponent>())
            {
                var contractStatus = GetComponent<ContractStatusComponent>(entity);
                var age = this.Simulation.DateTime - contractStatus.date;
                if ((age.TotalDays > (5 * 30))
                        && (contractStatus.status == ContractStatus.Proposed))
                {
                    try
                    {
                        this.showContractIndicator = true;
                        if (HasComponent<RelatedOrganizationComponent>(entity))
                        {
                            var organizationEntity = GetComponent<RelatedOrganizationComponent>(entity).Entity;
                            var name = GetComponent<NameAndDescriptionComponent>(organizationEntity).DisplayName;
                            this.contracts.Add(name);

                        }
                    }
                    catch (Exception e)
                    {
                        Logging.Error(string.Format("Exception: {0}", e.ToString()));
                    }
                }
            }

            this.showBudgetLow = (this.Simulation .latestBudget.money < 10_000M);
            this.showBudgetNegative = (this.Simulation .latestBudget.money < 0M);


            this.messages.Clear();
            this.typeMessages.Clear();
            this.contractStatusMessages.Clear();
            this.selectedMessages.Clear();
            this.jumpCameraMessages.Clear();
            this.gameStateMessages.Clear();
            this.resolutionMessages.Clear();
            this.fontMessages.Clear();
            this.trespassingPolicyMessages.Clear();
            this.spawnGameMessages.Clear();
            this.setTextVariableMessages.Clear();
            this.setDialogMessages.Clear();
            this.dialogChoiceMessages.Clear();
            this.gameRateMessages.Clear();
            this.setOptionMessages.Clear();
            this.quitGameMessages.Clear();
			this.toggleToolMessages.Clear();
            this.playSoundMessages.Clear();
            this.debugAlterTreesMessages.Clear();
        }
    }
}
