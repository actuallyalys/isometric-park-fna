using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using Encompass;
using JM.LinqFaster;

using isometricparkfna.Messages;
using isometricparkfna.Components;

namespace isometricparkfna.Engines {

    [Receives(typeof(SpawnSelection),
              typeof(AdjustSelection),
              typeof(UndoPreserveMessage))]
    [Writes(typeof(AreaComponent),
            typeof(PointComponent),
            typeof(PreserveComponent))]
    [Reads(typeof(SelectedComponent),
           typeof(ContractStatusComponent),
           typeof(AreaComponent),
           typeof(PreserveComponent),
           typeof(SelectedComponent),
           typeof(ToolComponent),
           typeof(PointComponent),
           typeof(StructureComponent))]
    [Sends(typeof(ChangeContractStatusMessage),
            typeof(PlaySoundMessage),
            typeof(SingleExpenseMessage),
            typeof(SpawnDialogMessage))]
    public class BuildToolEngine : Engine {

        private CellMap Map;
        private Simulation Simulation;

        private static Dictionary<Tool, bool> ToolArea = new Dictionary<Tool, bool> {{Tool.Preserve, true},
            {Tool.Dezone, true},
            {Tool.Tower, false},
            {Tool.Bulldozer, true}};

        public BuildToolEngine(Simulation simulation) {
            this.Map = simulation.map;
            this.Simulation = simulation;
        }

        private System.Collections.Generic.IEnumerable<float> step_until(float start, float stop) {
            if (stop >= start) {
                for(float i = start; i <= stop; i++)
                {
                    yield return i;
                }
            }
            else {
                for(float i = start; i >= stop; i--)
                {
                    yield return i;
                }
            }
        }

        private void clear_areas(Vector2[] clear_squares) {
            List<Entity> entities = new List<Entity>();
            foreach(ref readonly var entity in ReadEntities<AreaComponent>() ) {
                var squares = GetComponent<AreaComponent>(entity).squares;

                foreach(var clear_square in clear_squares) {

                    foreach(var square in squares) {
                        if(square == clear_square) {
                            entities.Add(entity);
                            break;
                        }
                    }
                }
            }

            foreach(var entity in entities) {
                Logging.Debug("Deleting entity.");

                if (HasComponent<ContractStatusComponent>(entity)) {
                    SendMessage(new ChangeContractStatusMessage {newStatus = ContractStatus.Broken, Entity = entity});
                }
                else {
                    Destroy(entity);
                }
            }
        }

        private void clear_structures(Vector2[] clear_squares) {
            List<Entity> entities = new List<Entity>();
            foreach(ref readonly var entity in ReadEntities<StructureComponent>() ) {
                var square = GetComponent<PointComponent>(entity).Square;

                foreach(var clear_square in clear_squares) {
                    if(square == clear_square) {
                        entities.Add(entity);
                    }
                }
            }

            foreach(var entity in entities) {
                Logging.Debug("Deleting entity.");
                Destroy(entity);
            }
        }

        private void SpawnSelection(Tool tool) {
            foreach (ref readonly var message in ReadMessages<SpawnSelection>())
            {
                if (message.Start.X >= 0 && message.Start.X < this.Map.MapWidth
                        && message.Start.Y >= 0 && message.Start.Y < this.Map.MapHeight) {
                    var entity = CreateEntity();
                    if (BuildToolEngine.ToolArea[tool]) {
                        AddComponent(entity, new AreaComponent {squares = new[] {message.Start},
                                Tool = tool});
                    }
                    else {
                        AddComponent(entity, new PointComponent {Square = message.Start,
                                Tool=tool});
                    }
                    AddComponent(entity, new SelectedComponent {selected = true, Type= SelectionType.Area});
                }
            }
        }


        public List<Vector2> GetOccupied() {
            List<Vector2> occupied = new List<Vector2>();

            foreach (var (entity, status) in ReadEntities<AreaComponent>()
                    .WhereF((e) => HasComponent<ContractStatusComponent>(e))
                    .SelectWhereF((e) => (e, GetComponent<ContractStatusComponent>(e)),
                        (e) => ((e.Item2.status != ContractStatus.Broken)
                            && (e.Item2.status != ContractStatus.Rejected)
                            && (e.Item2.status != ContractStatus.Expired)
                            && (e.Item2.status != ContractStatus.Completed)
                            && (e.Item2.status != ContractStatus.Proposed))
                        )) {
                var entitySquares = GetComponent<AreaComponent>(entity).squares;
                occupied.AddRange(entitySquares);
            }
            return occupied;
        }

        public override void Update(double dt) {
            Dictionary<Tool, bool> statuses = new Dictionary<Tool, bool>();

            foreach (var entity in ReadEntities<ToolComponent>()) {
                var tool = GetComponent<ToolComponent>(entity);
                var selected = GetComponent<SelectedComponent>(entity);

                statuses.Add(tool.Tool, selected.selected);
            }

            if (statuses.ContainsKey(Tool.Preserve) && statuses[Tool.Preserve]) {
                var occupied = GetOccupied();
                foreach (var entity in ReadEntities<AreaComponent>()
                        .WhereF((e) => HasComponent<PreserveComponent>(e))) {
                    var entitySquares = GetComponent<AreaComponent>(entity).squares;
                    occupied.AddRange(entitySquares);
                }

                SpawnSelection(Tool.Preserve);

                foreach (ref readonly var message in ReadMessages<AdjustSelection>()) {
                    foreach (ref readonly var entity in ReadEntities<SelectedComponent>()) {
                        var selection = GetComponent<SelectedComponent>(entity);
                        if(selection.Type == SelectionType.Area
                                && selection.selected) {
                            if(message.Type == AdjustmentType.Clear) {
                                Destroy(entity);
                            }
                            else if(message.Type == AdjustmentType.Complete) {
                                SetComponent(entity, new PreserveComponent {date = this.Simulation.DateTime});
                                // SetComponent(entity, new SelectedComponent {selected = false });
                                var area = GetComponent<AreaComponent>(entity);
                                if (area.squares.Length > 400) {
                                    SendMessage<SpawnDialogMessage>(new SpawnDialogMessage { 
                                            Path = "LargePreserve" });
                                }
                            }
                            else {
                                var area = GetComponent<AreaComponent>(entity);

                                var newSquares = new List<Vector2>();

                                var end_x = MathUtils.Clamp(message.End.X, 0.0f, this.Map.MapWidth-1);
                                var end_y = MathUtils.Clamp(message.End.Y, 0.0f, this.Map.MapHeight-1);

                                if (area.squares.Length > 0) {
                                    foreach (var i in step_until(area.squares[0].X, end_x)) {
                                        foreach (var j in step_until(area.squares[0].Y, end_y)) {
                                            var newSquare = new Vector2(i, j);
                                            if (!occupied.Contains(newSquare)) {
                                                newSquares.Add(newSquare);
                                            }
                                        }
                                    }
                                }
                                SetComponent(entity, new AreaComponent { squares = newSquares.ToArray(), Tool = area.Tool});
                            }
                        }
                    }
                }
            }
            else if (statuses.ContainsKey(Tool.Dezone) && statuses[Tool.Dezone]) {
                SpawnSelection(Tool.Dezone);

                foreach (ref readonly var message in ReadMessages<AdjustSelection>()) {
                    foreach (ref readonly var entity in ReadEntities<SelectedComponent>()) {
                        var selection = GetComponent<SelectedComponent>(entity);
                        if(selection.Type == SelectionType.Area
                                && selection.selected) {
                            if(message.Type == AdjustmentType.Clear) {
                                Destroy(entity);
                            }
                            else if(message.Type == AdjustmentType.Complete) {
                                    var squares = GetComponent<AreaComponent>(entity).squares;
                                    clear_areas(squares);
                            }
                            else {
                                var area = GetComponent<AreaComponent>(entity);

                                var newSquares = new List<Vector2>();

                                var end_x = MathUtils.Clamp(message.End.X, 0.0f, this.Map.MapWidth);
                                var end_y = MathUtils.Clamp(message.End.Y, 0.0f, this.Map.MapHeight);

                                foreach (var i in step_until(area.squares[0].X, end_x)) {
                                    foreach (var j in step_until(area.squares[0].Y, end_y)) {
                                        var newSquare = new Vector2(i, j);
                                        newSquares.Add(newSquare);
                                    }
                                }
                                SetComponent(entity, new AreaComponent { squares = newSquares.ToArray(), Tool = area.Tool});
                            }
                        }
                    }
                }
            }
            else if (statuses.ContainsKey(Tool.Tower) && statuses[Tool.Tower]) {
                SpawnSelection(Tool.Tower);

                var occupied = GetOccupied();

                foreach (var (entity, point) in ReadEntities<StructureComponent>()
                        .WhereF((e) => HasComponent<PointComponent>(e))
                        .Select((e) => (e, GetComponent<PointComponent>(e)))) {

                    occupied.Add(point.Square);
                }

                foreach (ref readonly var message in ReadMessages<AdjustSelection>()) {
                    foreach (ref readonly var entity in ReadEntities<SelectedComponent>()) {
                        var selection = GetComponent<SelectedComponent>(entity);

                        if (selection.Type == SelectionType.Area
                                && selection.selected) {
                            if(message.Type == AdjustmentType.Clear) {
                                Destroy(entity);
                            }
                            else if (message.Type == AdjustmentType.Complete) {
                                var point = GetComponent<PointComponent>(entity);
                                var structure_entity = CreateEntity();
                                var cell = this.Map.cells[(int)point.Square.X][(int)point.Square.Y];
                                //Verify square is free:
                                //
                                if (!occupied.Contains(point.Square) && !cell.HasTree && !cell.HasWater) {

                                    AddComponent(structure_entity, new PointComponent {Square = point.Square,
                                            Tool = point.Tool} );
                                    AddComponent(structure_entity,
                                            new StructureComponent { Structure = Structure.Tower});
                                    Destroy(entity);
                                    SendMessage(new PlaySoundMessage { SoundName = "ConstructionShort" });
                                    SendMessage(new SingleExpenseMessage { category = "Construction",
                                            amount = 500M });
                                    Logging.Success("Placed Tower!");
                                }

                            }
                            else {
                                var point = GetComponent<PointComponent>(entity);
                                SetComponent(entity, new PointComponent {Square = message.End,
                                        Tool = point.Tool});
                            }
                        }
                    }
                }
            }

            else if ((statuses.ContainsKey(Tool.Bulldozer) && statuses[Tool.Bulldozer])) {
                SpawnSelection(Tool.Bulldozer);

                foreach (ref readonly var message in ReadMessages<AdjustSelection>()) {
                    foreach (ref readonly var entity in ReadEntities<SelectedComponent>()) {
                        var selection = GetComponent<SelectedComponent>(entity);
                        if(selection.Type == SelectionType.Area
                                && selection.selected) {
                            if (message.Type == AdjustmentType.Clear) {
                                Destroy(entity);
                            }
                            else if (message.Type == AdjustmentType.Complete) {
                                var squares = GetComponent<AreaComponent>(entity).squares;
                                clear_structures(squares);
                                Destroy(entity); //Dezone destroys itself, so it doesn't need this command.
                            }
                            else {
                                var area = GetComponent<AreaComponent>(entity);

                                var newSquares = new List<Vector2>();

                                var end_x = MathUtils.Clamp(message.End.X, 0.0f, this.Map.MapWidth);
                                var end_y = MathUtils.Clamp(message.End.Y, 0.0f, this.Map.MapHeight);

                                foreach (var i in step_until(area.squares[0].X, end_x)) {
                                    foreach (var j in step_until(area.squares[0].Y, end_y)) {
                                        var newSquare = new Vector2(i, j);
                                        newSquares.Add(newSquare);
                                    }
                                }
                                SetComponent(entity, new AreaComponent { squares = newSquares.ToArray(), Tool = area.Tool});
                            }
                        }
                    }
                }
            }

            DateTime min = DateTime.MinValue;
            Entity newestEntity = default;

            foreach (ref readonly var message in ReadMessages<UndoPreserveMessage>()) {
                foreach (var entity in ReadEntities<PreserveComponent>()) {
                    var preserveComponent = GetComponent<PreserveComponent>(entity);
                    if (min < preserveComponent.date) {
                        min = preserveComponent.date;
                        newestEntity = entity;
                    }
                    Logging.Debug(String.Format("Checking preserve entity for {0}.", min));
                }
            }
            if  (min != DateTime.MinValue) {
                Destroy(newestEntity);
            }
        }
    }
}
