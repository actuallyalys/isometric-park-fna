
using Encompass;

using isometricparkfna.Messages;
using isometricparkfna.Components;

namespace isometricparkfna.Engines {

    [Receives(typeof(ZoomCameraMessage), typeof(MoveCameraMessage),
              typeof(JumpCameraMessage))]
    class CameraBridgeEngine : Engine
    {
        private Camera Camera;

        public CameraBridgeEngine(Camera camera)
        {
            this.Camera = camera;
        }


        public override void Update(double dt)
        {
            foreach (ref readonly var message in ReadMessages<ZoomCameraMessage>())
            {
                if (message.ZoomIn)
                {
                    this.Camera.ZoomIn();
                }
                else
                {
                    this.Camera.ZoomOut();
                }
            }

            foreach (ref readonly var message in ReadMessages<MoveCameraMessage>())
            {
                this.Camera.Move(message.Movement);
            }
            foreach (ref readonly var message in ReadMessages<JumpCameraMessage>())
            {
                this.Camera.Jump(message.Movement);
            }

        }
    }
}
