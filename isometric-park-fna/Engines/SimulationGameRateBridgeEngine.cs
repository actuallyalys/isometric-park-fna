using System;
using System.Linq;
using System.Collections.Generic;

using Microsoft.Xna.Framework;

using Encompass;

using isometricparkfna.Messages;
using isometricparkfna.Components;

namespace isometricparkfna.Engines
{
    [Receives(typeof(GameRateMessage), typeof(TogglePauseMessage))]
    public class SimulationGameRateBridgeEngine : Engine
    {
        public Simulation simulation;
        private Random random_generator;

        public SimulationGameRateBridgeEngine(Simulation simulation)
        {
            this.simulation = simulation;
            this.random_generator = new Random();

        }


        public override void Update(double dt)
        {
            foreach (ref readonly var message in ReadMessages<GameRateMessage>())
            {
                this.simulation.paused = message.paused;
                if (message.rate != null)
                {
                    this.simulation.setRate(message.rate ?? 0);
                }
            }

            foreach (ref readonly var message in ReadMessages<TogglePauseMessage>())
            {
                this.simulation.paused = !this.simulation.paused;
            }

        }

    }
}
