
using Encompass;

using isometricparkfna.Messages;
using isometricparkfna.Components;

namespace isometricparkfna.Engines {

    [Receives(typeof(ChangeContractStatusMessage))]
    [Writes(typeof(ContractStatusComponent))]
    [Reads(typeof(ContractStatusComponent))]

// [QueryWith(typeof(ContractStatusComponent))]
    class ContractStatusEngine : Engine
    {

        private Simulation simulation;

        public ContractStatusEngine(Simulation simulation)
        {
            this.simulation = simulation;
        }

        public override void Update(double dt)
        {
            foreach (ref readonly var message in ReadMessages<ChangeContractStatusMessage>())
            {
                var entity = message.Entity;

                SetComponent(entity, new ContractStatusComponent {status = message.newStatus,
                             date = simulation.DateTime
                                                                 });
            }

            foreach (ref readonly var entity in ReadEntities<ContractStatusComponent>())
            {

                var status = GetComponent<ContractStatusComponent>(entity);

                switch (status.status)
                {
                    case ContractStatus.Proposed:
                        if (this.simulation.DateTime.AddMonths(-6) > status.date)
                        {
                            SetComponent(entity, new ContractStatusComponent {status = ContractStatus.Expired, date = this.simulation.DateTime });
                            // SendMessage<ChangeContractStatusMessage>(new ChangeContractStatusMessage{newStatus = ContractStatus.Rejected});
                        }

                        break;

                }
            }
        }

    }
}
