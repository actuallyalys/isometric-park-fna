
using System.Collections.Generic;
using System.IO;

using Microsoft.Xna.Framework.Input;
using Newtonsoft.Json;
using Encompass;

using isometricparkfna.Messages;
using isometricparkfna.Components;

namespace isometricparkfna.Engines
{


    [Receives(typeof(ToggleWindowTypeMessage), typeof(ToggleWindowMessage),
              typeof(GameStateMessage),
              typeof(ToggleVisibilityMessage),
              typeof(SetResolutionMessage),
              typeof(SetFontMessage),
              typeof(QuitGameMessage))]
    [Reads(typeof(AreaComponent),
            typeof(PointComponent),
            typeof(ContractStatusComponent),
            typeof(OptionsComponent),
            typeof(PreserveComponent))]
    [Sends(typeof(UndoPreserveMessage))]
    class GameBridgeEngine : Engine
    {

        public FNAGame game;

        public List<UndoPreserveMessage> undoPreserveMessages;

        public GameBridgeEngine(FNAGame game)
        {
            this.game = game;

            this.undoPreserveMessages = new List<UndoPreserveMessage>();
        }

        public override void Update(double dt)
        {

            ProfanityLevel profanity_setting = default;
	    float sound_effect_volume = 1.0f; //full volume
	    bool sound_effect_muted = false;

            foreach (var message in undoPreserveMessages)
            {
                SendMessage<UndoPreserveMessage>(message);
            }

            this.undoPreserveMessages.Clear();

            foreach (ref readonly var windowMessage in ReadMessages<ToggleWindowTypeMessage>())
            {
                switch (windowMessage.Window)
                {
                    case Window.Debug:
                        game.show_another_window = !game.show_another_window;
                        break;
                    case Window.Budget:
                        game.showBudget = !game.showBudget;
                        break;

                }
            }
            foreach (ref readonly var visibilityMessage in ReadMessages<ToggleVisibilityMessage>())
            {

                switch (visibilityMessage.Element)
                {
                    case Element.Grid:
                        game.showGrid = !game.showGrid;
                        break;
                    case Element.Trees:
                        game.showTrees = !game.showTrees;
                        break;
                }
            }
            foreach (ref readonly var stateMessage in ReadMessages<GameStateMessage>())
            {
                game.isPlaying = stateMessage.isPlaying;
            }
            foreach (ref readonly var resolutionMessage in ReadMessages<SetResolutionMessage>())
            {
                game.setResolution(resolutionMessage.resolution,
                                   resolutionMessage.fullscreen);

            }
            foreach (ref readonly var entity in ReadEntities<OptionsComponent>())
            {
                var component = GetComponent<OptionsComponent>(entity);

                profanity_setting = component.ProfanitySetting;
		sound_effect_volume = component.SoundEffectVolume;
		sound_effect_muted = component.SoundEffectMuted;
            }

            foreach (ref readonly var fontMessage in ReadMessages<SetFontMessage>())
            {
                game.setFont(fontMessage.fontName, fontMessage.fontSize);
                Options.writeOptions(fontMessage.fontName, fontMessage.fontSize,
                                     profanity_setting, sound_effect_volume, sound_effect_muted);
            }


            game.in_zone = false;
            game.in_active_zone = false;
            game.in_preserve = false;
            game.has_tower = false;
            foreach (ref readonly var entity in ReadEntities<AreaComponent>())
            {
                var areaComponent = GetComponent<AreaComponent>(entity);

                if (HasComponent<ContractStatusComponent>(entity))
                {
                    var contractStatusComponent = GetComponent<ContractStatusComponent>(entity);
                    foreach (var square in areaComponent.squares)
                    {
                        if (game.mouseGrid == square)
                        {
                            game.in_zone = true;
                            if ((contractStatusComponent.status == ContractStatus.Active)
                                    || (contractStatusComponent.status == ContractStatus.Accepted))
                            {
                                game.in_active_zone = true;
                            }
                        }
                    }
                }
                else if (HasComponent<PreserveComponent>(entity)) {
                    foreach (var square in areaComponent.squares) {
                        if (game.mouseGrid == square)
                        {

                            game.in_preserve = true;
                        }

                    }
                }
            }
            foreach (ref readonly var entity in ReadEntities<PointComponent>())
            {
                var point_component = GetComponent<PointComponent>(entity);

                if (game.mouseGrid == point_component.Square)
                {
                    game.has_tower = true;
                }
            }

            foreach (ref readonly var message in ReadMessages<QuitGameMessage>())
            {
                this.game.quit = true;
            }
        }
    }
}
