using Encompass;

using isometricparkfna.Messages;
using isometricparkfna.Components;

namespace isometricparkfna.Engines
{

    [Receives(typeof(GameStateMessage))]
    [Sends(typeof(ToggleWindowTypeMessage))]
    [Reads(typeof(GameStateComponent))]
    [Writes(typeof(GameStateComponent))]
    class GameStateEngine : Engine
    {

        public override void Update(double dt)
        {

            foreach (var message in ReadMessages<GameStateMessage>())
            {
                if (message.isPlaying)
                {
                    startGame();
                }

                foreach (var entity in ReadEntities<GameStateComponent>())
                {
                    var state = GetComponent<GameStateComponent>(entity).isPlaying;
                    Logging.Debug("Changing state: " + state.ToString());

                    SetComponent(entity, new GameStateComponent { isPlaying = message.isPlaying });
                }
            }
        }

        public void startGame()
        {
        }
    }
}
