using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Audio;

using Encompass;

using isometricparkfna.Messages;
using isometricparkfna.Components;

namespace isometricparkfna.Engines
{

    [Reads(typeof(OptionsComponent))]
    [Receives(typeof(PlaySoundMessage))]
    public class SoundEffectEngine : Engine
    {
        public static Dictionary<string, SoundEffect> sounds = new Dictionary<string, SoundEffect>();
        public float volume = 1.0f;
        public float pitch = 0.0f;
        public float pan = 0.0f;
        public bool muted = false;

        public static SoundEffect LoadSound(ContentManager content, string path) {
            return LoadSound(content, path, path);
        }

        public static SoundEffect LoadSound(ContentManager content, 
                string path, string name) {
            var sfx = content.Load<SoundEffect>(path);
            sounds.Add(name, sfx);
            return sfx;

        }

        public static void DisposeSounds() {

            foreach (KeyValuePair<string, SoundEffect> entry  in sounds ) {
                entry.Value.Dispose();
            }

        }

        public override void Update(double dt) {

            foreach(ref readonly var entity in
                    ReadEntities<OptionsComponent>()) {

                var component = GetComponent<OptionsComponent>(entity);
                volume = component.SoundEffectVolume;
                muted = component.SoundEffectMuted;

            }

            foreach(ref readonly var message 
                    in ReadMessages<PlaySoundMessage>()) {
                var sound = sounds[message.SoundName];

                var volume = message.Volume ?? this.volume;

                if (!muted) {
                    sound.Play(volume, this.pitch, this.pan);
                }
            }

        }

    }
}
