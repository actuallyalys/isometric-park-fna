
using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using ImGuiNET;

using Encompass;

using isometricparkfna.Messages;
using isometricparkfna.Components;

namespace isometricparkfna.Engines {

    [Sends( typeof(ZoomCameraMessage),
            typeof(MoveCameraMessage),
            typeof(JumpCameraMessage),
            typeof(ToggleWindowTypeMessage),
            typeof(ToggleWindowMessage), //???
            typeof(ToggleVisibilityMessage),
            typeof(TogglePauseMessage),
            typeof(GameRateMessage),
            typeof(GameStateMessage),
            typeof(QuitGameMessage),
            typeof(SpawnSelection),
            typeof(AdjustSelection),
            typeof(PlaySoundMessage))]
    [Reads(typeof(WindowTypeComponent),
           typeof(GameStateComponent),
           typeof(VisibilityComponent),
           typeof(CursorComponent))]
    [Writes(typeof(CursorComponent))]
    public class InputEngine : Engine
    {
        private KeyboardState keyboardPrev;
        private MouseState mousePrev;

        private GraphicsDevice graphicsDevice;
        private GraphicsDeviceManager gdm;
        private Camera camera;

        //Area to ignore:
        private int menuBarHeight;
        private int height;
        private int width;

        private double timeInRange;
        private const double SCROLL_TIME_THRESHOLD = 0.333f;

        public InputEngine(int menuBarHeight, Camera camera,
                           GraphicsDeviceManager gdm, int height, int width) {
            //initialize to blank for now
            this.keyboardPrev = new KeyboardState();
            this.menuBarHeight = menuBarHeight;
            this.camera = camera;
            this.gdm = gdm;
            this.graphicsDevice = gdm.GraphicsDevice;
            this.height = height;
            this.width = width;

            this.timeInRange = 0.0f;
        }


        Vector2 calculateMousegrid(Vector2 normalizedMousePos)
        {
            Vector2 adjust = new Vector2(Tile.TileSpriteWidth / 2, Tile.TileSpriteHeight);
            Vector2 adjustedMousePos = normalizedMousePos - adjust;

            float boardx = ((adjustedMousePos.X / Tile.TileSpriteWidth) + (adjustedMousePos.Y / Tile.TileSpriteHeight));
            float boardy = ((adjustedMousePos.Y / Tile.TileSpriteHeight) - (adjustedMousePos.X / Tile.TileSpriteWidth));

            return new Vector2((int)boardx, (int)boardy);
        }

        public override void Update(double dt) {

            var keyboardCur = Keyboard.GetState();
            var mouseCur = Mouse.GetState();
            var original_point = Vector2.Transform(new Vector2(mouseCur.X, mouseCur.Y),
                                                   Matrix.Invert(this.camera.get_transformation(this.graphicsDevice)));
            bool isPlaying = false;

            var viewWidth = gdm.PreferredBackBufferWidth;
            var viewHeight = gdm.PreferredBackBufferHeight;

            ImGuiIOPtr io = ImGui.GetIO();

            foreach (var entity in ReadEntities<GameStateComponent>())
            {
                var state = GetComponent<GameStateComponent>(entity).isPlaying;
                isPlaying = state;
            }

            if (isPlaying)
            {
                #region camera_movement_keys
                if (keyboardCur.IsKeyDown(Keys.Down))
                {
                    SendMessage(new MoveCameraMessage { Movement = new Vector2(0, 2)});
                }
                if (keyboardCur.IsKeyDown(Keys.Up))
                {
                    SendMessage(new MoveCameraMessage { Movement = new Vector2(0, -2)});

                }
                if (keyboardCur.IsKeyDown(Keys.Left))
                {
                    SendMessage(new MoveCameraMessage { Movement = new Vector2(-2, 0)});

                }
                if (keyboardCur.IsKeyDown(Keys.Right))
                {
                    SendMessage(new MoveCameraMessage {Movement = new Vector2(2, 0)});

                }
                if (keyboardCur.IsKeyDown(Keys.Subtract) && keyboardPrev.IsKeyUp(Keys.Subtract))
                {
                    SendMessage(new ZoomCameraMessage {ZoomIn = false});
                }
                else if (keyboardCur.IsKeyDown(Keys.Add) && keyboardPrev.IsKeyUp(Keys.Add))
                {
                    SendMessage(new ZoomCameraMessage {ZoomIn = true});
                }
                if (keyboardCur.IsKeyDown(Keys.C) && keyboardPrev.IsKeyUp(Keys.C))
                {
                    SendMessage(new JumpCameraMessage {Movement = Vector2.Zero });
                }
                #endregion camera_movement_keys
                #region gamerate_keys
                if (keyboardCur.IsKeyDown(Keys.P) && keyboardPrev.IsKeyUp(Keys.P) )
                {

                    SendMessage(new TogglePauseMessage());

                }
                if (keyboardCur.IsKeyDown(Keys.D0) && keyboardPrev.IsKeyUp(Keys.D0) )
                {
                    SendMessage(new TogglePauseMessage());

                }
                if (keyboardCur.IsKeyDown(Keys.D1) && keyboardPrev.IsKeyUp(Keys.D1) )
                {
                    SendMessage(new GameRateMessage {
                        paused = false,
                        rate = 0
                    });
                }
                if (keyboardCur.IsKeyDown(Keys.D2) && keyboardPrev.IsKeyUp(Keys.D2) )
                {
                    SendMessage(new GameRateMessage {
                        paused = false,
                        rate = 1
                    });
                }
                if (keyboardCur.IsKeyDown(Keys.D3) && keyboardPrev.IsKeyUp(Keys.D3) )
                {
                    SendMessage(new GameRateMessage {
                        paused = false,
                        rate = 2
                    });
                }
                if (keyboardCur.IsKeyDown(Keys.D4) && keyboardPrev.IsKeyUp(Keys.D4) )
                {
                    SendMessage(new GameRateMessage {
                        paused = false,
                        rate = 3
                    });
                }
#if DEBUG
                if (keyboardCur.IsKeyDown(Keys.D5) && keyboardPrev.IsKeyUp(Keys.D5) )
                {
                    SendMessage(new GameRateMessage {
                        paused = false,
                        rate = 4
                    });
                }
#endif
                #endregion gamerate_keys
                #region misc_keys
                if (keyboardCur.IsKeyDown(Keys.B) && keyboardPrev.IsKeyUp(Keys.B))
                {
                    SendMessage(new ToggleWindowTypeMessage {Window = Window.Budget});

                }
                if (keyboardCur.IsKeyDown(Keys.F) && keyboardPrev.IsKeyUp(Keys.F))
                {
                    SendMessage(new ToggleWindowTypeMessage {Window = Window.Forest});

                }
                if (keyboardCur.IsKeyDown(Keys.N) && keyboardPrev.IsKeyUp(Keys.N))
                {
                    SendMessage(new ToggleWindowTypeMessage {Window = Window.News});

                }
                if (keyboardCur.IsKeyDown(Keys.O) && keyboardPrev.IsKeyUp(Keys.O))
                {
                    Logging.Trace("Contracts toggled.");
                    SendMessage(new ToggleWindowTypeMessage {Window = Window.Contracts});
                }
                if (keyboardCur.IsKeyDown(Keys.G) && keyboardPrev.IsKeyUp(Keys.G))
                {
                    SendMessage(new ToggleVisibilityMessage {Element = Element.Grid});

                }
#if DEBUG
                if (keyboardCur.IsKeyDown(Keys.T) && keyboardPrev.IsKeyUp(Keys.T))
                {
                    SendMessage(new ToggleVisibilityMessage {Element = Element.Trees});

                }
#endif

                if (keyboardCur.IsKeyDown(Keys.Escape) && keyboardPrev.IsKeyUp(Keys.Escape))
                {
                    SendMessage(new ToggleWindowTypeMessage {Window = Window.InGameMenu});
                    SendMessage(new GameRateMessage { paused = true, rate = null });
		    //People will probably expect escape to clear, even though its primary purpose
		    //is to open the menu:
                    SendMessage(new AdjustSelection {Type = AdjustmentType.Clear });
                }

		//Back => Backspace
		if (keyboardCur.IsKeyDown(Keys.Back) && keyboardPrev.IsKeyUp(Keys.Back)) {
                    SendMessage(new AdjustSelection {Type = AdjustmentType.Clear });
		}
            }

            if (keyboardCur.IsKeyDown(Keys.OemPipe) && keyboardPrev.IsKeyUp(Keys.OemPipe))
            {
                SendMessage(new ToggleWindowTypeMessage {Window = Window.Debug});
            }

#if DEBUG
            if (keyboardCur.IsKeyDown(Keys.Q) && keyboardPrev.IsKeyUp(Keys.Q))
            {
                Logging.Info("Quitting");
                SendMessage(new QuitGameMessage {});
            }
#endif
            if ((keyboardCur.IsKeyDown(Keys.Q) && keyboardPrev.IsKeyUp(Keys.Q))
                    && (keyboardCur.IsKeyDown(Keys.LeftControl)
                        || keyboardCur.IsKeyDown(Keys.RightControl)))
            {
                Logging.Info("Quitting");
                SendMessage(new QuitGameMessage {});
            }
            #endregion misc_keys
            #region mouse_movement

            var within_top_range = MathUtils.BetweenExclusive(mouseCur.Y, menuBarHeight, 50 + menuBarHeight);
            var within_bottom_range  = MathUtils.BetweenExclusive(mouseCur.Y, (viewHeight - 50 -menuBarHeight), viewHeight-menuBarHeight);
            var within_left_range = MathUtils.BetweenExclusive(mouseCur.X, 0, 50);
            var within_right_range = MathUtils.BetweenExclusive(mouseCur.X, (viewWidth - 50), viewWidth);

            if (within_top_range || within_bottom_range || within_right_range || within_left_range)
            {
                this.timeInRange += dt;
            }
            else
            {
                this.timeInRange = 0;
            }

            if (within_top_range && this.timeInRange > SCROLL_TIME_THRESHOLD)
            {
                SendMessage(new MoveCameraMessage {Movement = new Vector2(0, -4)});
            }
            else if (within_bottom_range && this.timeInRange > SCROLL_TIME_THRESHOLD)
            {
                SendMessage(new MoveCameraMessage {Movement = new Vector2(0, 4)});
            }
            if (within_left_range && this.timeInRange > SCROLL_TIME_THRESHOLD)
            {
                SendMessage(new MoveCameraMessage {Movement = new Vector2(-4, 0)});
            }
            else if (within_right_range && this.timeInRange > SCROLL_TIME_THRESHOLD)
            {
                SendMessage(new MoveCameraMessage {Movement = new Vector2(4, 0)});
            }
            #endregion
            #region mouse_click

	    if (isPlaying && !io.WantCaptureMouse) {
		    if (mouseCur.RightButton == ButtonState.Pressed && mousePrev.RightButton == ButtonState.Released)
		    {
                SendMessage(new PlaySoundMessage { SoundName = "Click" });
			    SendMessage(new JumpCameraMessage {Movement = original_point});
		    }

		    if (mouseCur.LeftButton == ButtonState.Pressed && mousePrev.LeftButton == ButtonState.Released
				    // && keyboardCur.IsKeyDown(Keys.LeftShift)
		       )
		    {
			    SendMessage(new SpawnSelection {Start = CellMap.calculateMousegrid(original_point)});
		    }

		    if ( mouseCur.LeftButton == ButtonState.Released && mousePrev.LeftButton == ButtonState.Pressed)
		    {
			    SendMessage(new AdjustSelection {Type = AdjustmentType.Complete });
                // SendMessage(new 
		    }

		    if (mouseCur.LeftButton == ButtonState.Pressed && mousePrev.LeftButton == ButtonState.Pressed)
		    {
			    SendMessage(new AdjustSelection {End =  CellMap.calculateMousegrid(original_point)});
		    }

            var transformedPosition = Vector2.Transform(new Vector2(mouseCur.X, mouseCur.Y), Matrix.Invert(camera.get_transformation(this.graphicsDevice)));
            var gridPosition = calculateMousegrid(transformedPosition);
            if (MathUtils.Between(gridPosition.X, 0, this.width)
                    && MathUtils.Between(gridPosition.Y, 0, this.height))
            {
                foreach (ref readonly var entity in ReadEntities<CursorComponent>()) {
                    var cursorComponent = GetComponent<CursorComponent>(entity);

                    SetComponent(entity, new CursorComponent { position = gridPosition,
                            size = 1 });
                }
            }
	    }
        else if (ImGui.IsAnyItemHovered()) {
            if (mouseCur.LeftButton == ButtonState.Pressed && mousePrev.LeftButton == ButtonState.Released) {
                SendMessage(new PlaySoundMessage { SoundName = "ClickPart1" });
            }
            else if (mouseCur.LeftButton == ButtonState.Released && mousePrev.LeftButton == ButtonState.Pressed) {
                SendMessage(new PlaySoundMessage { SoundName = "ClickPart2" });
            }
        }

            #endregion
            this.keyboardPrev = keyboardCur;
            this.mousePrev = mouseCur;
        }
    }
}
