
using System;
using System.Linq;

using Encompass;
using TraceryNet;

using isometricparkfna.Messages;
using isometricparkfna.Components;

namespace isometricparkfna.Engines
{

    [Receives(typeof(SetTextVariableMessage))]
    public class TraceryBridgeEngine : Engine
    {
        public Grammar grammar;

        public TraceryBridgeEngine(Grammar grammar)
        {
            this.grammar = grammar;
        }


        public override void Update(double dt)
        {
            foreach (ref readonly var message in ReadMessages<SetTextVariableMessage>())
            {
                string variableString  = "#";
                variableString += String.Format("[{0}:{1}]", message.variable, message.value);
                variableString += "vars# ";
                // Logging.Spy(new {var_string = variableString});
                var result = grammar.Flatten(variableString);
            }
        }

    }
}
