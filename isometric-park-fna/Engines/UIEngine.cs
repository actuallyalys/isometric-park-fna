using System;
using System.Text.RegularExpressions;
using System.Linq;

using Microsoft.Xna.Framework.Input;

using Encompass;
using Ink.Runtime;
using TraceryNet;

using isometricparkfna.Messages;
using isometricparkfna.Components;

namespace isometricparkfna.Engines
{

    [Receives(typeof(ToggleWindowMessage),
              typeof(ToggleWindowTypeMessage),
              typeof(ToggleVisibilityMessage),
              typeof(SetWindowVisibilityMessage),
              typeof(SelectMessage),
              typeof(SetDialogMessage),
              typeof(DialogChoiceMessage),
              typeof(AdjustSelection),
			  typeof(ToggleToolMessage))]
    [Reads(typeof(DialogComponent),
           typeof(WindowTypeComponent),
           typeof(VisibilityComponent),
           typeof(SelectedComponent),
		   typeof(ToolComponent))]
    [Writes(typeof(VisibilityComponent),
            typeof(SelectedComponent))]
    class UIEngine : Engine
    {
        public Story Story;

        public UIEngine(Story story)
        {
            this.Story = story;
        }

        public override void Update(double dt)
        {
            foreach (var entity in ReadEntities<SelectedComponent>())
            {
                var selected = GetComponent<SelectedComponent>(entity);
                if (selected.Type == SelectionType.Window) {
                    SetComponent(entity, new SelectedComponent { selected = false });
                }
            }

            foreach (ref readonly var windowMessage in ReadMessages<SetWindowVisibilityMessage>())
            {
                foreach (ref readonly var entity in ReadEntities<WindowTypeComponent>())
                {
                    Logging.Spy(entity.ID, "ID");
                    if (EntityExists(windowMessage.Entity) && entity.ID == windowMessage.Entity.ID)
                    {
                        AddComponent(entity, new VisibilityComponent { visible = windowMessage.Visibility });
                        Logging.Success("Set Visibility.");

                    }
                }
            }
            foreach (ref readonly var windowMessage in ReadMessages<ToggleWindowMessage>())
            {
                foreach (ref readonly var entity in ReadEntities<WindowTypeComponent>())
                {
                    if (EntityExists(windowMessage.Entity) && entity.ID == windowMessage.Entity.ID)
                    {
                        var visibilityComponent = GetComponent<VisibilityComponent>(entity);
                        SetComponent(entity, new VisibilityComponent { visible = !visibilityComponent.visible });

                    }
                }
            }
            foreach (ref readonly var windowMessage in ReadMessages<ToggleWindowTypeMessage>())
            {
                foreach (ref readonly var entity in ReadEntities<WindowTypeComponent>())
                {

                    var window_type = GetComponent<WindowTypeComponent>(entity).type;
                    if (window_type == windowMessage.Window)
                    {
                        var visibilityComponent = GetComponent<VisibilityComponent>(entity);
                        SetComponent(entity, new VisibilityComponent { visible = !visibilityComponent.visible });
                    }
                }
            }


			//Set tool
            foreach (ref readonly var toolMessage in ReadMessages<ToggleToolMessage>())
            {
                foreach (ref readonly var entity in ReadEntities<ToolComponent>())
                {

                    var tool_type = GetComponent<ToolComponent>(entity).Tool;
                    if (tool_type == toolMessage.Tool)
                    {
						//Clear selections
						foreach (ref readonly var inner_entity in ReadEntities<ToolComponent>())
						{
							var inner_selected_component = GetComponent<SelectedComponent>(inner_entity);
							inner_selected_component.selected = false;
							SetComponent(inner_entity, inner_selected_component);
						}
                        var selectedComponent = GetComponent<SelectedComponent>(entity);
						selectedComponent.selected = !selectedComponent.selected;
                        SetComponent(entity, selectedComponent);
                    }
                }
            }

            foreach (ref readonly var selectedMessage in ReadMessages<SelectMessage>())
            {
                SetComponent<SelectedComponent>(selectedMessage.Entity,
                                                new SelectedComponent { selected = true });
            }

            foreach (ref readonly var message in ReadMessages<AdjustSelection>()) {
                if(message.Type == AdjustmentType.Complete) {
                    foreach (ref readonly var entity in ReadEntities<SelectedComponent>()) {
                        var selection = GetComponent<SelectedComponent>(entity);
                        if(selection.Type == SelectionType.Area
                                && selection.selected) {
                            SetComponent(entity, new SelectedComponent {Type = SelectionType.Area,
                                         selected = false
                                                                       });
						}
					}
				}
			}
		}
	}
}
