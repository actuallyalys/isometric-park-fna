using System;
using System.Linq;
using System.Collections.Generic;

using Microsoft.Xna.Framework;


using Encompass;
using JM.LinqFaster;
using TraceryNet;

using isometricparkfna.Messages;
using isometricparkfna.Components;

namespace isometricparkfna.Spawners {

//Only used for controlling generation, at least for now
    public enum OrganizationType
    {
        Unspecified,
        Family,
        LargeCorporation,
        Cooperative
    }

    [Receives(typeof(SpawnOrganizationtMessage))]
    class OrganizationSpawner : Spawner<SpawnOrganizationtMessage>
    {

        private Random random_generator;
        private Grammar grammar;

        public OrganizationSpawner(Simulation simulation, Grammar grammar)
        {
            this.random_generator = new Random();

            // this.simulation = simulation;
            this.grammar = grammar;
        }


        protected override void Spawn(in SpawnOrganizationtMessage message)
        {

            var organization = CreateEntity();
            var image_index =  message.type switch {
                OrganizationType.Cooperative => MathUtils.NextSample(random_generator, new[] {1, 2, 3, 11, 12, 13, 15}),
                    OrganizationType.LargeCorporation => MathUtils.NextSample(random_generator, new[] {0, 6, 7, 9, 10}),
                    OrganizationType.Family => MathUtils.NextSample(random_generator, new[] {4, 5, 8, 11, 14}),
                    _ => random_generator.Next(1, 7)

            };
            Logging.Success("Generated image index.");

            var name =  message.name != null ? message.name : "#logging_company.capitalizeAll#";
            var description = message.description != null ? message.description : "";

            AddComponent(organization, new NameAndDescriptionComponent { DisplayName = this.grammar.Flatten(name),
                         Description = this.grammar.Flatten(description)
                                                                       });
            AddComponent(organization, new ImageComponent { ImageIndex = image_index });
            AddComponent(organization, new OffersContractsComponent { });
            AddComponent(organization, new OrganizationTypeComponent { type = message.type  });

        }
    }
}
