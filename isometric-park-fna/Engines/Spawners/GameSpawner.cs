
using System;
using System.Collections.Generic;

using Microsoft.Xna.Framework;

using isometricparkfna.Messages;
using isometricparkfna.Components;
using static isometricparkfna.CellMap;
using isometricparkfna.UI;

using Encompass;
using TraceryNet;

namespace isometricparkfna.Spawners {

    [Receives(typeof(SpawnGameMessage))]
    [Sends(typeof(SpawnContractMessage),
           typeof(SpawnOrganizationtMessage),
           typeof(ToggleWindowMessage),
           typeof(SpawnDialogMessage))]
    [Writes(typeof(WindowTypeComponent)
            //, typeof(DialogComponent)
           )]
    class GameSpawner : Spawner<SpawnGameMessage>

    {
        private Simulation simulation;
        Random random_generator;
        FNAGame game;
        Grammar grammar;

        public GameSpawner(Simulation simulation, FNAGame game, Grammar grammar)
        {
            this.simulation = simulation;
            this.random_generator = new Random();
            this.game = game;
            this.grammar = grammar;
        }

        protected override void Spawn(in SpawnGameMessage message)
        {

            #region generate_water

            /*
             * Differs from code in ContractSpawner in a few ways:
             * —Not concerned with existing tiles—this is one of the first things we're doing to the new map
             * —Doesn't keep track of attempts (should be few and far betweet.)
             * —No logging (not sure what logging is necessary here)
             *
             * These notes are mostly for myself in the future if I decide to unify these implementations.
             */
            for (int i = 0; i < Simulation.NUM_WATER_FEATURES; i++) {
                var water_x = this.random_generator.Next(0, this.simulation.map.MapWidth);
                var water_y = this.random_generator.Next(0, this.simulation.map.MapHeight);


                var water_squares = new List<Vector2>(new[] { new Vector2(water_x, water_y) });
                var squares_to_add = new HashSet<Vector2>();
                var odds = 0.50;

                var water_size = random_generator.Next(50, 250);

                while (water_squares.Count < water_size) {
                    foreach (var square in water_squares)
                    {
                        foreach (var new_square in new[] {new Vector2(square.X + 1, square.Y),
                                new Vector2(square.X, square.Y + 1),
                                new Vector2(square.X - 1, square.Y),
                                new Vector2(square.X, square.Y - 1)
                                })
                        {
                            if (random_generator.NextDouble() < odds
                                    && !water_squares.Contains(new_square)
                                    && MathUtils.Between(new_square.X, 0, this.simulation.map.MapWidth)
                                    && MathUtils.Between(new_square.Y, 0, this.simulation.map.MapHeight)
                               )
                            {
                                squares_to_add.Add(new_square);
                            }
                        }
                    }
                    water_squares.AddRange(squares_to_add);
                    squares_to_add.Clear();
                }
                this.simulation.map.WaterCells.AddRange(water_squares);

                foreach (var square in water_squares) {
                    this.simulation.map.cells[(int)square.X][(int)square.Y].AddWater();

                }
            }

            for (int i = 0; i < Simulation.NUM_PONDS; i++) {
                var water_x = this.random_generator.Next(0, this.simulation.map.MapWidth);
                var water_y = this.random_generator.Next(0, this.simulation.map.MapHeight);
                this.simulation.map.WaterCells.Add(new Vector2(water_x, water_y));
                this.simulation.map.cells[water_x][water_y].AddWater();
                Logging.Info(String.Format("Adding water at {0},{1}", water_x, water_y));
            }


            #endregion
            #region generate_trees

            foreach (List<Cell> row in this.simulation.map.cells)
            {
                foreach (Cell cell in row)
                {
                    var next = this.random_generator.NextDouble();
                    if (next > 0.75 && !cell.HasWater)
                    {
                        int random_year = (int)MathHelper.Clamp((float)MathUtils.NextNormal(random_generator, 2010.0f, 40.0f), 1800, Simulation.START_YEAR);
                        int random_month = random_generator.Next(1, 13);
                        DateTime random_date = new DateTime(random_year, random_month, 1);

                        int random_type = random_generator.Next(0, 4);

                        cell.AddTree(random_date, (TreeType)random_type);
                    }
                }
            }
            #endregion

            #region create_contracts

            var area = CreateEntity();
            var size = 5;
            var squares = new Vector2[size * size];
            var start_x = 10;

            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    squares[i * size + j] = new Vector2(i + start_x, j);
                }

            }
            SendMessage(new SpawnContractMessage
            {
                squares = squares,
                name = "Northshore Logging"
            });
            SendMessage(new SpawnContractMessage
            {
                name = "Aeres Maximalis Ltd."
            });
            #endregion

            #region create_organizations
            for (int i = 0; i < 3; i++)
            {

                SendMessage(new SpawnOrganizationtMessage { offersContracts = true,
                            name = "#family_company.capitalizeAll#",
                            description = "#family_company_description#",
                            type = OrganizationType.Family
                                                          });
                SendMessage(new SpawnOrganizationtMessage { offersContracts = true,
                            name = "#large_company.capitalizeAll#",
                            description = "#large_company_description#",
                            type = OrganizationType.LargeCorporation
                                                          });
            }
            SendMessage(new SpawnOrganizationtMessage { offersContracts = true,
                        name = "#logging_company.capitalizeAll#",
                        description = "#company_description#"
                                                      });
            SendMessage(new SpawnOrganizationtMessage { offersContracts = true,
                        name = "#coop_company.capitalizeAll#",
                        description = "#coop_company_description#",
                        type = OrganizationType.Cooperative
                                                      });
            SendMessage(new SpawnOrganizationtMessage { offersContracts = true,
                        name = "#coop_company.capitalizeAll#",
                        description = "#coop_company_description#",
                        type = OrganizationType.Cooperative
                                                      });
            #endregion
            #region dialog

            SendMessage(new SpawnDialogMessage { Path = "IntroGovernor"});
            // SendMessage(new SpawnDialogMessage { Path = "Once"});
            SendMessage(new SpawnDialogMessage { Path = "IntroAssistant"});
            #endregion
            this.simulation.Subsidy = message.Difficulty switch {
                DifficultyLevel.Hard => 0M,
                DifficultyLevel.Medium => 12_550M,
                DifficultyLevel.Easy => 15_000M,
                _ => 1000M
        };

        this.simulation.SubsidyDelta = message.Difficulty switch {
                DifficultyLevel.Hard => 0M,
                DifficultyLevel.Medium => -50M,
                DifficultyLevel.Easy => 0M,
                _ => 1000M
        };

#region 

        var cursorEntity = CreateEntity();
	AddComponent(cursorEntity,
			new CursorComponent { position = new Vector2(20, 20),
			size = 1 });

#endregion

        Logging.Success("Spawned new game.");
        }
    }
}
