using System;

using Microsoft.Xna.Framework;

using Encompass;

using isometricparkfna.Messages;
using isometricparkfna.Components;
using isometricparkfna.Utils;

namespace isometricparkfna.Engines
{

    [Receives(typeof(TickMessage))]
    [Sends(typeof(SpawnDialogMessage))]
    [Reads(typeof(EventComponent),
           typeof(TreeDeltaComponent))]
    [Writes(typeof(TreeDeltaComponent))]
    class EventEngine : Engine
    {
        Random Random;

        public EventEngine()
        {
            this.Random = new Random();

        }

        public override void Update(double dt)
        {
            DateTime simulationDate = default;

            #region random_events
            foreach (var tick in ReadMessages<TickMessage>())
            {
                simulationDate = tick.SimulationDateTime;

                #region remove_events
                foreach(var entity in ReadEntities<EventComponent>())
                {
                    DateTime endDate = GetComponent<EventComponent>(entity).EndEventDate;
                    if (endDate == null || simulationDate >= endDate)
                    {
                        Logging.Debug("Destroying Event entity.");
                        Destroy(entity);
                    }
                }
                #endregion
#if DEBUG
                if (this.Random.Next(0, 100) == 1)
                {
                    Logging.Debug("Spawning Once dialog.");
                    SendMessage(new SpawnDialogMessage { Path = "Once" });
                }
#endif

                if (this.Random.Next(0, 100) == 2)
                {
                    Logging.Debug("Tree planting campaign!🎉");

                    var campaignEntity = CreateEntity();

                    AddComponent(campaignEntity, new TreeDeltaComponent {deltaTrees = new Fact<int>(50*12)});
                    AddComponent(campaignEntity, new EventComponent {});
                }
                if (this.Random.Next(0, 100) == 4)
                {
                    Logging.Debug("Mass vandalism 😿");

                    var campaignEntity = CreateEntity();

                    AddComponent(campaignEntity, new TreeDeltaComponent {deltaTrees = new Fact<int>(-50*12)});
                    AddComponent(campaignEntity, new EventComponent {});

                    SendMessage(new SpawnDialogMessage { Path = "MassVandalism" });
                }

                if (this.Random.Next(0, 100) == 5)
                {
                    Logging.Debug("Fundraiser! 💰");

                    var fundraiserEntity = CreateEntity();

                    AddComponent(fundraiserEntity, new BudgetLineComponent {
                        category = "Misc",
                        amount = 10000M
                    });

                    // AddComponent(fundraiserEntity, new EventComponent {});
                    AddComponent(fundraiserEntity, new EventComponent { EndEventDate = simulationDate.AddMonths(1) });
                    SendMessage(new SpawnDialogMessage { Path = "Fundraiser" });
                }
                if (this.Random.Next(0, 100) == 6)
                {
                    Logging.Debug("Federal Grant! 💰");

                    var fundraiserEntity = CreateEntity();

                    AddComponent(fundraiserEntity, new BudgetLineComponent {
                        category = "Misc",
                        amount = 1000M
                    });

                    AddComponent(fundraiserEntity, new EventComponent { EndEventDate = simulationDate.AddMonths(12) });
                    SendMessage(new SpawnDialogMessage { Path = "GovernmentGrant" });
                }
            }
            #endregion

        }
    }
}
