using System;

using Encompass;

using isometricparkfna.Messages;
using isometricparkfna.Components;

namespace isometricparkfna.Engines
{

    [Receives(typeof(SetTrespassingPolicyMessage))]
    [Reads(typeof(TrespassingPolicyComponent))]
    [Writes(typeof(TrespassingPolicyComponent),
            typeof(BudgetLineComponent))]
        public class PolicyEngine : Engine
    {
        public PolicyEngine()
        {
        }

        public override void Update(double dt)
        {
            foreach (ref readonly var message
                    in ReadMessages<SetTrespassingPolicyMessage>())
            {
                foreach (ref readonly var entity
                        in ReadEntities<TrespassingPolicyComponent>())
                {

                    SetComponent<TrespassingPolicyComponent>(entity,
                            new TrespassingPolicyComponent
                            {
                            tresspassingPolicy
                            = message.newEnforcementLevel
                            });

                    var newCost = message.newEnforcementLevel switch {
                        EnforcementLevel.Unspecified => 0,
                            EnforcementLevel.NoEnforcement => 0,
                            EnforcementLevel.EnforcedWithWarnings => 100,
                            EnforcementLevel.EnforcedWithFines => 100,
                            _ => 0 //C# enums can take on non-declared vaues. Sigh.
                    };

                    SetComponent<BudgetLineComponent>(entity,
                            new BudgetLineComponent
                            {category = "Enforcement",
                            amount = newCost});
                }
            }
        }
    }
}
