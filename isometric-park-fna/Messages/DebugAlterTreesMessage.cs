using Microsoft.Xna.Framework;

using Encompass;

namespace isometricparkfna.Messages {

    public struct DebugAlterTreesMessage : IMessage {
        public int DeltaTrees;
    }
}
