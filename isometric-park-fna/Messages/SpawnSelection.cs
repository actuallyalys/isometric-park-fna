using Microsoft.Xna.Framework;

using Encompass;

namespace isometricparkfna.Messages {
    public struct SpawnSelection : IMessage {
        public Vector2 Start;
    }
}
