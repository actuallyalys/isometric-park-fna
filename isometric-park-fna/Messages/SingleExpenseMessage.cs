using Encompass;

namespace isometricparkfna.Messages 
{
    public struct SingleExpenseMessage : IMessage 
    {
        public string category;
        public decimal amount;
    }
}
