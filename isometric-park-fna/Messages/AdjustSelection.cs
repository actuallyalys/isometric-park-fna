using Microsoft.Xna.Framework;

using Encompass;

namespace isometricparkfna.Messages {

    public enum AdjustmentType {
        None,
        Clear,
        Complete
    }

    public struct AdjustSelection : IMessage {
        public Vector2 End;
        public AdjustmentType Type;
    }
}
