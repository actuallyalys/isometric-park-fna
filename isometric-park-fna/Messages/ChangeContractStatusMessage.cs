using Encompass;

using isometricparkfna.Components;

namespace isometricparkfna.Messages {
    public struct ChangeContractStatusMessage : IMessage, IHasEntity
    {
        public ContractStatus newStatus;
        public Entity Entity { set; get; }
    }
}