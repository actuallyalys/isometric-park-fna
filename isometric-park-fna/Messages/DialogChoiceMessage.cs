
using Encompass;

using isometricparkfna.Utils;
using isometricparkfna.UI;

namespace isometricparkfna.Messages
{

//You must specify both or you get 0 for a window, which is the default window :(
    public struct DialogChoiceMessage : IMessage, IHasEntity
    {

        public Entity Entity { set; get; }
        public int Choice;
    }
}
