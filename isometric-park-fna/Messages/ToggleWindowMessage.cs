using Encompass;

namespace isometricparkfna.Messages
{

//You must specify both or you get 0 for a window, which is the default window :(
    public struct ToggleWindowMessage : IMessage, IHasEntity
    {

        public Window Window;
        public Entity Entity { set; get; }
    }
}
