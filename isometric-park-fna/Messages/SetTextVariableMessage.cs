
using Encompass;

namespace isometricparkfna.Messages
{


    public struct SetTextVariableMessage : IMessage
    {
        public string variable;
        public string value;
    }
}
