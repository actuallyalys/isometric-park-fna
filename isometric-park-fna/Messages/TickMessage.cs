using System;

using Encompass;

namespace isometricparkfna.Messages
{
    struct TickMessage : IMessage {
        public DateTime SimulationDateTime;
    }
}
