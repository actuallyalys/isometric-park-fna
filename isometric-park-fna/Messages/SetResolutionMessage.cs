

using Microsoft.Xna.Framework;
using Encompass;

namespace isometricparkfna.Messages
{
    public struct SetResolutionMessage : IMessage
    {
        public Vector2 resolution;
        public bool fullscreen;

    }
}
