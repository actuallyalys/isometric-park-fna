


using Microsoft.Xna.Framework;
using Encompass;

namespace isometricparkfna.Messages
{
    public struct SetFontMessage : IMessage
    {
        public string fontName;
        public int fontSize;
    }
}
