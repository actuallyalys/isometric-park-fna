
using Microsoft.Xna.Framework;
using Encompass;

namespace isometricparkfna.Messages {
    public struct SpawnContractMessage : IMessage
    {
        public Vector2[] squares;
        public string name;
        public int max_squares;

        public int? min_squares;

    }
}
