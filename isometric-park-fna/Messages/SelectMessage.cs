
using Microsoft.Xna.Framework;
using Encompass;

namespace isometricparkfna.Messages {
    public struct SelectMessage : IMessage, IHasEntity
    {
        public Entity Entity { set; get; }

    }
}
