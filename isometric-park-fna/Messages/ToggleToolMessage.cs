using isometricparkfna.Components;

using Encompass;

namespace isometricparkfna.Messages
{
    public struct ToggleToolMessage : IMessage
    {
        public Tool Tool;
    }
}
