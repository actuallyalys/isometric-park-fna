
using Encompass;

namespace isometricparkfna.Messages
{

//You must specify both or you get 0 for a window, which is the default window :(
    public struct SetWindowVisibilityMessage : IMessage, IHasEntity
    {

        public Entity Entity { set; get; }
        public bool Visibility;
    }
}
