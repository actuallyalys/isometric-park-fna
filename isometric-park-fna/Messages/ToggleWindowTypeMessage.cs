using Encompass;

namespace isometricparkfna.Messages
{

    public enum Window
    {
        Debug,
        Budget,
        Forest,
        News,
        Contracts,
        Contract,
        MainMenu,
        InGameMenu,
        Options,
        NewGame,
        Dialog,
        Graph,
    }


    public struct ToggleWindowTypeMessage : IMessage
    {
        public Window Window;
    }
}
