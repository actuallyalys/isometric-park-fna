using Encompass;
using isometricparkfna.Components;

namespace isometricparkfna.Messages {

    public struct SetTrespassingPolicyMessage : IMessage {
        public EnforcementLevel newEnforcementLevel;
    }
}
