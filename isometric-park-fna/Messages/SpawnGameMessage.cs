using Encompass;

namespace isometricparkfna.Messages {
    public enum DifficultyLevel
    {
        Easy,
        Medium,
        Hard
    }

    public struct SpawnGameMessage : IMessage {
        public DifficultyLevel Difficulty;
    }
}
