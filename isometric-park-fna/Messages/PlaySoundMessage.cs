using Microsoft.Xna.Framework.Audio;

using Encompass;

#nullable enable

namespace isometricparkfna.Messages
{
    public struct PlaySoundMessage : IMessage
    {
        public string SoundName;
        public float? Volume;

    }
}
