using Microsoft.Xna.Framework;
using Encompass;

namespace isometricparkfna.Messages
{
    public struct SetOptionMessage : IMessage
    {
        public ProfanityLevel NewProfanitySetting;
        public float NewSoundEffectVolume;
        public bool NewSoundEffectMuted;
    }
}
