

using Microsoft.Xna.Framework;
using Encompass;

namespace isometricparkfna.Messages {
    public struct JumpCameraMessage : IMessage
    {
        public Vector2 Movement;
    }
}
