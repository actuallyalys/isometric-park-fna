
using Microsoft.Xna.Framework;
using Encompass;

using isometricparkfna.Spawners;

#nullable enable

namespace isometricparkfna.Messages {

    public struct SpawnOrganizationtMessage : IMessage
    {
        public string? name;
        public string? description;

        public bool offersContracts;
        public OrganizationType type;


    }
}
