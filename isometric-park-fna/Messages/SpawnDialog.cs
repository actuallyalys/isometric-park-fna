

using Microsoft.Xna.Framework;
using Encompass;

using isometricparkfna.Spawners;

#nullable enable

namespace isometricparkfna.Messages {
    public struct SpawnDialogMessage : IMessage
    {
        public string Path;
    }
}
