

using Encompass;

namespace isometricparkfna.Messages
{


    public struct GameRateMessage : IMessage//, IHasEntity
    {
        public bool paused;
        public int? rate;
    }
}
