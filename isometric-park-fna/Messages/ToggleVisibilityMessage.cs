
using Encompass;

namespace isometricparkfna.Messages
{

    public enum Element
    {
        Grid,
        Trees
    }

    public struct ToggleVisibilityMessage : IMessage
    {

        public Element Element;
    }
}
