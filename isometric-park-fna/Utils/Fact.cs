
using System;
namespace isometricparkfna.Utils
{
    public struct Fact<T>
    {
        public T Value;

        public bool Known {get;}
        public bool Aware {get;}

        public Fact(T value)
        {
            this.Value = value;
            this.Known = true;
            this.Aware = true;
        }

    }
}
