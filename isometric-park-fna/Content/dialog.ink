
VAR playerSwears = 0
VAR playerRude = 0


VAR GovernorOpinion = 0
VAR HeadOfParksOpinion = 0

VAR GovernorPlayerAddress = "director"
VAR AssistantPlayerAddress = "director"

LIST assistantTraits = shy, esoteric, sweary


EXTERNAL endGame()

EXTERNAL undoPreserve()


//This is needed to make both including and for jumping to dialog sections work:
=== placeholder ===

-> END


=== function alter(ref x, k) ===
	~ x = x + k
	

=== function inc(ref x) ===
	~ x = x + 1
	
=== function dec(ref x) ===
	~ x = x - 1
	
	
//fallbacks

=== function endGame ===
~ return 

=== Once ===

Once upon a time...

 + There were two choices.
 + There were four lines of content.

- They lived happily ever after.
    -> END


=== IntroGovernor ===

Governor: Welcome to your new park, director!

    * [Nice to meet you, I'm  \#playerTitle\# \#playerFormal\#] 
    ~ GovernorPlayerAddress = "\#playerTitle\# \#playerFormal\#"
    -> AfterIntro
    * [Nice to meet you, I'm  \#playerCasual\#]
    ~ GovernorPlayerAddress = "\#playerCasual\#"
    -> AfterIntro

= AfterIntro
* [Okay]
    Governor:  Make sure that you keep visitors happy and the budget in the black! You're currently getting an annual grant out of my budget—it'd sure be nice if you park were self-sufficient so we could drop that expense!
* * [And I need to keep the forest healthy, too, right?] 
~ GovernorOpinion = GovernorOpinion - 1
    Governor: Ummm, yeah
    * * * [\...]
        -> END
* * [Sounds good!]
~ GovernorOpinion = GovernorOpinion + 1

    Governor:  I'll check in soon, {GovernorPlayerAddress}. 


-> END


=== IntroAssistant ===

\#assistantName\#: Hello,  \#playerTitle\# \#playerFormal\#. I'm \#assistantName\#. 
    * [Nice to meet you.]
    ~ AssistantPlayerAddress = " \#playerTitle\# \#playerFormal\#"
        -> Instruction
    * [Call me \#playerCasual\#]
    ~ AssistantPlayerAddress = "\#playerCasual\#"
        -> Instruction
    
= Instruction 
   \#assistantName\#: You can use the mouse or arrow keys to move around, and the plus and minus keys to zoom in and out. B opens the budget and F lets you adjust Forest Policy.
   
        * * [Got it, thanks.]
        
            \#assistantName\#: Bye
            -> END
        * * [How are you?]
        
            \#assistantName\#: \#howdoing\#. Bye, {AssistantPlayerAddress}.
            -> END
    
    
=== BadNewsReact ===


    + [Damn.] {inc(playerSwears)}
    ->->
    + [Fuck.] {inc(playerSwears)}
    ->->
    + [Shoot.] {inc(playerSwears)}
    ->->
    + [*Sigh.* Fine.]
    ->->
    + [Who cares?]
    ->->
    
=== AssistantAcknowlege ===
    + [Thanks.]
        ->->
    + [Mmmm hmm.]
        ->->
    + [Get to it.] {inc(playerRude)}
        ->->
    + [I really appreciate it, \#assistantName\#]
        -> Appreciate
        
 = Appreciate
    \#assistantName\#: {assistantTraits ? shy:...|{~No problem.|Oh, you're welcome, {AssistantPlayerAddress}.|My pleasure.|Any time, {AssistantPlayerAddress}!}}
    ->->
        
=== MassVandalism ===
\#assistantName\#: Bad news, {AssistantPlayerAddress}.
    + [Oh?]
        
        \#assistantName\#: A number of trees have been seriously damaged overnight. 
            + + [How bad are they?]
            
            \#assistantName\#: I'm no botanist, but they're not great. The gouges are pretty deep.
        
            -> BadNewsReact ->
        
            \#assistantName\#: Yeah, it's {assistantTraits ? sweary:fucking|}  awful.
            
            I'll see who's around and get to cleaning.
            
            -> AssistantAcknowlege ->
            
            -> END
            
=== LowFunds ===
\#assistantName\#: Director, our funds are getting dangerously low!

    + [Don't worry about it. I have a plan.]
        -> END
    + [I know.]
        -> END
    + [Stop bothering me with things I already know!] {inc(playerRude)}
        -> END
    + [Have any ideas?]
    
        \#assistantName\#: Consider accepting some contracts. Even if they're not the best terms, they'll keep us from running out of money.
        -> END
    + [What?]
    
        \#assistantName\#:  Yes, we have less than $25,000. We're running the risk of a default.
            -> BadNewsReact ->
                \#assistantName\#:  I know! consider accepting some contracts. Even if they're not the best terms, they'll keep us from running out of money.
                    -> END
                    

=== VeryLowFunds ===
Governor: Director, you have almost no reserve funds. This isn't acceptable.

{VeryLowFunds > 1: I can't believe I'm having this conversation with you again. |} 

    + [I know. I'm working on it.]
        Governor: For your sake, I hope so.
            -> END
    + [Have any ideas?]
        Governor: Accept some contracts. Now's not the time to worry about sustainability, or whatever you natural resources types care about. Just take the deal. They'll keep you from running out of money.
        -> END
    + [What happens if I run out of money?]
        -> WhatHappens
    + [What?]
            -> Surprise       

    = Surprise 
    
    Governor: Is this a *surprise* to you?
        + [Uh, um, no.]
            Governor: Well, I hope you have a plan...
            For your sake.
                -> END
        + [Honestly, yes.]
            Governor: Well! I appreciate your directness. But I hope you have a plan...
            For your sake.
                 -> END
        + [What happens if I run out of money?]
            -> WhatHappens
    = WhatHappens
        Governor: If you run out of money entirely...
        The park will be fine, in the long run. The state department of natural resources will use its funds to bail the park out. Someone from my office will come in and trim the budget.
        You, however, will be asked to resign.
        
            + [And if I don't resign?]
                Governor: I'll fire you.
                    + + [Oh.]
                        -> END
            + [I understand.]
                -> END
                
=== EndLowFunds ===

Governor: I've warned you, {GovernorPlayerAddress}.
    + [I understand. I resign. (Game Over)] { endGame() }
        -> END

=== MeetFriendOfThePark ===


\#friendOfThePark\#: Hi, I'm the president of the Friends of the Park. Nice to meet you!

    * [Nice to meet you!]
        ->->
    * [Ahh.]
        ->->
    * [I've been expecting you.]
    
        \#friendOfThePark\#: Huh!
        ->->

=== Fundraiser ===
{- not MeetFriendOfThePark:
    -> MeetFriendOfThePark ->
    } 
\#friendOfThePark\#: I'm happy to announce the completion of our fundraiser! You won't guess how much we made!

    + [Please don't make me guess.]
        
        \#friendOfThePark\#: It's more fun if you guess.
            + + [ Fine.]
                -> Guess
            + + [Absolutely not.]
                    -> Amount
            + + [Well, I don't enjoy it.]
                    -> Amount
            
    + [Okay...]
        -> Guess

    
    = Guess
    \#friendOfThePark\#: 
    
    + [$1,000?]
    
        \#friendOfThePark\#: Higher
        
        -> Guess
        
    + [$5,000?]
    
        \#friendOfThePark\#: Higher
        
        -> Guess
        
    + [$10,000?]
    
        \#friendOfThePark\#: You're right!
        
        -> Amount
    
    + [$25,000?]
    
        \#friendOfThePark\#: Higher
        
        -> Guess
        
    + [$1 million?]
    
        \#friendOfThePark\#: No, haha. I wish!
        
        -> Guess
    
    + [$1 billion?]
    
        \#friendOfThePark\#: Not quite that high, heh 
        
        -> Guess
        
    + [I give up.]
        
        -> Amount   
    
    = Amount 
    \#friendOfThePark\#:  Anyway. we made $10,000!
        -> END
        
=== GovernmentGrant ===

Governor: Good news, director. You've been awarded a federal grant.

    + [Awesome]
        -> Money
    
    + [Sounds good]
        -> Money
            
    = Money
    Governor: Well, it won't last forever. Don't get dependent on it.
        -> END
        
        
=== MeetHeadofPark ===
Head of Parks: I'm the fucking head of parks, that's who I am, you absolute motherfucker. ->->
        
=== PoorTreeHealth ===
Head of Parks: What the hell do you think you're doing, you absolute piece of shit?!?
    
    
    * {- not MeetHeadofPark } [Sorry, who are you?] -> MeetHeadofPark ->
        -> Screwed
        
    + [Uhh, what the fuck?]
        -> Screwed
        
    = Screwed
    Head of Parks: We're all fucking screwed and it's your fault, you colossal shitheel.
        + [...]
            Head of Parks: Look at this park, it has anxiety! You did that you megadick.
                ++ [...]
                    Head of Parks: Yeah, you'd better stay quiet. I don't want to hear a single word out of your fucking useless knifewound of a mouth.
                            -> END
                            
                            
=== PreserveProgress ===
Head of Parks: Hey, you goddam dunderhead. A quarter of this park is now a preserve! {inc(HeadOfParksOpinion)}
    
    
    * {- not MeetHeadofPark } [Sorry, who are you?] -> MeetHeadofPark
        -> WhatPreserves
    
    + [And you're mad?]
        Head of Parks: No, you vainglorious buffoon! I'm pretty goddamn pleased. -> WhatPreserves
    + [Right!]
        -> WhatPreserves
    
    = WhatPreserves
    Head of Parks: I goddamn love preserves. The peach kind. The strawberry kind. But especially the park kind. The trees are left alone. They fucking flourish! Do you think you could fucking photosynthese with people traipsing all over?

        + [No?]
            Head of Parks: You're goddamn right! You couldn't! Do you know how many preserves have been added since our cuntforsaken governor took office in 2014???
                ++ [Uh one?]
                    Head of Parks: Not even one! -> Zero
                
                ++ [None?]
                    Head of Parks: Bingo, my motherfucker -> Zero
                
                ++ [HellifIknow] {inc(playerSwears)}
                    Head of Parks: Least your honest. -> Zero
                
                ++ [One dozen]
                   Head of Parks:  What? Not even close. -> Zero
                
                ++ [One Hundred]
                   Head of Parks:  What? Not even close. -> Zero
                
                ++ [One billion]
                    Head of Parks: Fuck off. {dec(HeadOfParksOpinion)} -> HeadofParkSideRant 
                    Anyway, it's not a billion!!  -> Zero
        
    = Zero
        Head of Parks: It's zero! Zero new preserves on God's green earth. Or brown earth. I swear to god...
            -> END 

=== PreserveHalf ===
Head of Parks: Hey, you goddamm wunderkind. A whole half of this park is now a preserve! {inc(HeadOfParksOpinion)}

In fact, I want to shake your bloody hand!

    + [Sure!] {inc(HeadOfParksOpinion)}
        -> PostShake
    
    + [Umm..okay]
        -> PostShake
    
    + [No thanks]
        -> PostShake
    
    + [No, you have germs] {dec(HeadOfParksOpinion)}
        Head of Parks: Bloody hell, I wash! -> PostShake
        
    = PostShake
    Head of Parks: Anywyay, you're doing great. If that bloody governor gives you any shit, send him my way. And I think I can scrounge up some money to help you keep the park running.
    
    + [Thanks]
        -> END
        
    

=== HeadofParkSideRant ===
Head of Parks: Are you fucking with me? Really fucking with me? Listen, you motherfucking piss-eared shit-for-brains backroad bureaucrat, I didn't come hear from the fucking capitol building holding up the whole natural resources administrative state like MiracleGrow Atlas to get scoffed at by a tinpot administrator who think 100 acres and a desk makes you some sort of important person. Apologize!
    + [Sorry] ->->
    
    
=== LargePreserve ===
\#assistantName\#: Hi, {AssistantPlayerAddress}, I noticed you reserved quite a bit of space for a nature preserve. Remember that these are unavailable for leisure use or development.
    + [Isn't it great!]
    \#assistantName\#: Ummm. Yeah! It is.
        -> END
    + [It's fine.]
        -> END
    + [Thanks for checking in, \#assistantName\#.]
        -> END
    + [I know what I'm doing! Never question me again!] {inc(playerRude)}
        -> END
    + [Maybe I've overreached. (Undoes Preserve)]  { undoPreserve() }
        -> END
