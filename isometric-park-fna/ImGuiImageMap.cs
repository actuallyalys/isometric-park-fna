
using System;
using System.Diagnostics;

using Num = System.Numerics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using ImGuiNET.SampleProgram.XNA; //for renderer


namespace isometricparkfna
{
    public class ImGuiImageMap : ImageMap
    {
        public IntPtr ImGuiTexture;

        public ImGuiImageMap(int tileWidth, int tileHeight, Texture2D ImageMapTexture,
                             ImGuiRenderer renderer, string filename)
            : base(tileWidth, tileHeight, ImageMapTexture, filename)

        {
            Logging.Info("Constructed ImGuiImageMap.");
            this.ImGuiTexture = renderer.BindTexture(ImageMapTexture);
            Debug.Assert(this.ImGuiTexture != null);
        }
    }
}
