using System;
using System.Collections.Generic;

using Microsoft.Xna.Framework;

namespace isometricparkfna
{

    public class CellMap
    {
        public List<List<Cell>> cells;
        public List<Vector2> WaterCells;

        //Defaults; overridden by settings from FNAGame in practice:
        public int MapWidth = 50;
        public int MapHeight = 50;

        public int ZoneWidth = 10;
        public int ZoneHeight = 10;


        //These coefficients modify values I set based on the original 50x50 maps.
        //Eventually I'll probably allow for multiple map sizes, so instead of changing those constants,
        //I'm adding a multiplier based on the size of the map.
        //
        //What's the difference between these two multipliers?
        //If you double the sides of the map, the linear multiplier is 2,
        //but the area multiplier is 4 (2 x 2).
        public int LinearMultiplier;
        public int AreaMultiplier;

        public int tree_count
        {
            get
            {
                int count = 0;
                foreach (Cell cell in this.tree_cells())
                {
                    count++;
                }
                return count;
            }
        }

        public int tree_capacity
        {
            get
            {
                return MapWidth * MapHeight;
            }
        }

        public int remaining_tree_capacity
        {
            get
            {
                return this.tree_capacity - this.tree_count;
            }
        }

        public CellMap()
        {
            //TileMap(MapWidth, MapHeight);
        }

        public CellMap(int width, int height)
        {
            this.MapWidth = width;
            this.MapHeight = height;

            this.cells = new List<List<Cell>>();

            //Cached:
            this.WaterCells = new List<Vector2>();

            this.LinearMultiplier = (int)(((this.MapWidth / 50) + (this.MapHeight / 50)) / 2);
            this.AreaMultiplier = (int)((this.MapWidth * this.MapHeight) / (50 * 50));

            for (int i = 0; i < height; i++)
            {
                List<Cell> newRow = new List<Cell>();
                for (int j = 0; j < width; j++)
                {
                    newRow.Add(new Cell());
                }

                this.cells.Add(newRow);
            }

        }

        public System.Collections.Generic.IEnumerable<Cell> tree_cells()
        {
            foreach (List<Cell> row in cells)
            {
                foreach (Cell cell in row)
                {
                    if (cell.HasTree)
                    {
                        yield return cell;
                    }
                }
            }
        }

        public System.Collections.Generic.IEnumerable<Cell> tree_cells(int zone)
        {
            foreach (List<Cell> row in cells)
            {
                foreach (Cell cell in row)
                {
                    if (cell.HasTree)
                    {
                        yield return cell;
                    }
                }
            }
        }

        public System.Collections.Generic.IEnumerable<Cell> iterate_cells()
        {
            foreach (List<Cell> row in cells)
            {
                foreach (Cell cell in row)
                {
                    yield return cell;
                }
            }
        }

        public Boolean inBounds(int x, int y)
        {
            return MathUtils.BetweenExclusive(x, 0, MapWidth - 1) && MathUtils.BetweenExclusive(y, 0, MapHeight - 1);
        }

        public System.Collections.Generic.IEnumerable<Cell> iterate_neighbor_cells(int x, int y)
        {
            //iterates over neighbors (clockwise starting at noon/midnight)
            if (inBounds(x, y + 1))
            {
                yield return this.cells[x - 1][y];
            }
            if (inBounds(x + 1, y + 1))
            {
                yield return this.cells[x + 1][y + 1];
            }
            if (inBounds(x + 1, y))
            {
                yield return this.cells[x + 1][y];
            }
            if (inBounds(x + 1, y - 1))
            {
                yield return this.cells[x + 1][y - 1];
            }
            if (inBounds(x, y - 1))
            {
                yield return this.cells[x][y - 1];
            }
            if (inBounds(x - 1, y-1))
            {
                yield return this.cells[x - 1][y-1];
            }
            if (inBounds(x - 1, y))
            {
                yield return this.cells[x - 1][y];
            }
            if (inBounds(x - 1, y + 1))
            {
                yield return this.cells[x - 1][y+1];
            }
        }

        public System.Collections.Generic.IEnumerable<(int, int)> iterate_neighbor_cell_locations(int x, int y)

        {
            //iterates over neighbors (clockwise starting at noon/midnight)
            if (inBounds(x, y + 1))
            {
                yield return (x - 1, y);
            }
            if (inBounds(x + 1, y + 1))
            {
                yield return (x + 1, y + 1);
            }
            if (inBounds(x + 1, y))
            {
                yield return (x + 1, y);
            }
            if (inBounds(x + 1, y - 1))
            {
                yield return (x + 1, y - 1);
            }
            if (inBounds(x, y - 1))
            {
                yield return (x, y - 1);
            }
            if (inBounds(x - 1, y-1))
            {
                yield return (x - 1, y-1);
            }
            if (inBounds(x - 1, y))
            {
                yield return (x - 1, y);
            }
            if (inBounds(x - 1, y + 1))
            {
                yield return (x - 1, y+1);
            }
        }


        public System.Collections.Generic.IEnumerable<Cell> iterate_neighbors(int x, int y)
        {
            foreach (Cell neighbor in this.iterate_neighbor_cells(x, y))
            {
                if (neighbor.HasTree) {
                    yield return neighbor;
                }
            }
        }

        private int countNeighbors(int x, int y)
        {
            int count = 0;
            foreach (Cell neighbor in this.iterate_neighbor_cells(x, y))
            {
                if (neighbor.HasTree) {
                    count++;
                }
            }

            return count;
        }

        public static Vector2 calculateMousegrid(Vector2 normalizedMousePos)
        {
            Vector2 adjust = new Vector2(Tile.TileSpriteWidth / 2, Tile.TileSpriteHeight);
            Vector2 adjustedMousePos = normalizedMousePos - adjust;

            float boardx = ((adjustedMousePos.X / Tile.TileSpriteWidth) + (adjustedMousePos.Y / Tile.TileSpriteHeight));
            float boardy = ((adjustedMousePos.Y / Tile.TileSpriteHeight) - (adjustedMousePos.X / Tile.TileSpriteWidth));

            return new Vector2((int)boardx, (int)boardy);
        }

        public System.Collections.Generic.IEnumerable<Cell> iterate_cells_with_neighbors(int neighbors)
        {
            for (int i = 0; i < MapHeight; i++)
            {
                List<Cell> newRow = new List<Cell>();
                for (int j = 0; j < MapWidth; j++)
                {
                    if (this.countNeighbors(i, j) >= neighbors)
                    {
                        yield return cells[i][j];
                    }
                }
            }
        }

        public System.Collections.Generic.IEnumerable<(int, int)> iterate_cell_locations_with_neighbors(int neighbors)
        {
            for (int i = 0; i < MapHeight; i++)
            {
                List<Cell> newRow = new List<Cell>();
                for (int j = 0; j < MapWidth; j++)
                {
                    if (this.countNeighbors(i, j) >= neighbors)
                    {
                        yield return (i, j);
                    }
                }
            }
        }

        public enum CellStatus {
            Clear,
            LivingTree,
            DeadTree,
            Water
        }

        public enum TreeType {
            Oak,
            GenericEvergreen,
            GenericDeciduous,
            GenericShrub
        }

        public class Cell
        {
            public CellStatus Status {
                get;
                private set;
            }

            public String StatusAdjective {
                get {
                    return this.Status.ToString().Replace("Tree", "");
                }
            }

            public TreeType Type {
                get;
                private set;
            }

            public String TypeName {
                get {
                    return this.Type.ToString().Replace("Generic", "");
                }
            }

            public Boolean HasTree {
                get {
                    return this.Status == CellStatus.LivingTree;
                }
            }

            public Boolean HasWater {
                get {
                    return this.Status == CellStatus.Water;
                }
            }

            public DateTime Planted;

            public void AddTree(DateTime datetime, TreeType type) {
                this.Status = CellStatus.LivingTree;

                this.Planted = datetime;

                this.Type = type;
            }

            public void AddWater() {
                this.Status = CellStatus.Water;
            }

            public void RemoveTree() {
                this.Status = CellStatus.Clear;
            }

            public void MarkTreeDead() {
                this.Status = CellStatus.DeadTree;
            }
        }
    }
}
