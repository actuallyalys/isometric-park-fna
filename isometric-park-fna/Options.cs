
using System.IO;
using Newtonsoft.Json;

namespace isometricparkfna
{

    public enum ProfanityLevel
    {
        Uncensored,
        Minced,
        Removed
    }

    public class Options
    {
        public string fontName;
        public int fontSize;
        public ProfanityLevel profanitySetting;

        //Sound
        public float SoundEffectVolume;
        public bool SoundEffectMuted;

        public Options(string fontName, int fontSize, ProfanityLevel profanitySetting,
                float soundEffectVolume, bool soundEffectMuted)
        {
            this.fontName = fontName;
            this.fontSize = fontSize;
            this.profanitySetting = profanitySetting;

            this.SoundEffectVolume = soundEffectVolume;
            this.SoundEffectMuted = soundEffectMuted;
        }

        public static void writeOptions(string fontName, int fontSize, ProfanityLevel profanitySetting,
                float soundEffectVolume, bool soundEffectMuted)
        {
            var options = new Options(fontName, fontSize, profanitySetting, soundEffectVolume,
                    soundEffectMuted);

            string json = JsonConvert.SerializeObject(options,
                          Formatting.Indented);

            File.WriteAllText(@"options.json", json);
            Logging.Success("Writing options to file.");

        }

        public static Options readOptions()
        {
            var json = File.ReadAllText(@"options.json");
            Logging.Debug(new {json=json}.ToString());

            Options options = JsonConvert.DeserializeObject<Options>(json);

            Logging.Success("Read options.");

            return options;
        }
    }
}
