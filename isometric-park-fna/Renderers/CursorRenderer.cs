using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using isometricparkfna.UI;
using isometricparkfna.Components;

using Encompass;
using SpriteFontPlus;

namespace isometricparkfna.Renderers
{
    public class CursorRenderer : GeneralRenderer
    {
        private SpriteBatch batch;
        private SpriteFont font;

        public CursorRenderer(SpriteBatch batch, SpriteFont font)
        {
            this.batch = batch;
            this.font = font;
        }

        public override void Render()
        {

            Tool selectedTool = Tool.None;

            foreach (ref readonly var entity in ReadEntities<ToolComponent>()) {
                var toolComponent = GetComponent<ToolComponent>(entity);
                var selectedComponent = GetComponent<SelectedComponent>(entity);
                if (selectedComponent.selected) {
                    selectedTool = toolComponent.Tool;
                }
            }

            foreach (ref readonly var entity in ReadEntities<CursorComponent>())
            {

                var cursorComponent = GetComponent<CursorComponent>(entity);
                switch (selectedTool) {
                    case Tool.Dezone:
                        Tile.OutlineSquare(batch, cursorComponent.position.X, cursorComponent.position.Y, Color.Red);
                        break;
                    case Tool.Bulldozer:
                        Tile.OutlineSquare(batch, cursorComponent.position.X, cursorComponent.position.Y, Color.Red);
                        break;
                    case Tool.Preserve:
                        Tile.OutlineSquare(batch, cursorComponent.position.X, cursorComponent.position.Y, Color.Blue);
                        break;
                    default:
                        Tile.OutlineSquare(batch, cursorComponent.position.X, cursorComponent.position.Y, Color.Yellow);
                        break;

                }
                if(selectedTool == Tool.Tower) {
                    Tile.drawTileAt(batch, (int)cursorComponent.position.X, (int)cursorComponent.position.Y, 300, 2, 0.70f, new Color(1.0f, 1.0f, 1.0f, 0.5f));
                }
            }
        }


    }
}
