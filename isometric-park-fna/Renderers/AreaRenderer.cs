using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using isometricparkfna.UI;
using isometricparkfna.Components;

using Encompass;
using SpriteFontPlus;

namespace isometricparkfna.Renderers
{
    public class AreaRenderer : GeneralRenderer
    {
        private SpriteBatch batch;
        private SpriteFont font;

        public AreaRenderer(SpriteBatch batch, SpriteFont font)
        {
            this.batch = batch;
            this.font = font;
        }


        public override void Render()
        {

            var budgetWindow = new BudgetWindow(new Budget {}, this.font, 0, 0);

            foreach (ref readonly var entity in ReadEntities<AreaComponent>())
            {
                var areaComponent = GetComponent<AreaComponent>(entity);
                // var SelectedComponent = GetComponent<SelectedComponent>(entity);
                var selected = GetComponent<SelectedComponent>(entity).selected;

                if (HasComponent<PreserveComponent>(entity) || areaComponent.Tool == Tool.Preserve)
                {
                    Quad.FillSquares(batch, areaComponent.squares, Color.Blue, 0.5f, 0.79f);
                    Tile.DrawOutlinedSquares(batch, areaComponent.squares, Color.Blue);
                }
                else if (!HasComponent<ContractStatusComponent>(entity))
                {
                    Quad.FillSquares(batch, areaComponent.squares, Color.Red, 0.5f, 0.79f);
                    Tile.DrawOutlinedSquares(batch, areaComponent.squares, Color.Red);
                }
                else if ((!HasComponent<ContractStatusComponent>(entity)
                            || GetComponent<ContractStatusComponent>(entity).status == ContractStatus.Accepted)
                        && !selected)
                {
                    Tile.DrawOutlinedSquares(batch, areaComponent.squares, Color.Teal);
                    Quad.FillSquares(batch, areaComponent.squares, Color.Teal, 0.5f, 0.78f);
                }
                else if (HasComponent<ContractStatusComponent>(entity)
                        && selected)
                {
                    Tile.DrawOutlinedSquares(batch, areaComponent.squares, Color.Teal);
                    Quad.FillSquares(batch, areaComponent.squares, Color.Gray, 0.5f, 0.78f);
                }
            }
        }
    }
}
