using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using System;
using System.Collections.Generic;
using System.Linq;

using isometricparkfna.UI;
using isometricparkfna.Utils;
using isometricparkfna.Engines;
using isometricparkfna.Components;
using isometricparkfna.Messages;

using ImGuiNET;

using Encompass;
using SpriteFontPlus;

namespace isometricparkfna.Renderers
{

    class ImGuiWindowRenderer : GeneralRenderer
    {
        private ImGuiWindowBridgeEngine BridgeEngine;
        private FNAGame game;
        private Simulation simulation;

        private GraphicsDeviceManager gdm;

        public ImGuiWindowRenderer(FNAGame game, Simulation simulation, ImGuiWindowBridgeEngine engine, GraphicsDeviceManager gdm)
        {
            this.BridgeEngine = engine;
            this.game = game;
            this.simulation = simulation;
            this.gdm = gdm;
        }

        private ContractDetails
        getContractDetails(Entity entity)
        {

            var name = GetComponent<NameAndDescriptionComponent>(entity).DisplayName;
            var status = GetComponent<ContractStatusComponent>(entity).status;
            var amount = GetComponent<BudgetLineComponent>(entity).amount;
            var tree_delta = GetComponent<TreeDeltaComponent>(entity).deltaTreesName;
            var date_time = GetComponent<ContractStatusComponent>(entity).date;
            var image_index = GetComponent<ImageComponent>(entity).ImageIndex;
            var description = "";

            var square = GetComponent<AreaComponent>(entity).squares[0];

            var age = (this.BridgeEngine.DateTime - date_time).TotalDays;
            var area_size = GetComponent<AreaComponent>(entity).squares.Length;



            if (HasComponent<RelatedOrganizationComponent>(entity))
            {
                var related_organization = GetComponent<RelatedOrganizationComponent>(entity).Entity;

                var name_component = GetComponent<NameAndDescriptionComponent>(related_organization);
                name = name_component.DisplayName;
                description = name_component.Description;
                image_index = GetComponent<ImageComponent>(related_organization).ImageIndex;

            }

            return new ContractDetails {
                entity = entity,
                name = name,
                description = description,
                status = status,
                amount = amount,
                delta_trees = tree_delta,
                image_index = image_index,
                age = age,
                area_size = area_size,
                square = square
            };

        }

        public override void Render()
        {

            var width = gdm.PreferredBackBufferWidth;

            Entity dialogEntity = default;
            DialogComponent dialogNode = default;
            var image_index = 0; // Needs to be here for scoping reasons.

            var dialog_count = 0;
            var dialog_open = false;

            ProfanityLevel profanityLevel = default;
	    OptionsComponent options_component = default;

            foreach (ref readonly var entity in ReadEntities<OptionsComponent>())
            {
		options_component = GetComponent<OptionsComponent>(entity);
                profanityLevel = options_component.ProfanitySetting;
            }


            foreach (ref readonly var entity in ReadEntities<WindowTypeComponent>())
            {
                var window_type = GetComponent<WindowTypeComponent>(entity).type;
                var visible = HasComponent<VisibilityComponent>(entity) ? GetComponent<VisibilityComponent>(entity).visible : false;

                if (visible)
                {
                    switch (window_type)
                    {
                        case Window.Contracts:
                            var contracts = ReadEntities<AreaComponent>();

                            var contract_data = new List<ContractDetails>();

                            foreach (var e in contracts)
                            {
                                if(HasComponent<ContractStatusComponent>(e)) {
                                    contract_data.Add(getContractDetails(e));
                                }
                            }

                            ContractsWindow.Render(this.BridgeEngine.font, this.BridgeEngine, contract_data);
                            break;
                        case Window.Contract:

                            var data = getContractDetails(entity);

                            var area = GetComponent<AreaComponent>(data.entity).squares;

                            ContractWindow.Render(this.BridgeEngine.font, this.BridgeEngine.italicFont, this.BridgeEngine, data);

                            break;
                        case Window.News:
                            NewsWindow.Render(this.BridgeEngine.font, this.simulation, this.BridgeEngine);
                            break;
                        case Window.Forest:
                            ForestWindow.Render(this.BridgeEngine.font, this.simulation, this.BridgeEngine);
                            break;
                        case Window.MainMenu:
                            MainMenu.Render(this.BridgeEngine.font, this.BridgeEngine, width);
                            break;
                        case Window.InGameMenu:
                            InGameMenu.Render(this.BridgeEngine.font, this.BridgeEngine, width);
                            break;
                        case Window.Options:
                            OptionsWindow.Render(this.BridgeEngine.font, this.BridgeEngine.italicFont, this.BridgeEngine, width);
                            break;
                        case Window.NewGame:
                            NewGameWindow.Render(this.BridgeEngine.font, this.BridgeEngine.italicFont, this.BridgeEngine);
                            break;
                        case Window.Graph:
                            GraphWindow.Render(this.BridgeEngine.font, this.simulation, this.BridgeEngine);
                            break;
                        case Window.Dialog:
                            dialog_count++;


                            if (HasComponent<ImageComponent>(entity))
                            {
                                image_index = GetComponent<ImageComponent>(entity).ImageIndex;
                            }
                            else
                            {
                                //Could address this with a placeholder image, but
                                //I think I prefer eliminating the possibility of a placeholder.
                                // Logging.Warning(String.Format("Conversation (Entity ID {0}) without an image.", entity.ID));

                            }
                            var dialog = GetComponent<DialogComponent>(entity);

                            if ((dialogEntity == null)
                                    || (dialogEntity.ID == 0)
                                    || entity.ID < dialogEntity.ID
                                    && dialog.Hydrated)
                            {
                                dialogEntity = entity;
                                dialogNode = dialog;
                            }
                            break;
                        default:
                            Logging.Warning(String.Format("Unknown window type {0} ", window_type));
                            break;
                    }

                }
            }
            if (dialogNode.Hydrated)
            {
                var show = true;
                DialogInterface.RenderDialog(dialogEntity, this.BridgeEngine, ref show, this.BridgeEngine.font, dialogNode, image_index);
                dialog_open = true;
            }
            Logging.Trace(String.Format("{0} dialogs (open? {1}).", dialog_count, dialog_open));
        }


        public void setFont(ImFontPtr font)
        {
            this.BridgeEngine.font = font;
        }
    }
}
