using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using isometricparkfna.UI;
using isometricparkfna.Components;

using Encompass;
using SpriteFontPlus;

namespace isometricparkfna.Renderers
{
    public class BudgetWindowRenderer : GeneralRenderer
    {
        private SpriteBatch batch;
        private SpriteFont font;

        public BudgetWindowRenderer(SpriteBatch batch, SpriteFont font)
        {
            this.batch = batch;
            this.font = font;
        }

        public override void Render()
        {
            var budgetWindow = new BudgetWindow(new Budget { }, this.font, 0, 0);

            foreach (ref readonly var entity in ReadEntities<BudgetComponent>())
            {
                // budgetWindow.update
                // 	this.showBudget = this.budgetWindow.update(mouseCur, this.simulation.latestBudget, this.simulation.previousBudget);
            }
        }
    }
}