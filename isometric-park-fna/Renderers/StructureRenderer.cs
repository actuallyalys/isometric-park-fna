using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using isometricparkfna.UI;
using isometricparkfna.Components;

using Encompass;
using SpriteFontPlus;

namespace isometricparkfna.Renderers
{
    public class StructureRenderer : GeneralRenderer
    {
        private SpriteBatch batch;
        private SpriteFont font;

        public StructureRenderer(SpriteBatch batch, SpriteFont font)
        {
            this.batch = batch;
            this.font = font;
        }

        public override void Render()
        {


            foreach (ref readonly var entity in ReadEntities<StructureComponent>()) 
            {
                var point = GetComponent<PointComponent>(entity);
                Tile.drawTileAt(this.batch, (int)point.Square.X, (int)point.Square.Y,
                        300, 2, 0.79f, Color.White);
            }



        }
    }


}
