using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace isometricparkfna
{

    public class Quad
    {
        static public BasicEffect quadEffect;
        static VertexPositionTexture[] vert;
        static short[] ind;

        public static Texture2D PixelTexture;
        public static Texture2D SolidTexture;


        public static void Initialize(GraphicsDevice device, Texture2D texture)
        {
            Quad.quadEffect = new BasicEffect(device);
            Quad.quadEffect.TextureEnabled = true;

            Quad.vert = new VertexPositionTexture[4];
            Quad.ind = new short[6];
            ind[0] = 0;
            ind[1] = 2;
            ind[2] = 1;
            ind[3] = 1;
            ind[4] = 2;
            ind[5] = 3;

            Quad.PixelTexture = new Texture2D(device, 1, 1);
            Quad.SolidTexture = texture;
        }

        static public void FillSquare(GraphicsDevice device, int x, int y, Color color, float alpha)
        {
            var startX = ((x - y) * Tile.TileSpriteWidth / 2) + (Tile.TileSpriteWidth / 2);
            var startY = (x + y) * Tile.TileSpriteHeight / 2 + (Tile.TileSpriteHeight);
            startX = 0;
            startY = 10;

            vert[0].Position = new Vector3(startX, startY, 0);
            vert[1].Position = new Vector3(startX + 100, startY, 0);
            vert[2].Position = new Vector3(startX, startY + 100, 0);
            vert[3].Position = new Vector3(startX + 100, startY + 100, 0);

            vert[0].TextureCoordinate = new Vector2(0, 0);
            vert[1].TextureCoordinate = new Vector2(1, 0);
            vert[2].TextureCoordinate = new Vector2(0, 1);
            vert[3].TextureCoordinate = new Vector2(1, 1);
            var PixelTexture = new Texture2D(device, 1, 1);

            PixelTexture.SetData<Color>(new Color[] { color });

            quadEffect.Texture = PixelTexture;


            foreach (EffectPass effectPass in Quad.quadEffect.CurrentTechnique.Passes)
            {
                effectPass.Apply();
                device.DrawUserIndexedPrimitives<VertexPositionTexture>(
                    PrimitiveType.TriangleList, Quad.vert, 0, Quad.vert.Length, Quad.ind, 0, Quad.ind.Length / 3);
            }
        }

        public static void FillSquare2(SpriteBatch batch, int x, int y, Color color, float alpha, float depth)
        {

            int screenx = (x - y) * Tile.TileSpriteWidth / 2;
            int screeny = (x + y) * Tile.TileSpriteHeight / 2;

            PixelTexture.SetData<Color>(new Color[] { color });

            color *= alpha;

            batch.Draw(
                Quad.SolidTexture,
                new Rectangle(
                    screenx,
                    screeny,
                    Tile.TileWidth, Tile.TileHeight),
                Tile.GetExtendedSourceRectangle(0, 1),
                color,
                0.0f,
                Vector2.Zero,
                SpriteEffects.None,
                depth);
        }

        public static void FillSquares(SpriteBatch batch, Vector2[] squares, Color color, float alpha, float depth)
        {
            foreach(var square in squares)
            {
                FillSquare2(batch, (int)square.X, (int)square.Y, color, alpha, depth);
            }
        }
    }
}
