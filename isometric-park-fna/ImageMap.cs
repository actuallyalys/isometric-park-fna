using System.Diagnostics;
using System.IO;
using System.Collections.Generic;
using System.Linq;

using YamlDotNet.Serialization;

using Num = System.Numerics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using isometricparkfna.Spawners;

namespace isometricparkfna
{
// public enum Role {
//    Unspecified,
//    Assistant,
//    Governor
// }

    public struct ImageMetadata {
        public string Description;
        public string Subject;
        public string Source;
        public string URL;
        public string Filename;
        public OrganizationType[] OrganizationType;
        public int Subjects;
    }

    public class ImageMap
    {
        public Texture2D ImageMapTexture;

        public int TileWidth;
        public int TileHeight;

        public ImageMetadata[] Metadata;

        public ImageMap(int tileWidth, int tileHeight, Texture2D ImageMapTexture, string filename)
        {
            this.TileHeight = tileHeight;
            this.TileWidth = tileWidth;
            this.ImageMapTexture = ImageMapTexture;

            if (filename != null)
            {
                var deserializer = new DeserializerBuilder()
                .Build();
                var yamlString = File.ReadAllText(filename);
                var input = new StringReader(yamlString);
                var docs = deserializer.Deserialize<List<ImageMetadata>>(input);

                foreach (var meta in docs)
                {
                    Logging.Info(meta.Filename);
                }
                this.Metadata = docs.OrderBy(meta => meta.Filename).ToArray();
                foreach (var meta in this.Metadata)
                {
                    Logging.Info(meta.Filename);
                }
            }

            Debug.Assert(ImageMapTexture != null);
            // Debug.Assert(tileHeight != null);
            // Debug.Assert(tileWidth != null);
            Logging.Info("Constructed ImageMap.");


        }

        public Rectangle GetSourceRectangle(int tileIndex)
        {
            int tileY = tileIndex / (ImageMapTexture.Width / TileWidth);
            int tileX = tileIndex % (ImageMapTexture.Width / TileWidth);

            return new Rectangle(tileX * TileWidth, tileY * TileHeight, TileWidth, TileHeight);
        }
        public Num.Vector2 GetSourceUVStart(int tileIndex)
        {
            int tileX = tileIndex % (ImageMapTexture.Width / TileWidth);
            int tileY = tileIndex / (ImageMapTexture.Width / TileWidth);

            int imageMapX = (tileX) * TileWidth;
            int imageMapY = (tileY) * TileHeight;


            return new Num.Vector2( ((float)imageMapX)/ImageMapTexture.Width,
                                    ((float)imageMapY)/ImageMapTexture.Height);

        }
        public Num.Vector2 GetSourceUVEnd(int tileIndex)
        {
            int tileX = tileIndex % (ImageMapTexture.Width / TileWidth);
            int tileY = tileIndex / (ImageMapTexture.Width / TileWidth);

            int imageMapX = (tileX+1) * TileWidth;
            int imageMapY = (tileY+1) * TileHeight;


            return new Num.Vector2( ((float)imageMapX)/ImageMapTexture.Width,
                                    ((float)imageMapY)/ImageMapTexture.Height);

        }

        public Rectangle GetExtendedSourceRectangle(int tileIndex, int height)
        {
            int tileY = tileIndex / (ImageMapTexture.Width / TileWidth);
            int tileX = tileIndex % (ImageMapTexture.Width / TileWidth);

            return new Rectangle(tileX * TileWidth, tileY * TileHeight, TileWidth, TileHeight*height);
        }

    }
}
