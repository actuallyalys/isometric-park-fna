﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace isometricparkfna
{
    public class FilledRectangle
    {
        public FilledRectangle()
        {
        }

        public static void drawFilledRectangle(SpriteBatch batch, Rectangle coordinates, Color color)
        {
            drawFilledRectangle(batch, coordinates, color, 0, 1);
        }

        public static void drawFilledRectangle(SpriteBatch batch, Rectangle coordinates, Color color, float depth)
        {
            drawFilledRectangle(batch, coordinates, color, depth, 1);

        }

        public static void drawFilledRectangle(SpriteBatch batch, Rectangle coordinates, Color color, float depth, int width)
        {
            batch.Draw(Line.PixelTexture, coordinates, null, color, 0.0f, new Vector2(0, 0), SpriteEffects.None, depth);
        }

        public static void initialize(GraphicsDevice graphics)
        {
            SpriteBatch spriteBatch = new SpriteBatch(graphics);
            Line.PixelTexture = new Texture2D(graphics, 1, 1);

            Line.PixelTexture.SetData<Color>(new Color[] { Color.White });
        }
    }
}
