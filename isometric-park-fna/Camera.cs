﻿using System;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace isometricparkfna
{
    public class Camera
    {

        public Vector2 position;

        float _zoom;
        float[] zoom_levels;


        public static int world_width { get; set; }
        public static int world_height { get; set; }
        public static Vector2 display_offset { get; set; }

        public float zoom
        {
            get { return _zoom; }
            set { _zoom = value; if (zoom < 0.1f) _zoom = 0.1f; } // Negative zoom will flip image
        }


        public Camera()
        {
            this.zoom = 1.0f;
            position = Vector2.Zero;
        }

        public Camera(float[] zoomLevels)
        {
            this.zoom = 1.0f;
            position = Vector2.Zero;
            this.zoom_levels = zoomLevels;
        }

        public void Move(Vector2 change)
        {
            this.position += change;
        }
        public void Jump (Vector2 change)
        {
            this.position = change;
        }

        public Matrix get_transformation(GraphicsDevice graphicsDevice)
        {
            Viewport viewPort = graphicsDevice.Viewport;


            Matrix transformation = Matrix.CreateTranslation(new Vector3(-this.position.X, -this.position.Y, 0)) *
                                    Matrix.CreateScale(new Vector3(this.zoom, this.zoom, 1)) *
                                    Matrix.CreateTranslation(new Vector3(viewPort.Width * 0.5f, viewPort.Height * 0.5f, 0));


            return transformation;
        }

        public Vector2 world_to_screen(Vector2 worldPosition)
        {
            return worldPosition - this.position + Camera.display_offset;
        }

        public Vector2 screen_to_world(Vector2 screenPosition)
        {
            return screenPosition + this.position - Camera.display_offset;
        }

        public float ZoomIn()
        {
            float next_level;

            foreach (float level in this.zoom_levels)
            {
                if(level > this.zoom)
                {
                    next_level = level;
                    this.zoom = next_level;
                    return next_level;
                }
            }

            return this.zoom;

        }

        public float ZoomOut()
        {
            float next_level;

            foreach (float level in this.zoom_levels.Reverse())
            {
                if (level < this.zoom)
                {
                    next_level = level;
                    this.zoom = next_level;
                    return next_level;
                }
            }

            return this.zoom;

        }
    }
}
