
using Encompass;

namespace isometricparkfna.Components {

    public struct BudgetWindowComponent : IComponent {
        public Budget currentBudget;
        public Budget priorBudget;

    }

}
