using Encompass;

namespace isometricparkfna.Components {

    public struct OptionsComponent : IComponent {
        public ProfanityLevel ProfanitySetting;
        public float SoundEffectVolume;
        public bool SoundEffectMuted;

    }
}
