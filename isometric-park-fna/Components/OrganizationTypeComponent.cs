

using Encompass;

using isometricparkfna.Spawners;

namespace isometricparkfna.Components
{
    public struct OrganizationTypeComponent : IComponent
    {
        public OrganizationType type;
    }
}