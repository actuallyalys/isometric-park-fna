using Encompass;

using isometricparkfna.Messages;

namespace isometricparkfna.Components {

    public enum Tool
    {
        None,
        Preserve,
        Dezone,
        Tower,
        Bulldozer
    }

    public struct ToolComponent : IComponent {
        public Tool Tool;
    }
}
