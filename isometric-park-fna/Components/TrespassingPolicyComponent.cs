using Encompass;
using System;
using System.ComponentModel.DataAnnotations;
// using System.ComponentModel;

namespace isometricparkfna.Components {

    public enum EnforcementLevel {
        Unspecified,
        // [Display(Name = "No Enforcement")]
        NoEnforcement,
        // [Display(Name = "Rangers, warnings")]
        EnforcedWithWarnings,
        // [Display(Name = "Rangers, citations")]
        EnforcedWithFines
    }

    public struct TrespassingPolicyComponent : IComponent {
        public EnforcementLevel tresspassingPolicy;
    }
}
