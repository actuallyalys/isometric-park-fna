using Encompass;

namespace isometricparkfna.Components
{

    public struct BudgetComponent : IComponent
    {
        public Budget currentBudget;
        public Budget priorBudget;
    }
}
