
using Encompass;

namespace isometricparkfna.Components
{
    public enum SelectionType {
        None,
        Window, //Selected in a window
        Area, //Selecting an area
        Tool //Selecting a tool
    }

    public struct SelectedComponent : IComponent {
        public bool selected;
        public SelectionType Type;
    }
}
