
using Encompass;

namespace isometricparkfna.Components
{
    public struct RelatedOrganizationComponent : IComponent, IHasEntity
    {
        public Entity Entity {get; set;}
    }
}