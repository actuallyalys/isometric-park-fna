using Microsoft.Xna.Framework;

using Encompass;

namespace isometricparkfna.Components {

    public struct AreaComponent : IComponent {
        public Vector2[] squares;
	public Tool Tool;
    }
}
