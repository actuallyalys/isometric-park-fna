using Encompass;
using System;

namespace isometricparkfna.Components {

    public enum ContractStatus {
        Proposed,
        Accepted, //Can be accepted without being active?
        Active,
        Completed,
        Rejected,
        Broken,
        Expired
    }

    public struct ContractStatusComponent : IComponent {
        public ContractStatus status;
        public DateTime date;
    }
}
