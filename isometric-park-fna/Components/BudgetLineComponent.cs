using Encompass;

namespace isometricparkfna.Components
{

    public struct BudgetLineComponent : IComponent
    {
        public string category;
        public decimal amount;
    }
}
