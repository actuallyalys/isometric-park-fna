using System.Collections.Generic;


using Microsoft.Xna.Framework;

using Encompass;
using isometricparkfna.Utils;
using isometricparkfna.UI;
using isometricparkfna.Engines;

namespace isometricparkfna.Components {

    public struct DialogComponent : IComponent { /*, IHasEntity*/
        public string StartingKnot;
        public string Knot;
        public string CurrentDialog;
        public string CurrentSpeaker;
        public List<string> Options;
        public bool Hydrated {
            get {
                return (this.CurrentDialog != null)
                       && (this.CurrentSpeaker != null);
            }
        }
    }
}
