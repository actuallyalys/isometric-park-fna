using System;

using Encompass;

namespace isometricparkfna.Components
{
    public struct EventComponent : IComponent {
        public DateTime EndEventDate;
    }
}
