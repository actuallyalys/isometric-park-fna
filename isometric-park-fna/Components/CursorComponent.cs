using Microsoft.Xna.Framework;

using Encompass;

namespace isometricparkfna.Components {

    public struct CursorComponent : IComponent {
        public Vector2 position;
        public int size;
    }
}
