
using Encompass;

namespace isometricparkfna.Components
{
    public struct ImageComponent : IComponent
    {
        public int ImageIndex;
    }
}