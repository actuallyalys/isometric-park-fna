using Encompass;

using isometricparkfna.Messages;

namespace isometricparkfna.Components {

    public struct WindowTypeComponent : IComponent {
        public Window type;
    }
}
