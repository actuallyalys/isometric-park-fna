
using Encompass;

#nullable enable

namespace isometricparkfna.Components
{
    public struct NameAndDescriptionComponent : IComponent
    {
        public string DisplayName;
        public string? Description;
    }
}