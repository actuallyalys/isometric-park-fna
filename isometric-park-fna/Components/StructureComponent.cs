
using Encompass;

using isometricparkfna.Messages;

namespace isometricparkfna.Components {
        public enum Structure
        {
            None,
            Tower
        }

    public struct StructureComponent : IComponent {
        public Structure Structure;
        // public DateTime Placed;
    }
}
