using Encompass;

namespace isometricparkfna.Components {

    public struct VisibilityComponent : IComponent {
        public bool visible;
    }
}
