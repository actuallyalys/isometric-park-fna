using Microsoft.Xna.Framework;

using Encompass;

namespace isometricparkfna.Components {

    public struct PointComponent : IComponent {
        public Vector2 Square;
        public Tool Tool;
    }
}
