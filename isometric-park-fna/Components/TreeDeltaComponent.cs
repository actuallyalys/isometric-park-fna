using Encompass;

using isometricparkfna.Utils;

namespace isometricparkfna.Components
{

    public struct TreeDeltaComponent : IComponent
    {
        public Fact<int> deltaTrees;

        public string deltaTreesName
        {
            get
            {
                if (deltaTrees.Value <= -18)
                {
                    return "Unsustainable";
                }
                else if (MathUtils.BetweenExclusive(deltaTrees.Value, -18, -6))
                {
                    return "Moderately unsustainable";
                }
                else if (MathUtils.Between(deltaTrees.Value, -6, 0))
                {
                    return "Somewhat unsustainable";
                }
                else if (deltaTrees.Value == 0)
                {
                    return "Break even";
                }
                else if (deltaTrees.Value > 0)
                {
                    return "Restoration";
                }
                return "???";
            }
        }
    }
}
