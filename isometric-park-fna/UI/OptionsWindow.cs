using System;
using ImGuiNET;

using Num = System.Numerics;

using Microsoft.Xna.Framework;

using isometricparkfna.Components;
using isometricparkfna.Engines;
using isometricparkfna.Messages;
using isometricparkfna.UI;

namespace isometricparkfna.UI
{

    public static class OptionsWindow
    {

        public static bool hadFocus = false;

        public static bool newFullscreen;
        public static Vector2 newResolution;
        public static float newSoundEffectVolume;
        public static bool newSoundEffectsMute;

        private static string fontName = "Iosevka";
        private static int fontSize = 15;
        private static ProfanityLevel profanityLevel = default;


        private static bool origFullscreen = OptionsWindow.newFullscreen;
        private static Vector2 origResolution = OptionsWindow.newResolution;

        private static string origFontName = OptionsWindow.fontName;
        private static int origFontSize = OptionsWindow.fontSize;
        private static ProfanityLevel origProfanityLevel = OptionsWindow.profanityLevel;
        private static float origSoundEffectVolume;
        private static bool origSoundEffectsMute;

        private static float prevSoundEffectVolume; //Tracks the previously set. (As opposed to origSoundEffectVolume, which is the previously *applied* volume.

        public static void Initialize(Vector2 resolution, bool fullscreen, ProfanityLevel profanityLevel, Options startingOptions) 
        {

            OptionsWindow.newFullscreen = fullscreen;
            OptionsWindow.newResolution = resolution;
            OptionsWindow.profanityLevel = profanityLevel;

            newSoundEffectVolume = startingOptions.SoundEffectVolume;
            newSoundEffectsMute = startingOptions.SoundEffectMuted;
            prevSoundEffectVolume = startingOptions.SoundEffectVolume;
        }

        public static void Render(ImFontPtr font, ImFontPtr italicFont, ImGuiWindowBridgeEngine bridgeEngine, int width)
        {
            ImGui.GetStyle().WindowMenuButtonPosition = ImGuiDir.None;
            bool newShow = true;

            StyleSets.defaultSet.push();


            ImGui.PushFont(font);

            if(OptionsWindow.hadFocus)
            {
                ImGui.PushStyleColor(ImGuiCol.Text, StyleSets.white);
            }
            ImGui.Begin("Options", ref newShow, ImGuiWindowFlags.NoCollapse | ImGuiWindowFlags.NoSavedSettings | ImGuiWindowFlags.AlwaysAutoResize | ImGuiWindowFlags.NoResize | ImGuiWindowFlags.NoCollapse);

            if (OptionsWindow.hadFocus)
            {
                ImGui.PopStyleColor();
            }
            OptionsWindow.hadFocus = ImGui.IsWindowFocused();

            ImGui.PushFont(italicFont);
            ImGui.Text("Graphics");
            ImGui.PopFont();

            ImGui.Text("Resolution:");

            ImGui.SameLine();

            if (ImGui.BeginCombo("##resolutions", string.Format("{0}x{1}",
                                 newResolution.X, newResolution.Y)))
            {

                foreach(var (width_option, height_option) in new[] {(1280, 640),
                        (640, 320), (960, 480), (1600, 800),
                        (2560, 1440), (1280, 720), (1920, 1080)
                                                                   })
                {
                    if (ImGui.Selectable(string.Format("{0}x{1}", width_option, height_option)))
                    {
                        newResolution.X = width_option;
                        newResolution.Y = height_option;
                    }
                }

                ImGui.EndCombo();
            }
            ImGui.Text("Font:\t");

            ImGui.SameLine();

            if (ImGui.BeginCombo("##Font", fontName))
            {

                foreach(var font_name in new[] {"Iosevka", "Roboto"})
                {
                    if(ImGui.Selectable(font_name))
                    {
                        OptionsWindow.fontName = font_name;
                    }
                }

                ImGui.EndCombo();
            }
            ImGui.Text("Size:\t");
            ImGui.SameLine();
            if (ImGui.BeginCombo("##FontSize", fontSize.ToString()))
            {

                foreach(var size in new[] {9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25})
                {
                    if(ImGui.Selectable(size.ToString()))
                    {
                        OptionsWindow.fontSize = size;
                    }
                }

                ImGui.EndCombo();
            }

            ImGuiIOPtr io = ImGui.GetIO();


            ImGui.Text("Scale:\t");

            ImGui.SameLine();
            ImGui.DragFloat("##Scale", ref io.FontGlobalScale, 0.005f, 0.2f, 5.0f, "%.2f");

            ImGui.SameLine();
            Widgets.LabelTooltip("(?)", "Adjust this if increasing font size isn't enough.");

            ImGui.Checkbox("Fullscreen", ref newFullscreen);



            ImGui.Separator();

            ImGui.PushFont(italicFont);
            ImGui.Text("Audio");
            ImGui.PopFont();
            ImGui.Text("Sound Effects:\t");

            ImGui.SameLine();
            ImGui.DragFloat("##Sfx", ref newSoundEffectVolume, 0.005f, 0.0f, 1.0f, "%.2f");

            if ((newSoundEffectVolume != prevSoundEffectVolume) && ImGui.IsItemDeactivatedAfterEdit()) {
                bridgeEngine.playSoundMessages.Add(new PlaySoundMessage { SoundName = "Bell", Volume = newSoundEffectVolume});
                prevSoundEffectVolume = newSoundEffectVolume;
            }

            ImGui.SameLine();
            ImGui.Checkbox("Mute", ref newSoundEffectsMute);

            ImGui.Separator();

            ImGui.PushFont(italicFont);
            ImGui.Text("Text");
            ImGui.PopFont();

            ImGui.Text("Profanity:");
            ImGui.SameLine();
            if (ImGui.BeginCombo("##Profanity", profanityLevel.ToString()))
            {

                foreach(var level in Enum.GetValues(typeof(ProfanityLevel)))
                {
                    if(ImGui.Selectable(level.ToString()))
                    {
                        bridgeEngine.setOptionMessages.Add(new SetOptionMessage { NewProfanitySetting = (ProfanityLevel)level,
					NewSoundEffectVolume = origSoundEffectVolume,
					NewSoundEffectMuted = origSoundEffectsMute
					});
                        OptionsWindow.profanityLevel = (ProfanityLevel)level;
                    }
                }

                ImGui.EndCombo();
            }
            ImGui.SameLine();
            Widgets.LabelTooltip("(?)", "Removes profanity from the game, if you must.");

            ImGui.Separator();

            if (ImGui.Button("Okay"))
            {
                bridgeEngine.typeMessages.Add(new ToggleWindowTypeMessage {Window = Window.Options});
                bridgeEngine.resolutionMessages.Add(new SetResolutionMessage {
                    resolution = newResolution,
                    fullscreen = newFullscreen
                });
                bridgeEngine.fontMessages.Add(new SetFontMessage {
                    fontSize = OptionsWindow.fontSize,
                    fontName = OptionsWindow.fontName
                });

                origFullscreen = OptionsWindow.newFullscreen;
                origResolution = OptionsWindow.newResolution;

                origFontName = OptionsWindow.fontName;
                origFontSize = OptionsWindow.fontSize;
                origProfanityLevel = OptionsWindow.profanityLevel;
                origSoundEffectVolume = OptionsWindow.newSoundEffectVolume;
                origSoundEffectsMute = OptionsWindow.newSoundEffectsMute;


                bridgeEngine.setOptionMessages.Add(new SetOptionMessage { NewProfanitySetting = (ProfanityLevel)profanityLevel,
				NewSoundEffectVolume = newSoundEffectVolume,
				NewSoundEffectMuted = newSoundEffectsMute});
	    }
            
            ImGui.SameLine();
            if (ImGui.Button("Apply"))
            {
                bridgeEngine.resolutionMessages.Add(new SetResolutionMessage {
                    resolution = newResolution,
                    fullscreen = newFullscreen
                });
                bridgeEngine.fontMessages.Add(new SetFontMessage {
                    fontSize = OptionsWindow.fontSize,
                    fontName = OptionsWindow.fontName
                });

                origFullscreen = OptionsWindow.newFullscreen;
                origResolution = OptionsWindow.newResolution;

                origFontName = OptionsWindow.fontName;
                origFontSize = OptionsWindow.fontSize;
                origProfanityLevel = OptionsWindow.profanityLevel;
                origSoundEffectVolume = OptionsWindow.newSoundEffectVolume;
                origSoundEffectsMute = OptionsWindow.newSoundEffectsMute;

                bridgeEngine.setOptionMessages.Add(new SetOptionMessage { NewProfanitySetting = (ProfanityLevel)profanityLevel,
				NewSoundEffectVolume = newSoundEffectVolume,
				NewSoundEffectMuted = newSoundEffectsMute});
            }

            ImGui.SameLine();
            if (ImGui.Button("Cancel")) {
                bridgeEngine.typeMessages.Add(new ToggleWindowTypeMessage {Window = Window.Options});

                OptionsWindow.newFullscreen = origFullscreen;
                OptionsWindow.newResolution = origResolution;

                OptionsWindow.fontName = origFontName;
                OptionsWindow.fontSize = origFontSize;
                OptionsWindow.profanityLevel = origProfanityLevel;
                OptionsWindow.newSoundEffectVolume = origSoundEffectVolume;
                OptionsWindow.newSoundEffectsMute = origSoundEffectsMute;


                if ((origSoundEffectsMute != newSoundEffectsMute)
                        | (origSoundEffectVolume != newSoundEffectVolume)) {
                    bridgeEngine.setOptionMessages.Add(new SetOptionMessage { NewProfanitySetting = (ProfanityLevel)origProfanityLevel,
                            NewSoundEffectVolume = origSoundEffectVolume,
                            NewSoundEffectMuted = origSoundEffectsMute});
                }
            }


            ImGui.End();

            ImGui.GetStyle().WindowMenuButtonPosition = ImGuiDir.Left;
            StyleSets.defaultSet.pop();
            ImGui.PopFont();

            if (!newShow)
            {
                bridgeEngine.typeMessages.Add(new ToggleWindowTypeMessage {Window = Window.Options});
            }
	}

        public static void setFont(string fontName, int fontSize)
        {
            OptionsWindow.fontName = fontName;
            OptionsWindow.fontSize = fontSize;
        }
    }
}
