using System;

using ImGuiNET;

using Num = System.Numerics;

using isometricparkfna.Engines;
using isometricparkfna.Messages;
using isometricparkfna.Components;

namespace isometricparkfna.UI
{

    public static class Menu
    {

        public const int MENU_BAR_HEIGHT = 20;


        public static bool activeButton(string label, bool active, Num.Vector4 activeColor, Num.Vector4 activeTextColor) {

            if (active) {
                ImGui.PushStyleColor(ImGuiCol.Button, activeColor);
                ImGui.PushStyleColor(ImGuiCol.ButtonHovered, activeColor);
                ImGui.PushStyleColor(ImGuiCol.Text, activeTextColor);
            }

            var result = ImGui.Button(label);
            if (active) {
                ImGui.PopStyleColor(3);
            }

            return result;
        }

        private static bool activeButtonIndicator(string label, bool active, Num.Vector4 activeColor, Num.Vector4 activeTextColor, string indicator, Num.Vector4 indicatorColor, string tooltip) {

            if (active) {
                ImGui.PushStyleColor(ImGuiCol.Button, activeColor);
                ImGui.PushStyleColor(ImGuiCol.ButtonHovered, activeColor);
                ImGui.PushStyleColor(ImGuiCol.Text, activeTextColor);
            }

            var result = ImGui.Button(String.Format("{0}    ", label));
            ImGui.SetItemAllowOverlap();


            if (active) {
                ImGui.PopStyleColor(3);
            }

            Widgets.Indicator(indicator, indicatorColor);

            Widgets.Tooltip(tooltip);


            return result;
        }

        private static bool contractsButton(ImGuiWindowBridgeEngine bridgeEngine)
        {
            if (bridgeEngine.showContractIndicator)
            {
                return Menu.activeButtonIndicator("\ue0c2 Contracts", bridgeEngine.windowStatuses[Window.Contracts], StyleSets.selected, StyleSets.white, "(!)", StyleSets.red, "Contract offer is about to expire:\n" + string.Join("\n", bridgeEngine.contracts));
            }
            else
            {
                return Menu.activeButton("\ue0c2 Contracts", bridgeEngine.windowStatuses[Window.Contracts], StyleSets.selected, StyleSets.white);
            }
        }

        private static bool budgetButton(ImGuiWindowBridgeEngine bridgeEngine)
        {
            if (bridgeEngine.showBudgetNegative)
            {
                return Menu.activeButtonIndicator("$ Budget", bridgeEngine.windowStatuses[Window.Budget], StyleSets.selected, StyleSets.white, "(!!)", StyleSets.red, "Funds are negative!");
            }
            else if (bridgeEngine.showBudgetLow)
            {
                return Menu.activeButtonIndicator("$ Budget", bridgeEngine.windowStatuses[Window.Budget], StyleSets.selected, StyleSets.white, "(!)", StyleSets.red, "Funds are low.");
            }
            else
            {
                return Menu.activeButton("$ Budget", bridgeEngine.windowStatuses[Window.Budget], StyleSets.selected, StyleSets.white);
            }
        }

        public static void Render(ImFontPtr font, int width, ImGuiWindowBridgeEngine bridgeEngine, ref bool quit, ref bool paused, ref int rate, ref bool showBudget, string header)
        {
            ImGui.PushFont(font);

            ImGui.GetStyle().WindowMenuButtonPosition = ImGuiDir.None;

            StyleSets.defaultSet.push();
            ImGui.PushStyleColor(ImGuiCol.MenuBarBg, StyleSets.grey);

            if (ImGui.BeginMainMenuBar())
            {
                ImGui.Text(header);


                var dimensions = ImGui.CalcTextSize("X Tower X Preserve X Dezone X Bulldozer | X Graph X Contracts $ Budget X Forest X News X | Pause 1 2 3 4 5") ;

                // ImGui.SetCursorPosX(width - 520);
                // Add 12 pixels for each button, plus separator
                ImGui.SetCursorPosX(width - (dimensions.X + 11*15));

                if (Menu.activeButton("\ue06e Tower", bridgeEngine.toolStatuses[Tool.Tower], StyleSets.selected, StyleSets.white))
                {
                    bridgeEngine.toggleToolMessages.Add(new ToggleToolMessage {Tool = Tool.Tower});
                }
                if (Menu.activeButton("\ue099 Preserve", bridgeEngine.toolStatuses[Tool.Preserve], StyleSets.selected, StyleSets.white))
                {
                    bridgeEngine.toggleToolMessages.Add(new ToggleToolMessage {Tool = Tool.Preserve});
                }
                if (!bridgeEngine.toolStatuses[Tool.Dezone]) {
                    if (Menu.activeButton("\ue122 Bulldozer", bridgeEngine.toolStatuses[Tool.Bulldozer], StyleSets.selected, StyleSets.white))
                    {
                        bridgeEngine.toggleToolMessages.Add(new ToggleToolMessage {Tool = Tool.Bulldozer});
                    }
                }
                else {
                    if (Menu.activeButton("\ue058 Dezone", bridgeEngine.toolStatuses[Tool.Dezone], StyleSets.selected, StyleSets.white))
                    {
                        bridgeEngine.toggleToolMessages.Add(new ToggleToolMessage {Tool = Tool.Dezone});
                    }
                }


                ImGui.SetCursorPosX(ImGui.GetCursorPosX() - 9); 
                if (ImGui.Button("\ue01d"))
                {
                    ImGui.OpenPopup("BulldozerChildren");
                }
                if (ImGui.BeginPopup("BulldozerChildren"))
                {
                    if (Menu.activeButton("\ue122 Bulldozer", bridgeEngine.toolStatuses[Tool.Bulldozer], StyleSets.selected, StyleSets.white))
                    {
                        bridgeEngine.toggleToolMessages.Add(new ToggleToolMessage {Tool = Tool.Bulldozer});
                    }
                    if (Menu.activeButton("\ue058 Dezone", bridgeEngine.toolStatuses[Tool.Dezone], StyleSets.selected, StyleSets.white))
                    {
                        bridgeEngine.toggleToolMessages.Add(new ToggleToolMessage {Tool = Tool.Dezone});
                        ImGui.CloseCurrentPopup();
                    }
                    if (ImGui.Button("Cancel"))
                    {
                        ImGui.CloseCurrentPopup();
                    }
                    ImGui.EndPopup();
                }
                ImGui.Text("|");

                if (Menu.activeButton("\ue03e Graph", bridgeEngine.windowStatuses[Window.Graph], StyleSets.selected, StyleSets.white))
                {
                    bridgeEngine.typeMessages.Add(new ToggleWindowTypeMessage {Window = Window.Graph});

                }

                if (contractsButton(bridgeEngine))
                {
                    Logging.Trace("Contracts toggled.");
                    // Logging.Spy(bridgeEngine.windowStatuses, "statuses");
                    bridgeEngine.typeMessages.Add(new ToggleWindowTypeMessage {Window = Window.Contracts});
                }
                //Budget isn't connected to an entity yet:
                if (budgetButton(bridgeEngine)) //" \ue0b2 ", StyleSets.red, "New budget"
                {
                    bridgeEngine.typeMessages.Add(new ToggleWindowTypeMessage {Window = Window.Budget});

                }
                if (Menu.activeButton("\ue124 Forest", bridgeEngine.windowStatuses[Window.Forest], StyleSets.selected, StyleSets.white))
                {
                    bridgeEngine.typeMessages.Add(new ToggleWindowTypeMessage {Window = Window.Forest});

                }
                if (Menu.activeButton("\ue0bf News", bridgeEngine.windowStatuses[Window.News], StyleSets.selected, StyleSets.white))
                {
                    bridgeEngine.typeMessages.Add(new ToggleWindowTypeMessage {Window = Window.News});
                }

                ImGui.Text("|");

                if (Menu.activeButton("\ue0ac Pause", paused, StyleSets.selected, StyleSets.white ))
                {
                    paused = !paused;
                }
                if (Menu.activeButton("1", (rate == 0), StyleSets.selected, StyleSets.white))
                {
                    paused = false;
                    rate = 0;
                }
                else if (Menu.activeButton("2", (rate == 1), StyleSets.selected, StyleSets.white))
                {
                    paused = false;
                    rate = 1;
                }
                else if (Menu.activeButton("3", (rate == 2), StyleSets.selected, StyleSets.white))
                {
                    paused = false;
                    rate = 2;
                }
                else if (Menu.activeButton("4", (rate == 3), StyleSets.selected, StyleSets.white))
                {
                    paused = false;
                    rate = 3;
                }
#if DEBUG
                else if (Menu.activeButton("5", (rate == 4), StyleSets.selected, StyleSets.white))
                {
                    paused = false;
                    rate = 4;
                }
#endif

                ImGui.EndMainMenuBar();

                ImGui.GetStyle().WindowMenuButtonPosition = ImGuiDir.Left;

                StyleSets.defaultSet.pop();
                ImGui.PopStyleColor(1);
                ImGui.PopFont();
            }
        }
    }
}
