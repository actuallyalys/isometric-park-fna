using System;
using Num = System.Numerics;

using ImGuiNET;

using isometricparkfna.Messages;
using isometricparkfna.Components;
using isometricparkfna.Engines;

namespace isometricparkfna.UI
{

    public static class ForestWindow
    {
        public static bool hadFocus = false;

        private static string enforceTresspassing2 = "Unspecified";

        public static void Render(ImFontPtr font, Simulation sim, ImGuiWindowBridgeEngine engine)
        {
            bool newShow = true;
            if (newShow)
            {
                ImGui.PushFont(font);

                ImGui.GetStyle().WindowMenuButtonPosition = ImGuiDir.None;
                StyleSets.defaultSet.push();


                if(ForestWindow.hadFocus)
                {
                    ImGui.PushStyleColor(ImGuiCol.Text, StyleSets.white);
                }

                ImGui.Begin("Forest Policy", ref newShow, ImGuiWindowFlags.NoResize | ImGuiWindowFlags.NoCollapse | ImGuiWindowFlags.AlwaysAutoResize | ImGuiWindowFlags.NoSavedSettings);
                if (ForestWindow.hadFocus)
                {
                    ImGui.PopStyleColor();
                }
                ForestWindow.hadFocus = ImGui.IsWindowFocused();


                int new_tree_planting = sim.tree_planting;
                ImGui.Text("Tree Planting: ");
                ImGui.SameLine();
                ImGui.SliderInt("##tree_planting", ref new_tree_planting, 0, Simulation.MAX_TREES_TO_PLANT, string.Format("%d (${0})", new_tree_planting*Simulation.TREE_PLANT_COST));
                sim.tree_planting = new_tree_planting;

                int new_tree_clearing = sim.tree_clearing;
                ImGui.Text("Tree Clearing: ");
                ImGui.SameLine();
                ImGui.SliderInt("##tree_clearing", ref new_tree_clearing, 0, Simulation.MAX_TREES_TO_CLEAR, string.Format("%d (${0})", new_tree_clearing*Simulation.TREE_CLEAR_COST));
                sim.tree_clearing = new_tree_clearing;

                ImGui.Text("Tresspassing policy:");
                ImGui.SameLine();

                if (ImGui.BeginCombo("##tresspassing", enforceTresspassing2))
                {

                    // if ( ImGui.Selectable("None"))
                    // {
                    // 	enforceTresspassing2 = "None";
                    // }
                    // if (ImGui.Selectable("Rangers, warnings"))
                    // {
                    // 	enforceTresspassing2 = "Rangers, warnings";
                    // }
                    // if (ImGui.Selectable("Rangers, citations"))
                    // {
                    // 	enforceTresspassing2 = "Rangers, citations";
                    // }

                    foreach (EnforcementLevel option in EnforcementLevel.GetValues(typeof(EnforcementLevel)))
                    {
                        if (ImGui.Selectable(option.ToString()))
                        {
                            enforceTresspassing2 = option.ToString();
                        }
                    }

                    ImGui.EndCombo();
                }
                ImGui.SameLine();
                ImGui.TextDisabled("(?)");
                if (ImGui.IsItemHovered())
                {
                    var rect = ImGui.GetItemRectMax();
                    ImGui.SetNextWindowPos(rect + new Num.Vector2(15, 0));
                    ImGui.BeginTooltip();
                    ImGui.Text("Enforcing tresspassing lowers upkeep, but costs money.\n\nRacial minorities may also be targeted disproportionately.");
                    ImGui.EndTooltip();
                }


                ImGui.Separator();

                ImGui.Text(string.Format("Crowded Trees: {0}", sim.crowded_trees));
                ImGui.Text(string.Format("Dead Trees: {0}", sim.dead_trees));
                ImGui.Text(string.Format("Percent Healthy Trees: {0:F2}", sim.healthy_percent));
                ImGui.Text(string.Format("Average Age of Trees: {0:F2}", sim.average_tree_age));
                ImGui.Text(string.Format("Max Age of Trees: {0:F2}", sim.max_tree_age));

                if (ImGui.Button("Okay"))
                {
                    newShow = false;
                    Logging.Spy(new {newEnforcementLevel = (EnforcementLevel)Enum.Parse(typeof(EnforcementLevel), enforceTresspassing2)});
                    engine.trespassingPolicyMessages.Add(new SetTrespassingPolicyMessage {newEnforcementLevel = (EnforcementLevel)Enum.Parse(typeof(EnforcementLevel), enforceTresspassing2)});
                }

                ImGui.End();
                ImGui.GetStyle().WindowMenuButtonPosition = ImGuiDir.Left;
                StyleSets.defaultSet.pop();
                ImGui.PopFont();
            }
            if (!newShow)
            {
                engine.typeMessages.Add(new ToggleWindowTypeMessage {Window = Window.Forest });
            }
        }
    }
}
