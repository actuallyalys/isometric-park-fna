﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace isometricparkfna.UI
{
    public class BudgetWindow
    {
        private Budget budget;
        private Budget previous_budget;
        private SpriteFont font;
        public int x;
        public int y;


        private Vector2 mouseEnd;
        private Vector2 mouseStart;
        private MouseState mousePrev;

        private static int bar_height = 25;
        private static int height = 500;
        private static int width = 700;

        public BudgetWindow(Budget budget, SpriteFont font, int start_x, int start_y)
        {

            this.budget = budget;
            this.font = font;
            this.x = start_x;
            this.y = start_y;
        }

        public bool update(MouseState mouseCur, Budget budget, Budget previous_budget)
        {
            this.budget = budget;
            this.previous_budget = previous_budget;

            if ((mouseCur.LeftButton == ButtonState.Released)
			    && (mousePrev.LeftButton == ButtonState.Pressed)
			    && MathUtils.BetweenExclusive(mouseCur.X, width+x-20, width+x)
			    && MathUtils.BetweenExclusive(mouseCur.Y, y+bar_height, y+bar_height+20)) {

                return false;

            }
            else if ((mouseCur.LeftButton == ButtonState.Pressed)
                     && MathUtils.BetweenExclusive(mouseCur.X, x, width+x)
                     && MathUtils.BetweenExclusive(mouseCur.Y, y, 500 + y))
            {
                if (mousePrev.LeftButton == ButtonState.Released)
                {
                    this.mouseStart = new Vector2(mouseCur.X, mouseCur.Y);

                }
                else
                {
                    this.mouseEnd = new Vector2(mouseCur.X, mouseCur.Y);
                    this.x = MathUtils.Clamp(this.x + (int)(this.mouseEnd.X - this.mouseStart.X), 0, width);
                    this.y = MathUtils.Clamp(this.y + (int)(this.mouseEnd.Y - this.mouseStart.Y), 0, 400);
                }
            }

            this.mouseStart = new Vector2(mouseCur.X, mouseCur.Y);

            this.mousePrev = mouseCur;

            return true;
        }

	public String line_format(String name, decimal first, decimal second) {
		var between_name_col = 20 - name.Length;
		int between_cols = 6;
		var first_formatted = String.Format("${0:}", first).PadLeft(6, '.');
		var second_formatted = String.Format("${0:}", second).PadLeft(6, '.');

		var formatted = String.Format("{0:}{1:}{2,6:}{3:}{4,6:}", name, new String('.', between_name_col), first_formatted, new String('.', between_cols), second_formatted);

		return formatted;
	}

        public void draw(SpriteBatch batch)
        {



            FilledRectangle.drawFilledRectangle(batch, new Rectangle(x - 20, y+bar_height, 20, height), Color.White);
            Line.drawLine(batch, new Vector2(x, y + bar_height), new Vector2(x, y + bar_height + height), Color.Gray);
            FilledRectangle.drawFilledRectangle(batch, new Rectangle(x + width, y+bar_height, 20, height), Color.White);
            Line.drawLine(batch, new Vector2(x + width, y + bar_height), new Vector2(x + width, y + bar_height + height), Color.Gray);

            for (int i = 1; i <= (height / bar_height); i++)
            {
                Rectangle position = new Rectangle(this.x, bar_height * i + this.y,
                                                   width, bar_height);

                if ((i % 2) == 0)
                {
                    FilledRectangle.drawFilledRectangle(batch, position, Color.LightGreen, 0.99f);
                }
                else
                {
                    FilledRectangle.drawFilledRectangle(batch, position, Color.White, 0.99f);
                }
            }

            Vector2 dimensions = font.MeasureString("X");
            batch.DrawString(font, "X", new Vector2(x+width -20  + (dimensions.X/2), y+bar_height), Color.Black);
            FilledRectangle.drawFilledRectangle(batch, new Rectangle(x + width - 20, y+bar_height, 20, 20), Color.LightGray);

            batch.DrawString(font, String.Format("BUDGET REPORT FOR {0:MMMMM yyyy}", this.budget.DateTime), new Vector2(x, bar_height * 1 + y), Color.Black);

            batch.DrawString(font, String.Format("Starting Funds.........${0:}", this.budget.money), new Vector2(x, bar_height * 2 + y), Color.Black);

            batch.DrawString(font, String.Format("REVENUE", this.budget.upkeep), new Vector2(x, bar_height * 4 + y), Color.Black);
            batch.DrawString(font, line_format("Subsidy", this.budget.subsidy, this.previous_budget.subsidy), new Vector2(x, bar_height * 5 + y), Color.Black);
            batch.DrawString(font, line_format("Contracts", this.budget.contracts, this.previous_budget.contracts), new Vector2(x, bar_height * 6 + y), Color.Black);
            batch.DrawString(font, line_format("Misc", this.budget.misc, this.previous_budget.misc), new Vector2(x, bar_height * 7 + y), Color.Black);

            batch.DrawString(font, "EXPENSES", new Vector2(x, bar_height * 10 + y), Color.Black);
            batch.DrawString(font, line_format("Upkeep", this.budget.upkeep, this.previous_budget.upkeep), new Vector2(x, bar_height * 11 + y), Color.Black);
            batch.DrawString(font, line_format("Tree Planting", this.budget.tree_planting, this.previous_budget.tree_planting), new Vector2(x, bar_height * 12 + y), Color.Black);
            batch.DrawString(font, line_format("Tree Clearing", this.budget.tree_clearing, this.previous_budget.tree_clearing), new Vector2(x, bar_height * 13 + y), Color.Black);
            batch.DrawString(font, line_format("Enforcement", this.budget.enforcement, this.previous_budget.enforcement), new Vector2(x, bar_height * 14 + y), Color.Black);
            batch.DrawString(font, line_format("Construction", this.budget.construction, this.previous_budget.construction), new Vector2(x, bar_height * 15 + y), Color.Black);

            Color cashflow_color = Color.Black;
            if (this.budget.cashflow < 0) {
                cashflow_color = Color.Red;
            }
            Color final_color = Color.Black;
            if (this.budget.final_money < 0) {
                final_color = Color.Red;
            }

            batch.DrawString(font, line_format("Cashflow", this.budget.cashflow, this.previous_budget.cashflow), new Vector2(x, bar_height * 17 + y), cashflow_color);
            batch.DrawString(font, line_format("Ending Funds", this.budget.final_money, this.previous_budget.final_money), new Vector2(x, bar_height * 18 + y), final_color);


            FilledRectangle.drawFilledRectangle(batch, new Rectangle(50, 50, 50, 50), new Color (0, 0,0, 0), 0.99f);


        }
    }
}
