using System.Collections.Generic;
using ImGuiNET;

using Num = System.Numerics;


namespace isometricparkfna.UI
{
    public class StyleSet
    {
        // public static IMFont
        public static void pushStyleVarSet(Dictionary<ImGuiStyleVar, float> style_set)
        {
            foreach(var pair in style_set)
            {
                ImGui.PushStyleVar(pair.Key, pair.Value);
            }
        }

        public static void popStyleVarSet(Dictionary<ImGuiStyleVar, float> style_set)
        {
            ImGui.PopStyleVar(style_set.Count);
        }
        public static void pushColorSet(Dictionary<ImGuiCol, Num.Vector4> style_set)
        {
            foreach(var pair in style_set)
            {
                ImGui.PushStyleColor(pair.Key, pair.Value);
            }

        }
        public static void popColorSet(Dictionary<ImGuiCol, Num.Vector4> style_set)
        {
            ImGui.PopStyleColor(style_set.Count);
        }

        public Dictionary<ImGuiStyleVar, float> WindowVars {get;}
        public Dictionary<ImGuiCol, Num.Vector4> WindowColors {get;}

        public StyleSet(Dictionary<ImGuiStyleVar, float> windowVars,
                        Dictionary<ImGuiCol, Num.Vector4> windowColors)
        {

            this.WindowVars = windowVars;
            this.WindowColors = windowColors;
        }

        public void push()
        {
            StyleSet.pushColorSet(this.WindowColors);
            StyleSet.pushStyleVarSet(this.WindowVars);
        }

        public void pop()
        {
            StyleSet.popColorSet(this.WindowColors);
            StyleSet.popStyleVarSet(this.WindowVars);
        }
    }
}
