using System; //FYI, this is where IntPtr comes from

using ImGuiNET;
using Num = System.Numerics;

namespace isometricparkfna.UI
{

    public static class Widgets
    {
//Relatively thin wrappers over common ImGui idioms
        #region basic_elements
        public static void Tooltip(string text, Num.Vector2 relative_position)
        {
            if (ImGui.IsItemHovered())
            {
                var rect = ImGui.GetItemRectMax();
                ImGui.SetNextWindowPos(rect + relative_position );
                ImGui.BeginTooltip();
                ImGui.Text(text);
                ImGui.EndTooltip();
            }
        }

        public static void Tooltip(string text)
        {
            Widgets.Tooltip(text, new Num.Vector2(15, 0));
        }

        public static void LabelTooltip(string label_text, string tooltip_text)
        {
            ImGui.TextDisabled(label_text);
            Widgets.Tooltip(tooltip_text, new Num.Vector2(15, 0));
        }

        public static void Indicator(string text, Num.Vector4 color)
        {
            var dimensions = ImGui.CalcTextSize(text);

            ImGui.PushStyleColor(ImGuiCol.Text, color);
            //Found through trial and error:
            ImGui.SetCursorPosX(ImGui.GetCursorPosX() -(dimensions.X + 10));
            ImGui.Text(text);
            ImGui.PopStyleColor();
        }

        public static void MapImage(ImGuiImageMap map, Num.Vector2 size, int image_index)
        {
            ImGui.Image(map.ImGuiTexture, size, map.GetSourceUVStart(image_index), map.GetSourceUVEnd(image_index), Num.Vector4.One, Num.Vector4.Zero); // Here, the previously loaded texture is used

        }
        #endregion

//Widgets that are made up of the components defined above
        #region composite_widgets

        public static void MapImageTooltip(ImGuiImageMap map, Num.Vector2 size, int image_index, string tooltip)
        {
            ImGui.Image(map.ImGuiTexture, size, map.GetSourceUVStart(image_index), map.GetSourceUVEnd(image_index), Num.Vector4.One, Num.Vector4.Zero); // Here, the previously loaded texture is used

            Widgets.Tooltip(tooltip, Num.Vector2.Zero);

        }

        #endregion
    }
}
