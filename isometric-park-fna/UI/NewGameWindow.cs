
using System;
using System.Text;
using Num = System.Numerics;
using Microsoft.Xna.Framework;

using ImGuiNET;
using TraceryNet;

using isometricparkfna.Engines;
using isometricparkfna.Messages;

namespace isometricparkfna.UI
{

    public static class NewGameWindow
    {

        private static Grammar grammar;

        public static bool had_focus = false;

        public static byte[] parkNameBuffer;
        public static int pos;

        public static byte[] playerNameBuffer;
        public static int playerNamePos;

        public static String selectedTitle;

        public static String playerName2Buffer;
        public static int playerName2Pos;

        public static byte[] customTitleBuffer;
        public static int customTitlePos;

        public static int choice;

        public static bool showModal;

        public const int BUFFER_SIZE = 32;

        public static void Initialize(Grammar grammar)
        {
            NewGameWindow.grammar = grammar;
            showModal = true;

            NewGameWindow.Reset();
        }

        public static void Reset() {
            // parkNameBuffer =  new byte[BUFFER_SIZE];
            parkNameBuffer = Encoding.UTF8.GetBytes(NewGameWindow.grammar.Flatten("#park_name#"));
            pos = 0;

            playerNameBuffer = new byte[BUFFER_SIZE];
            playerNamePos = 0;

            selectedTitle = "";

            playerName2Buffer = "";
            playerName2Pos = 0;

            customTitleBuffer = new byte[BUFFER_SIZE];
            customTitlePos = 0;
        }

        public static void Render(ImFontPtr font, ImFontPtr italicFont, ImGuiWindowBridgeEngine bridgeEngine)
        {
            ImGui.PushFont(font);
            StyleSets.defaultSet.push();

            ImGui.GetStyle().WindowMenuButtonPosition = ImGuiDir.None;
            var newShow = true;

            if (NewGameWindow.had_focus)
            {
                ImGui.PushStyleColor(ImGuiCol.Text, StyleSets.white);
            }
            ImGui.Begin("New Game", ref newShow,
                        ImGuiWindowFlags.AlwaysAutoResize |
                        ImGuiWindowFlags.NoResize |
                        ImGuiWindowFlags.NoCollapse |
                        ImGuiWindowFlags.NoSavedSettings );
            if (NewGameWindow.had_focus)
            {
                ImGui.PopStyleColor();
            }
            NewGameWindow.had_focus = ImGui.IsWindowFocused();

            ImGui.Text("Park Name: ");
            ImGui.SameLine();

            int newPos = NewGameWindow.pos;

            //God this sucks:
            unsafe {
                ImGui.InputText("##name", parkNameBuffer, (uint)parkNameBuffer.Length, ImGuiInputTextFlags.AutoSelectAll, null, (IntPtr)(&newPos));
            }
            NewGameWindow.pos = newPos;

            ImGui.SameLine();
            if (ImGui.Button("Random"))
            {
                parkNameBuffer = Encoding.UTF8.GetBytes(NewGameWindow.grammar.Flatten("#park_name#"));
            }

            ImGui.Text("Formal Name: ");
            ImGui.SameLine();




            ImGui.SetNextItemWidth(ImGui.CalcTextSize("<No Title>").X
                                   + ImGui.CalcTextSize("XXXX").X);
            if (ImGui.BeginCombo("##title", selectedTitle))
            {

                ImGui.PushFont(italicFont);
                if (ImGui.Selectable("<No Title>"))
                {
                    selectedTitle = "<No Title>";
                }
                ImGui.PopFont();

                foreach (var title in new String[] {"Mr.", "Mrs.", "Ms.","Miss", "Mx.", "M", "Dr.", "Rev.", "Sir", "Madam", "Hon.", "Dishon.", "Director", "Comrade", "Brother", "Sister", "Friend"})
                {
                    if (ImGui.Selectable(title))
                    {
                        selectedTitle = title;
                    }
                }

                ImGui.PushFont(italicFont);
                ImGui.PopFont();

                ImGui.EndCombo();
            }

            ImGui.SameLine();
            int newPlayerNamePos = NewGameWindow.playerNamePos;

            //God this sucks:
            unsafe {
                ImGui.InputText("##playerNameBuffer", playerNameBuffer, (uint)playerNameBuffer.Length, ImGuiInputTextFlags.AutoSelectAll, null, (IntPtr)(&newPlayerNamePos));
            }

            NewGameWindow.playerNamePos = newPlayerNamePos;

            ImGui.SameLine();

            if (ImGui.Button("Custom Title..."))
            {
                NewGameWindow.showModal = true;
                ImGui.OpenPopup("Custom");

            }
            if (ImGui.BeginPopup("Custom" /*, ref showModal*/))
            {
                int newCustomTitlePos = NewGameWindow.customTitlePos;
                unsafe {
                    ImGui.InputText("##customTitleBuffer", customTitleBuffer, (uint)customTitleBuffer.Length, ImGuiInputTextFlags.AutoSelectAll, null, (IntPtr)(&newCustomTitlePos));
                }

                NewGameWindow.customTitlePos = newCustomTitlePos;


                if (ImGui.Button("Okay"))
                {
                    selectedTitle = System.Text.Encoding.UTF8.GetString(customTitleBuffer);
                    ImGui.CloseCurrentPopup();
                }
                ImGui.SameLine();
                if (ImGui.Button("Cancel"))
                {
                    ImGui.CloseCurrentPopup();
                }
                ImGui.EndPopup();

            }

            ImGui.Text("Casual Name: ");
            ImGui.SameLine();

            int newPlayerName2Pos = NewGameWindow.playerName2Pos;

            //God this sucks:
            unsafe {
                ImGui.InputTextWithHint("##playerName2", "Leave blank to use full name", ref playerName2Buffer, 32, ImGuiInputTextFlags.AutoSelectAll, null, (IntPtr)(&newPlayerName2Pos));
            }

            NewGameWindow.playerName2Pos = newPlayerName2Pos;

            ImGui.RadioButton("Easy: Social Democracy", ref choice, ((int)DifficultyLevel.Easy));
            ImGui.RadioButton("Medium: Austerity", ref choice, ((int)DifficultyLevel.Medium));
            ImGui.RadioButton("Hard: Libertarianism", ref choice, ((int)DifficultyLevel.Hard));

            if (ImGui.Button("Okay"))
            {

                bridgeEngine.setTextVariableMessages.Add(new SetTextVariableMessage { variable = "playerFormal", value = System.Text.Encoding.UTF8.GetString(playerNameBuffer) });
                bridgeEngine.setTextVariableMessages.Add(new SetTextVariableMessage { variable = "playerCasual", value = playerName2Buffer });
                bridgeEngine.setTextVariableMessages.Add(new SetTextVariableMessage { variable = "playerTitle", value = selectedTitle});

                bridgeEngine.spawnGameMessages.Add(new SpawnGameMessage { Difficulty = (DifficultyLevel)choice });
                bridgeEngine.typeMessages.Add(new ToggleWindowTypeMessage {Window = Window.NewGame});
                bridgeEngine.gameStateMessages.Add(new GameStateMessage { isPlaying = true});
                NewGameWindow.Reset();
            }

            ImGui.SameLine();
            if ( ImGui.Button("Cancel"))
            {
                bridgeEngine.typeMessages.Add(new ToggleWindowTypeMessage {Window = Window.NewGame});

                bridgeEngine.typeMessages.Add(new ToggleWindowTypeMessage { Window = Window.MainMenu });
                NewGameWindow.Reset();
            }

            ImGui.End();

            StyleSets.defaultSet.pop();
            ImGui.PopFont();
        }
    }
}
