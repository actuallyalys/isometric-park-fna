using System;
using System.Collections.Generic;
using System.Linq;

using ImGuiNET;

using isometricparkfna.Components;
using isometricparkfna.Engines;
using isometricparkfna.Messages;

using Microsoft.Xna.Framework;
using Num = System.Numerics;

using Encompass;

namespace isometricparkfna.UI
{
    public static class ContractsWindow
    {
        public static bool show_all;
        public static bool had_focus = false;

        // private static (Entity entity, string name, string description, ContractStatus status, decimal amount, string delta_trees, int age, int image_index, Vector2 square) selected;
        private static ContractDetails selected;


        public static void Render(ImFontPtr font, ImGuiWindowBridgeEngine engine,
                                  List<ContractDetails> contracts)
        {
            bool newShow = true;


            if (newShow)
            {
                ImGui.PushFont(font);

                ImGui.GetStyle().WindowMenuButtonPosition = ImGuiDir.None;
                StyleSets.defaultSet.push();

                ImGui.SetNextWindowSize(new Num.Vector2(320, 340));
                if (ContractsWindow.had_focus)
                {
                    ImGui.PushStyleColor(ImGuiCol.Text, StyleSets.white);
                }
                ImGui.Begin("Contracts", ref newShow, ImGuiWindowFlags.NoResize | ImGuiWindowFlags.NoCollapse | ImGuiWindowFlags.NoSavedSettings);
                if (ContractsWindow.had_focus)
                {
                    ImGui.PopStyleColor();
                }
                ContractsWindow.had_focus = ImGui.IsWindowFocused();

                ImGui.BeginListBox("##Contracts:", new Num.Vector2(320, 150));

                var filter_statuses = new[] { ContractStatus.Expired, ContractStatus.Broken, ContractStatus.Rejected };

                foreach (var contract in contracts.Where((contract) =>
                         ((!(filter_statuses.Contains(contract.status))
                           || ContractsWindow.show_all))))
                {
                    if (ImGui.Selectable(String.Format("{0}##{1}", contract.name, contract.entity.ID),
                                         ContractsWindow.selected.entity == contract.entity))
                    {
                        ContractsWindow.selected.entity = contract.entity;
                        // newSelected = contract.entity;

                    }
                    if (ContractsWindow.selected.entity == contract.entity)
                    {
                        ContractsWindow.selected = contract;
                    }

                    if (ImGui.IsItemHovered() && ImGui.IsMouseDoubleClicked(0))
                    {
                        engine.messages.Add(new ToggleWindowMessage { Window = Window.Contract, Entity = ContractsWindow.selected.entity });
                    }

                    ImGui.SameLine();
                    switch (contract.status)
                    {
                        case ContractStatus.Proposed:
                            ImGui.TextColored(new Num.Vector4(0.25f, 0.25f, 0.95f, 1f), contract.status.ToString());
                            break;
                        case ContractStatus.Active:
                        case ContractStatus.Accepted:
                            ImGui.TextColored(new Num.Vector4(0.25f, 0.95f, 0.25f, 1f), contract.status.ToString());
                            break;
                        case ContractStatus.Rejected:
                        case ContractStatus.Broken:
                        case ContractStatus.Expired:
                            // if (ContractsWindow.show_all)
                            // {

                            // ImGui.TextColored(new Num.Vector4(0.95f, 0.25f, 0.25f, 1f), contract.status.ToString());
                            // }
                            ImGui.TextColored(new Num.Vector4(0.95f, 0.25f, 0.25f, 1f), contract.status.ToString());
                            break;
                        case ContractStatus.Completed:
                            ImGui.TextColored(new Num.Vector4(0.25f, 0.25f, 0.25f, 1f), contract.status.ToString());
                            break;
                        default:
                            break;
                    }

                    if ((contract.status == ContractStatus.Proposed)
                            &&  (contract.age > (30 * 5)))
                    {
                        ImGui.SameLine();
                        ImGui.TextColored(StyleSets.red, "(!)");

                        if (ImGui.IsItemHovered())
                        {
                            ImGui.BeginTooltip();
                            ImGui.Text("Offer is about to expire.");
                            ImGui.EndTooltip();
                        }
                    }

                }
                ImGui.EndListBox();

                ImGui.Checkbox("Show All", ref show_all);

                ImGui.Separator();
                ImGui.Text(string.Format("Amount: ${0}/mo.", ContractsWindow.selected.amount));

                if (ContractsWindow.selected.delta_trees != null)
                {
                    ImGui.Text("Sustainability: ");


                    ImGui.SameLine();
                    var color = new Num.Vector4(1f, 1f, 1f, 1f);
                    switch (ContractsWindow.selected.delta_trees)
                    {
                        case "Unsustainable":
                            color = new Num.Vector4(0.95f, 0.25f, 0.25f, 1f);
                            break;
                        case "Restoration":
                            color = new Num.Vector4(0.25f, 0.95f, 0.25f, 1f);
                            break;
                        case "Moderately unsustainable":
                        case "Somewhat unsustainable":
                            color = new Num.Vector4(0.95f, 0.65f, 0.25f, 1f);
                            break;
                    }
                    ImGui.TextColored(color, ContractsWindow.selected.delta_trees);

                }
                if (ImGui.Button("Open"))
                {
                    Logging.Trace(string.Format("{0} opened", ContractsWindow.selected.entity));

                    engine.messages.Add(new ToggleWindowMessage { Window = Window.Contract, Entity = ContractsWindow.selected.entity });

                }

                ContractWindow.ContractStatusButtons(engine, selected.entity, selected.status);

                ImGui.Separator();
                if (ImGui.Button("Okay"))
                {
                    newShow = false;
                }

                ImGui.End();
                ImGui.GetStyle().WindowMenuButtonPosition = ImGuiDir.Left;
                StyleSets.defaultSet.pop();
                ImGui.PopFont();
            }

            if (!newShow)
            {
                Logging.Spy(newShow, "newShow");
                Logging.Trace("Contracts toggled.");
                engine.typeMessages.Add(new ToggleWindowTypeMessage { Window = Window.Contracts });
            }
            engine.selectedMessages.Add(new SelectMessage { Entity = selected.entity });
        }
    }
}
