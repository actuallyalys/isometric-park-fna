
namespace isometricparkfna.UI {

    public enum ImGuiColor : uint {
        BLACK = 0xFF000000,
        RED = 0xFFFF0000,
        GREEN = 0xFF00FF00, 
        BLUE = 0xFF0000FF,
        DARKGREY = 0xFF999999,
        LIGHTRED = 0xFFAA0000,
        LIGHTGREEN = 0xFF00AA00, 
        LIGHTBLUE = 0xFF0000AA,
        LIGHTGREY = 0xFFAAAAAA,
        WHITE = 0xFF000000,

    }

}