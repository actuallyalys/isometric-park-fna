
using ImGuiNET;

using Num = System.Numerics;
using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using YamlDotNet.Serialization;
using Encompass;

using isometricparkfna.Messages;
using isometricparkfna.Engines;

namespace isometricparkfna.UI
{

    public struct NewsItem
    {
        public string hed;
        public string contents;
        public string source;
        public Dictionary<string, string> variables;

        public NewsItem Flatten(TraceryNet.Grammar grammar)
        {

            var variableString = "#";

            if (this.variables != null)
            {
                foreach (var variable in this.variables)
                {
                    variableString += String.Format("[{0}:{1}]", variable.Key, variable.Value);
                }
            }

            variableString += "vars# ";
            var result = grammar.Flatten(variableString);
            // Logging.Trace(String.Format("{0}: {1}", variableString, result));

            return new NewsItem
            {
                hed = grammar.Flatten(this.hed),
                contents = grammar.Flatten(contents),
                source = grammar.Flatten(this.source)
            };
        }



        public static List<NewsItem> FromYaml(string yamlString)
        {
            var input = new StringReader(yamlString);

            var deserializer = new DeserializerBuilder()
            .Build();

            //Dictionary<string, string>
            var items = deserializer.Deserialize<List<NewsItem>>(input);

            return items;
        }
    }

    public static class NewsWindow
    {
        public static bool wire_open = true;
        public static bool true_open = false;
        public static bool naturalist_open = false;
        public static bool had_focus = false;

        private static bool MenuItem(string label, bool active) {
            if (active)
            {
                ImGui.PushStyleColor(ImGuiCol.Text, StyleSets.white);
            }
            
            var result = ImGui.BeginTabItem(label);

            if (active)
            {
                ImGui.PopStyleColor(1);
            }

            return result;
        }

        public static void Render(ImFontPtr font, Simulation sim, ImGuiWindowBridgeEngine engine)
        {
            bool newShow = true;

            if (newShow)
            {
                ImGui.PushFont(font);

                ImGui.GetStyle().WindowMenuButtonPosition = ImGuiDir.None;

                StyleSets.defaultSet.push();

                ImGui.SetNextWindowSize(new Num.Vector2(400, 400));

                if (NewsWindow.had_focus)
                {
                    ImGui.PushStyleColor(ImGuiCol.Text, StyleSets.white);
                }
                ImGui.Begin("NEWS", ref newShow, ImGuiWindowFlags.NoResize | ImGuiWindowFlags.NoCollapse | ImGuiWindowFlags.NoSavedSettings);

                if (NewsWindow.had_focus)
                {
                    ImGui.PopStyleColor();
                }
                NewsWindow.had_focus = ImGui.IsWindowFocused();

                var content = sim.latestNewsItems;


                if (ImGui.BeginTabBar("Sources", 0))
                {
                    if (MenuItem("Wire", wire_open))
                    {
                        wire_open = true;
                        naturalist_open = false;
                        true_open = false;

                        foreach (NewsItem story in content.Where(s => (s.source == "Wire")).Take(3))
                        {
                            if (ImGui.TreeNode(story.hed))
                            {
                                ImGui.TextWrapped(story.contents);
                                ImGui.TreePop();
                            }
                        }
                        ImGui.EndTabItem();
                    }
                    if (MenuItem("The Naturalist", naturalist_open))
                    {
                        wire_open = false;
                        naturalist_open = true;
                        true_open = false;

                        foreach (NewsItem story in content.Where(s => (s.source == "Arborist")).Take(3))
                        {
                            if (ImGui.TreeNode(story.hed))
                            {
                                ImGui.TextWrapped(story.contents);
                                ImGui.TreePop();
                            }
                        }
                        ImGui.EndTabItem();
                    }
                    if (MenuItem("All True News", true_open))
                    {
                        wire_open = false;
                        naturalist_open = false;
                        true_open = true;

                        foreach (NewsItem story in content.Where(s => (s.source == "True")).Take(3))
                        {
                            if (ImGui.TreeNode(story.hed))
                            {
                                ImGui.TextWrapped(story.contents);
                                ImGui.TreePop();
                            }
                        }
                        ImGui.EndTabItem();
                    }
		    ImGui.EndTabBar();
                }


                if (ImGui.Button("Okay"))
                {
                    newShow = false;
                }

                ImGui.End();

                ImGui.GetStyle().WindowMenuButtonPosition = ImGuiDir.Left;
                StyleSets.defaultSet.pop();

                ImGui.PopFont();
            }

            if (!newShow)
            {
                engine.typeMessages.Add(new ToggleWindowTypeMessage { Window = Window.News });
            }
        }
    }
}
