
using System.Collections.Generic;
using ImGuiNET;

using Num = System.Numerics;


namespace isometricparkfna.UI
{
    public static class StyleSets
    {
        public static Num.Vector4 lightgrey = new Num.Vector4(0.9f, 0.9f, 0.9f, 1f);
        public static Num.Vector4 grey = new Num.Vector4(0.75f, 0.75f, 0.75f, 1f);
        public static Num.Vector4 grey_transparent = new Num.Vector4(0.75f, 0.75f, 0.75f, 0.25f);
        public static Num.Vector4 darkgrey = new Num.Vector4(0.45f, 0.45f, 0.45f, 1f);
        public static Num.Vector4 black = new Num.Vector4(0f, 0f, 0f, 1f);
        public static Num.Vector4 white = new Num.Vector4(1f, 1f, 1f, 1f);
        public static Num.Vector4 title_bar = new Num.Vector4(0.65f, 0.65f, 0.65f, 1f);

        public static Num.Vector4 red = new Num.Vector4(1f, 0.0f, 0.20f, 1f);

        // public static Num.Vector4 selected = new Num.Vector4(0.0f, 0.0f, 0.75f, 1f);
        public static Num.Vector4 selected = new Num.Vector4(0.0f, 0.0f, 0.55f, 1f);
        public static Dictionary<ImGuiStyleVar, float> defaultWindowVars = new Dictionary<ImGuiStyleVar, float> {
            { ImGuiStyleVar.FrameRounding, 0.0f },
            {ImGuiStyleVar.WindowRounding, 0.0f},
            {ImGuiStyleVar.FrameBorderSize, 1.0f},
            {ImGuiStyleVar.TabRounding, 0.0f},
            {ImGuiStyleVar.GrabRounding, 0.0f},
        };
        public static Dictionary<ImGuiCol, Num.Vector4> defaultWindowColors = new Dictionary<ImGuiCol, Num.Vector4> {

            {ImGuiCol.WindowBg, grey},
            {ImGuiCol.FrameBg, grey},
            {ImGuiCol.FrameBgHovered, grey},
            {ImGuiCol.Header, darkgrey},
            {ImGuiCol.HeaderHovered, darkgrey},
            {ImGuiCol.HeaderActive, darkgrey},
            {ImGuiCol.ButtonHovered, grey},
            {ImGuiCol.ButtonActive, darkgrey},
            {ImGuiCol.SliderGrab, darkgrey},
            {ImGuiCol.SliderGrabActive, darkgrey},


            {ImGuiCol.Tab, darkgrey},
            {ImGuiCol.TabHovered, darkgrey},
            {ImGuiCol.TabActive, selected},

            {ImGuiCol.CheckMark, black},

            {ImGuiCol.TitleBg, title_bar},
            {ImGuiCol.TitleBgActive, selected},
            {ImGuiCol.TitleBgCollapsed, title_bar},

            {ImGuiCol.Border, black},
            {ImGuiCol.BorderShadow, black},

            {ImGuiCol.PopupBg, grey},

            {ImGuiCol.Button, grey_transparent},
            {ImGuiCol.Text, black}
        };

        public static Dictionary<ImGuiCol, Num.Vector4> defaultWindowColorsLight = new Dictionary<ImGuiCol, Num.Vector4> {
            {ImGuiCol.WindowBg, lightgrey},
            {ImGuiCol.FrameBg, lightgrey},
            {ImGuiCol.FrameBgHovered, lightgrey},
            {ImGuiCol.Header, darkgrey},
            {ImGuiCol.HeaderHovered, darkgrey},
            {ImGuiCol.HeaderActive, darkgrey},
            {ImGuiCol.ButtonHovered, lightgrey},
            {ImGuiCol.ButtonActive, darkgrey},
            {ImGuiCol.SliderGrab, darkgrey},
            {ImGuiCol.SliderGrabActive, darkgrey},


            {ImGuiCol.Tab, darkgrey},
            {ImGuiCol.TabHovered, darkgrey},
            {ImGuiCol.TabActive, selected},

            {ImGuiCol.CheckMark, black},

            {ImGuiCol.TitleBg, title_bar},
            {ImGuiCol.TitleBgActive, selected},
            {ImGuiCol.TitleBgCollapsed, title_bar},

            {ImGuiCol.Border, black},
            {ImGuiCol.BorderShadow, black},

            {ImGuiCol.PopupBg, white},

            {ImGuiCol.Button, grey_transparent},
            {ImGuiCol.Text, black}
        };
        public static StyleSet defaultSet = new StyleSet(defaultWindowVars, defaultWindowColors);
    }

}
