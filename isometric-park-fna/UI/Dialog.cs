﻿using System;
using System.Linq;
using System.Collections.Generic;
using ImGuiNET;
using ImGuiNET.SampleProgram.XNA;
using isometricparkfna.Utils;
using TraceryNet;
using Num = System.Numerics;
using Encompass;

using isometricparkfna.Engines;
using isometricparkfna.Components;
using isometricparkfna.Messages;

#nullable enable

namespace isometricparkfna.UI
{


//#nullable enable
    public struct DialogOption
    {
        public String? choice;
        public String response;
        public String speaker;
    }


    public static class DialogTrees
    {

        public static Node<DialogOption> introTree = new Node<DialogOption>(
            new DialogOption
        {
            response = "Welcome to your new park, director! You can use the mouse or arrow keys to move around, and the plus and minus keys to zoom in and out. B opens the budget and F lets you adjust Forest Policy.",
            speaker = "The Governor"
        },
        new Node<DialogOption>(new DialogOption
        {
            choice = "Okay",
            response = "Make sure that you keep visitors happy and the budget in the black! You're currently getting an annual grant out of my budget—it'd sure be nice if you park were self-sufficient so we could drop that expense!",
            speaker = "The Governor"
        },
        new Node<DialogOption>[] {
            new Node<DialogOption>(new DialogOption {choice="And I need to keep the forest healthy, too, right?", response="Uhh yeah.", speaker = "The Governor" },
            new Node<DialogOption>(new DialogOption {choice="...", speaker = "The Governor"})),
            new Node<DialogOption>(new DialogOption {choice="Sounds good!", response="I'll check in soon.", speaker = "The Governor" })

        })
        );

        public static Node<DialogOption> testTree = new Node<DialogOption>(
            new DialogOption
        {
            response = "#addressGreetingFormal#",
            speaker = "#assistantName#"
        },
        new Node<DialogOption>[] {
            new Node<DialogOption>(new DialogOption {
                choice="Call me #playerCasual#", response="Sure",
                speaker = "#assistantName#"
            }),
            new Node<DialogOption>(new DialogOption {
                choice="Hi.", response="Bye!",
                speaker = "#assistantName#"
            }),
            new Node<DialogOption>(new DialogOption {
                choice="How are you?", response="#howdoing#",
                speaker = "#assistantName#"
            })

        }
        );

        public static Node<DialogOption> testTree2 = new Node<DialogOption>(
            new DialogOption
        {
            response = "#whatever#",
            speaker = "#assistantName#"
        },

        new Node<DialogOption>[] {
            new Node<DialogOption>(new DialogOption {
                choice="Hi.", response="Bye!",
                speaker = "#assistantName#"
            }),
            new Node<DialogOption>(new DialogOption {
                choice="How are you?", response="#howdoing#",
                speaker = "#assistantName#"
            })

        }
        );


        public static Node<DialogOption> flatten(Node<DialogOption> node, Grammar grammar)
        {

            DialogOption new_data = new DialogOption
            {
                choice = node.data.choice != null ? grammar.Flatten(node.data.choice) : node.data.choice,
                response = grammar.Flatten(node.data.response),
                speaker = grammar.Flatten(node.data.speaker)
            };


            if (node.children != null)
            {
                List<Node<DialogOption>> new_children = new List<Node<DialogOption>>();
                foreach (Node<DialogOption> child in node.children)
                {
                    new_children.Add(flatten(child, grammar));
                }

                return new Node<DialogOption>(new_data, new_children.ToArray());
            }
            else
            {
                return new Node<DialogOption>(new_data);

            }

        }

    }

    public static class DialogInterface
    {

        public static bool hadFocus = false;

        public static ImGuiImageMap? map;
        private static IntPtr _imGuiTexture;

        public static void LoadContent(ImGuiRenderer _imGuiRenderer,
                                       ImGuiImageMap map)
        {
            DialogInterface.map = map;
            var _xnaTexture = map.ImageMapTexture;
            DialogInterface._imGuiTexture = _imGuiRenderer.BindTexture(_xnaTexture);
        }

        private static bool ContentRemaining(DialogComponent dialogComponent)
        {
            return !String.IsNullOrWhiteSpace(dialogComponent.CurrentDialog)
                   || ((dialogComponent.Options != null) && dialogComponent.Options.Count > 0);
        }

        public static void RenderDialog(Entity entity,
                                        ImGuiWindowBridgeEngine bridgeEngine, ref bool show, ImFontPtr font, DialogComponent dialogComponent, int imageIndex)
        {
            var paused = true;

            //If we're supposed to show the dialog and there's something to
            //show.
            //I don't know if it's the best design, but it's possible to
            //get into a state where we're effectively done.but there's still an entity
            if (show && ContentRemaining(dialogComponent)) {
                ImGui.PushFont(font);
                ImGui.GetStyle().WindowMenuButtonPosition = ImGuiDir.None;

                StyleSets.defaultSet.push();

                ImGui.SetNextWindowSize(new Num.Vector2(400, 250));
                if (DialogInterface.hadFocus)
                {
                    ImGui.PushStyleColor(ImGuiCol.Text, StyleSets.white);
                }

                ImGui.Begin(dialogComponent.CurrentSpeaker, ref show, ImGuiWindowFlags.NoResize | ImGuiWindowFlags.NoCollapse | ImGuiWindowFlags.AlwaysAutoResize | ImGuiWindowFlags.NoSavedSettings);
                if (DialogInterface.hadFocus)
                {
                    ImGui.PopStyleColor();
                }
                DialogInterface.hadFocus = ImGui.IsWindowFocused();

                ImGui.Columns(2);
                ImGui.SetColumnWidth(0, 120);


                if (DialogInterface.map != null)
                {
                    var metadata = DialogInterface.map.Metadata[imageIndex];
                    String Tooltip = String.Format("{0}\n\nSource: {1}", metadata.Description, metadata.Source);
                    Widgets.MapImageTooltip(DialogInterface.map, new Num.Vector2(111, 148), imageIndex, DialogInterface.map.Metadata[imageIndex].Description);

                }
                else
                {
                    Logging.Error("Portrait ImageMap not initialized.");
                }

                ImGui.NextColumn();

                if (!String.IsNullOrWhiteSpace(dialogComponent.CurrentDialog))
                {
                    string messageText = dialogComponent.CurrentDialog;
                    ImGui.TextWrapped(messageText);
                }

                ImGui.Columns(1);

                if ((dialogComponent.Options != null) && dialogComponent.Options.Count > 0)
                {
                    for (int i = 0; i < dialogComponent.Options.Count; i++)
                    {
                        string buttonText = dialogComponent.Options[i];
                        if (ImGui.Button(buttonText))
                        {
                            bridgeEngine.dialogChoiceMessages.Add(new DialogChoiceMessage
                            {
                                Entity = entity,
                                Choice = i
                            });
                        }
                    }
                }
                else
                {
                    if (ImGui.Button("Okay"))
                    {
                        show = false;
                        paused = false;
                        bridgeEngine.dialogChoiceMessages.Add(new DialogChoiceMessage
                        {

                            Entity = entity,
                            Choice = -1
                        });
                    }
                }

                ImGui.End();
                ImGui.GetStyle().WindowMenuButtonPosition = ImGuiDir.Left;
                StyleSets.defaultSet.pop();
                ImGui.PopFont();
            }
            //If we're at the end, advance it and hide.
            if (!ContentRemaining(dialogComponent))
            {
                show = false;
                paused = false;
                bridgeEngine.dialogChoiceMessages.Add(new DialogChoiceMessage
                {
                    Entity = entity,
                    Choice = -1
                });
            }
            if (!paused)
            {
                bridgeEngine.gameRateMessages.Add(new GameRateMessage {
                    paused = paused
                });
            }
        }
    }
}
