using System;
using ImGuiNET;

using Num = System.Numerics;

using isometricparkfna.Engines;
using isometricparkfna.Messages;
using isometricparkfna.UI;

namespace isometricparkfna.UI
{

    public static class MainMenu
    {

        public static void Render(ImFontPtr font, ImGuiWindowBridgeEngine bridgeEngine, int width)
        {
            //Has to go first so the measurement is correct:
            ImGui.PushFont(font);

            ImGui.GetStyle().WindowMenuButtonPosition = ImGuiDir.None;
            bool newShow = true;

            Num.Vector2 text_size = ImGui.CalcTextSize("Quit to Main Menu");
            Num.Vector2 button_size = new Num.Vector2((int)text_size.X*1.1f,
                    (int)text_size.Y*1.25f+5);


            StyleSets.defaultSet.push();

            ImGui.SetNextWindowPos(new Num.Vector2(((width/2) - (button_size.X/2)), 200));

            ImGui.Begin("##Toolbar", ref newShow, ImGuiWindowFlags.AlwaysAutoResize | ImGuiWindowFlags.NoResize | ImGuiWindowFlags.NoCollapse | ImGuiWindowFlags.NoSavedSettings | ImGuiWindowFlags.NoTitleBar);

            if (ImGui.Button("New Game", button_size))
            {
                bridgeEngine.typeMessages.Add(new ToggleWindowTypeMessage { Window = Window.NewGame });
                bridgeEngine.typeMessages.Add(new ToggleWindowTypeMessage { Window = Window.MainMenu });
            }

            if (ImGui.Button("Quit", button_size))
            {
                System.Console.WriteLine("Quitting");
                bridgeEngine.quitGameMessages.Add(new QuitGameMessage {});
            }

            ImGui.End();

            ImGui.GetStyle().WindowMenuButtonPosition = ImGuiDir.Left;
            StyleSets.defaultSet.pop();
            ImGui.PopFont();
        }
    }
}
