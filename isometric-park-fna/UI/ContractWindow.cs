using System;
using System.Collections.Generic;
using System.Linq;

using ImGuiNET;
using ImGuiNET.SampleProgram.XNA;

using isometricparkfna.Components;
using isometricparkfna.Engines;
using isometricparkfna.Messages;
using Microsoft.Xna.Framework;

using Num = System.Numerics;

using Encompass;

namespace isometricparkfna.UI
{
    public struct ContractDetails {
        public Entity entity;
        public string name;
        public string description;
        public ContractStatus status;
        public decimal amount;
        public string delta_trees;
        public double age; //in days
        public int image_index;
        public int area_size;
        public Vector2 square;
    }
    public static class ContractWindow
    {

        private static ImGuiImageMap map;
        private static IntPtr _imGuiTexture;

        private static Dictionary<string, bool> hadFocus = new Dictionary<string, bool>();

        public static void LoadContent(ImGuiRenderer _imGuiRenderer, ImGuiImageMap map)
        {
            ContractWindow.map = map;

            var _xnaTexture = map.ImageMapTexture;
            ContractWindow._imGuiTexture = _imGuiRenderer.BindTexture(_xnaTexture);
        }

        public static void Render(ImFontPtr font, ImFontPtr italicFont, ImGuiWindowBridgeEngine engine, ContractDetails details /*Entity entity, string name, string description, ContractStatus status, decimal amount, string delta_trees, int area_size, int imageIndex, Vector2 square*/)

        {
            bool newShow = true;

            if (newShow)
            {
                ImGui.PushFont(font);

                ImGui.GetStyle().WindowMenuButtonPosition = ImGuiDir.None;
                StyleSets.defaultSet.push();
                ImGui.SetNextWindowSize(new Num.Vector2(320, 420));

                var title = string.Format("Contract {0} ## {1}", details.name, details.entity.ID);

                if (ContractWindow.hadFocus.ContainsKey(title) &&
                        ContractWindow.hadFocus[title])
                {
                    ImGui.PushStyleColor(ImGuiCol.Text, StyleSets.white);
                }

                ImGui.Begin(details.name, ref newShow,
                            ImGuiWindowFlags.NoResize | ImGuiWindowFlags.NoCollapse | ImGuiWindowFlags.NoSavedSettings);

                if (ContractWindow.hadFocus.ContainsKey(title) &&
                        ContractWindow.hadFocus[title])
                {
                    ImGui.PopStyleColor();
                }
                if(ContractWindow.hadFocus.ContainsKey(title))
                {
                    ContractWindow.hadFocus[title] = ImGui.IsWindowFocused();
                }
                else
                {
                    ContractWindow.hadFocus.Add(title, ImGui.IsWindowFocused());
                }

                Widgets.MapImage(map, new Num.Vector2(250, 200), details.image_index);  // Here, the previously loaded texture is used

                ImGui.PushFont(italicFont);
                ImGui.TextWrapped(details.description);
                ImGui.PopFont();

                ImGui.Separator();
                switch (details.status)
                {
                    case ContractStatus.Proposed:
                        ImGui.TextColored(new Num.Vector4(0.25f, 0.25f, 0.95f, 1f), details.status.ToString());
                        break;
                    case ContractStatus.Active:
                    case ContractStatus.Accepted:
                        ImGui.TextColored(new Num.Vector4(0.25f, 0.95f, 0.25f, 1f), details.status.ToString());
                        break;
                    case ContractStatus.Rejected:
                    case ContractStatus.Broken:
                    case ContractStatus.Expired:
                        // if (ContractsWindow.show_all)
                        // {

                        // ImGui.TextColored(new Num.Vector4(0.95f, 0.25f, 0.25f, 1f), contract.status.ToString());
                        // }
                        ImGui.TextColored(new Num.Vector4(0.95f, 0.25f, 0.25f, 1f), details.status.ToString());
                        break;
                    case ContractStatus.Completed:
                        ImGui.TextColored(new Num.Vector4(0.25f, 0.25f, 0.25f, 1f), details.status.ToString());
                        break;
                    default:
                        break;
                }
                ImGui.Text(string.Format("Amount: ${0}/mo.", details.amount));
                ImGui.Text(string.Format("Size: {0} acres", details.area_size));

                if (details.delta_trees != null)
                {
                    ImGui.Text("Sustainability: ");
                    ImGui.SameLine();
                    ImGui.TextDisabled("(?)");
                    if (ImGui.IsItemHovered())
                    {
                        var rect = ImGui.GetItemRectMax();
                        ImGui.SetNextWindowPos(rect + new Num.Vector2(15, 0));
                        ImGui.BeginTooltip();
                        ImGui.Text("Are logged trees replaced?");
                        ImGui.EndTooltip();
                    }


                    ImGui.SameLine();
                    var color = new Num.Vector4(1f, 1f, 1f, 1f);
                    switch (details.delta_trees)
                    {
                        case "Unsustainable":
                            color = new Num.Vector4(0.95f, 0.25f, 0.25f, 1f);
                            break;
                        case "Restoration":
                            color = new Num.Vector4(0.25f, 0.95f, 0.25f, 1f);
                            break;
                        case "Moderately unsustainable":
                        case "Somewhat unsustainable":
                            color = new Num.Vector4(0.95f, 0.65f, 0.25f, 1f);
                            break;
                    }
                    ImGui.TextColored(color, details.delta_trees);
                }

                ContractStatusButtons(engine, details.entity, details.status);

                ImGui.Separator();

                if (ImGui.Button("Okay"))
                {
                    newShow = false;
                }
                ImGui.SameLine();
                if (ImGui.Button("Jump To"))
                {
                    int adjustedx = (int)details.square.X;
                    int adjustedy = (int)details.square.Y;

                    int screenx = (adjustedx - adjustedy) * Tile.TileSpriteWidth / 2;
                    int screeny = (adjustedx + adjustedy) * Tile.TileSpriteHeight / 2;

                    engine.jumpCameraMessages.Add(new JumpCameraMessage {Movement = new Vector2(screenx, screeny)});
                }
                // Logging.Trace("Almost done.");

                ImGui.End();
                ImGui.GetStyle().WindowMenuButtonPosition = ImGuiDir.Left;
                StyleSets.defaultSet.pop();
                ImGui.PopFont();

                // Logging.Trace("Finished.");
            }

            if (!newShow)
            {
                engine.messages.Add(new ToggleWindowMessage {Window = Window.Contract, Entity = details.entity  });
            }

            engine.selectedMessages.Add(new SelectMessage { Entity = details.entity });

        }

        public static void ContractStatusButtons(ImGuiWindowBridgeEngine engine, Entity entity, ContractStatus status)
        {
            if (status == ContractStatus.Proposed)
            {
                if (ImGui.Button("Accept"))
                {
                    Logging.Trace(string.Format("{0} selected", entity));

                    engine.contractStatusMessages.Add(new ChangeContractStatusMessage { Entity = entity, newStatus = ContractStatus.Accepted });

                }
                ImGui.SameLine();
                if (ImGui.Button("Reject"))
                {
                    Logging.Trace(string.Format("{0} rejected", entity));

                    engine.contractStatusMessages.Add(new ChangeContractStatusMessage { Entity = entity, newStatus = ContractStatus.Rejected });

                }

            }
            else if (status == ContractStatus.Accepted)
            {
                if (ImGui.Button("Cancel"))
                {
                    Logging.Trace(string.Format("{0} canceled", entity));

                    engine.contractStatusMessages.Add(new ChangeContractStatusMessage { Entity = entity, newStatus = ContractStatus.Broken });
                }
            }
        }
    }
}
