﻿using System;

using NUnit.Framework;
using FluentAssertions;
using Encompass;
using System.Collections.Generic;

namespace Tests
{
    public class OrderedRendererTest
    {
        struct AComponent : IComponent { }
        struct BComponent : IComponent { }
        struct CComponent : IComponent { }

        struct TestDrawComponent : IComponent, IDrawableComponent
        {
            public int Layer { get; set; }
        }

        class TestRenderer : OrderedRenderer<TestDrawComponent>
        {
            public override void Render(Entity entity, in TestDrawComponent testDrawComponent) { }
        }

        static bool called = false;
        class DeactivatedRenderer : TestRenderer
        {
            public override void Render(Entity entity, in TestDrawComponent testDrawComponent)
            {
                called = true;
            }
        }

        static bool calledOnDraw = false;
        static (TestDrawComponent, Entity) resultComponent;

        class CalledRenderer : OrderedRenderer<TestDrawComponent>
        {
            public override void Render(Entity entity, in TestDrawComponent testDrawComponent)
            {
                resultComponent = (testDrawComponent, entity);
                calledOnDraw = true;
            }
        }

        [Test]
        public void RenderMethodCalledOnWorldDraw()
        {
            var worldBuilder = new WorldBuilder();
            worldBuilder.RegisterDrawLayer(2);

            var renderer = worldBuilder.AddOrderedRenderer(new CalledRenderer());

            AComponent aComponent;
            CComponent cComponent;

            var testDrawComponent = new TestDrawComponent { Layer = 2 };

            var entity = worldBuilder.CreateEntity();
            worldBuilder.SetComponent(entity, aComponent);
            worldBuilder.SetComponent(entity, cComponent);
            worldBuilder.SetComponent(entity, testDrawComponent);

            var world = worldBuilder.Build();

            world.Update(0.01f);
            world.Draw();

            Assert.IsTrue(calledOnDraw);
            resultComponent.Should().BeEquivalentTo((testDrawComponent, entity));
        }

        [Reads(typeof(TestDrawComponent))]
        class DestroyerEngine : Engine
        {
            public override void Update(double dt)
            {
                foreach (var entity in ReadEntities<TestDrawComponent>())
                {
                    Destroy(entity);
                }
            }
        }
        [Test]
        public void RenderMethodNotCalledAfterDestroy()
        {
            calledOnDraw = false;

            var worldBuilder = new WorldBuilder();
            worldBuilder.RegisterDrawLayer(1);

            worldBuilder.AddEngine(new DestroyerEngine());
            var renderer = worldBuilder.AddOrderedRenderer(new CalledRenderer());

            TestDrawComponent testDrawComponent = new TestDrawComponent { Layer = 1 };

            var entity = worldBuilder.CreateEntity();
            worldBuilder.SetComponent(entity, testDrawComponent);

            var world = worldBuilder.Build();

            world.Update(0.01);
            world.Draw();

            Assert.IsFalse(calledOnDraw);
        }
    }
}
