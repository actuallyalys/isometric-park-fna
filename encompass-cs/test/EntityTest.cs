using NUnit.Framework;
using FluentAssertions;

using Encompass;
using Encompass.Exceptions;

namespace Tests
{
    public class EntityTests
    {
        [Test]
        public void Equals()
        {
            var worldBuilder = new WorldBuilder();
            var entity = worldBuilder.CreateEntity();
            var entityTwo = worldBuilder.CreateEntity();

            var copyEntity = entity;

            Assert.AreNotEqual(entity, entityTwo);
            Assert.AreEqual(entity, entity);
            Assert.IsTrue(entity == copyEntity);
        }

        [Test]
        public void EntityOverflowException()
        {
            var worldBuilder = new WorldBuilder(16);

            for (var i = 0; i < 16; i++)
            {
                worldBuilder.CreateEntity();
            }

            Assert.Throws<EntityOverflowException>(() => worldBuilder.CreateEntity());
        }
    }
}
