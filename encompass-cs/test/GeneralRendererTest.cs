﻿using NUnit.Framework;
using Encompass;

namespace Tests
{
    public static class GeneralRendererTest
    {
        struct AComponent : IComponent { }

        public class SingletonRead
        {
            static (AComponent, Entity) result;

            class TestRenderer : GeneralRenderer
            {
                public override void Render()
                {
                    ref readonly var entity = ref ReadEntity<AComponent>();
                    result = (GetComponent<AComponent>(entity), entity);
                }
            }

            [Test]
            public void SingletonComponent()
            {
                var worldBuilder = new WorldBuilder();
                worldBuilder.AddGeneralRenderer(new TestRenderer(), 1);

                AComponent aComponent;

                var entity = worldBuilder.CreateEntity();
                worldBuilder.SetComponent(entity, aComponent);

                var world = worldBuilder.Build();

                world.Update(0.01f);
                world.Draw();

                world.Update(0.01);
                world.Draw();

                Assert.That(result, Is.EqualTo((aComponent, entity)));
            }

            [Test]
            public void MultipleComponents()
            {
                var worldBuilder = new WorldBuilder();
                worldBuilder.AddGeneralRenderer(new TestRenderer(), 1);

                AComponent aComponent;
                AComponent aComponentTwo;

                var entity = worldBuilder.CreateEntity();
                worldBuilder.SetComponent(entity, aComponent);

                var entityB = worldBuilder.CreateEntity();
                worldBuilder.SetComponent(entityB, aComponentTwo);
                var world = worldBuilder.Build();

                world.Update(0.01f);
                world.Draw();

                world.Update(0.01f);
                world.Draw();

                Assert.That(result, Is.EqualTo((aComponent, entity)).Or.EqualTo((aComponentTwo, entityB)));
            }
        }
    }
}
