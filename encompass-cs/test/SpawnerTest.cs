﻿using Encompass;
using NUnit.Framework;

namespace Tests
{
    public class SpawnerTest
    {
        struct TestComponent : IComponent { }
        struct SpawnMessageA : IMessage { }

        static Entity resultEntity;

        [Sends(typeof(SpawnMessageA))]
        class MessageEmitter : Engine
        {
            public override void Update(double dt)
            {
                SendMessage(new SpawnMessageA());
            }
        }

        [WritesImmediate(typeof(TestComponent))]
        [Writes(typeof(TestComponent))]
        class TestSpawner : Spawner<SpawnMessageA>
        {
            protected override void Spawn(in SpawnMessageA message)
            {
                resultEntity = CreateEntity();
                SetComponent(resultEntity, new TestComponent());
                Assert.Pass();
            }
        }

        [Test]
        public void RunsSpawnMethodOnMessageRead()
        {
            var worldBuilder = new WorldBuilder();
            worldBuilder.AddEngine(new MessageEmitter());
            worldBuilder.AddEngine(new TestSpawner());

            var world = worldBuilder.Build();

            world.Update(0.01);
        }
    }
}
