﻿namespace Encompass
{
    /// <summary>
    /// GeneralRenderer is a Renderer which generically reads the game state in order to draw elements to the screen.
    /// GeneralRenderers have a layer specified when they are added to the World.
    /// </summary>
    public abstract class GeneralRenderer : Renderer
    {
        public abstract void Render();
    }
}
