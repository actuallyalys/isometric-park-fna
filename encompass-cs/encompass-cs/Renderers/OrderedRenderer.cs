﻿using System;

namespace Encompass
{
    /// <summary>
    /// OrdereredRenderer provides a structure for the common pattern of wishing to draw a specific DrawComponent at a specific layer.
    /// </summary>
    public abstract class OrderedRenderer<TComponent> : Renderer where TComponent : struct, IComponent, IDrawableComponent
    {
        public abstract void Render(Entity entity, in TComponent drawComponent);

        internal void InternalRender(Entity entity)
        {
            ref readonly var component = ref GetComponent<TComponent>(entity);
            Render(entity, component);
        }
    }
}
