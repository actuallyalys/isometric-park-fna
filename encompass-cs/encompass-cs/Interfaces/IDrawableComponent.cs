namespace Encompass
{
    public interface IDrawableComponent
    {
        int Layer { get; }
    }
}
