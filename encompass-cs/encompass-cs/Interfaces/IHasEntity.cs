﻿namespace Encompass
{
    public interface IHasEntity
    {
        Entity Entity { get; }
    }
}
