﻿using MoonTools.FastCollections;

namespace Encompass
{
    internal struct EntitySetQuery
    {
        private BitSet512 WithImmediateMask { get; }
        private BitSet512 WithExistingMask { get; }
        private BitSet512 WithoutImmediateMask { get; }
        private BitSet512 WithoutExistingMask { get; }
        private BitSet512 NotWithMask { get; }

        internal EntitySetQuery(BitSet512 withImmediateMask, BitSet512 withExistingMask, BitSet512 withoutImmediateMask, BitSet512 withoutExistingMask, BitSet512 notWithMask)
        {
            WithImmediateMask = withImmediateMask;
            WithExistingMask = withExistingMask;
            WithoutImmediateMask = withoutImmediateMask;
            WithoutExistingMask = withoutExistingMask;
            NotWithMask = notWithMask;
        }

        public bool CheckEntity(int entityID, ComponentBitSet componentBitSet)
        {
            var existingBits = componentBitSet.EntityBitArray(entityID);
            var existing = (WithExistingMask & existingBits) | NotWithMask;

            var existingForbidden = ~(WithoutExistingMask & existingBits);

            return (existing & existingForbidden).AllTrue();
        }

        public bool ImmediateCheckEntity(int entityID, ComponentBitSet immediateBitLookup, ComponentBitSet existingBitLookup)
        {
            var immediateBits = immediateBitLookup.EntityBitArray(entityID);
            var existingBits = existingBitLookup.EntityBitArray(entityID);

            var immediate = WithImmediateMask & immediateBits;
            var existing = WithExistingMask & existingBits;
            var withCheck = immediate | existing | NotWithMask;

            var immediateForbidden = ~(WithoutImmediateMask & immediateBits);
            var existingForbidden = ~(WithoutExistingMask & existingBits);
            var withoutCheck = immediateForbidden & existingForbidden;

            return (withCheck & withoutCheck).AllTrue();
        }
    }
}
