using System.Collections.Generic;

namespace Encompass
{
    internal class IDManager
    {
        int _nextID = 0;

        private readonly Stack<int> _availableIDs = new Stack<int>();

        public int NextID()
        {
            if (_availableIDs.Count > 0)
            {
                return _availableIDs.Pop();
            }
            else
            {
                return _nextID++;
            }
        }

        public void Free(int id)
        {
            _availableIDs.Push(id);
        }
    }
}
