﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace Encompass
{
    class UberRenderer : Renderer
    {
        private readonly IEnumerable<Type> _componentTypes;
        private Entity _entity;

        public UberRenderer(IEnumerable<Type> componentTypes)
        {
            _componentTypes = componentTypes;
        }

        public void SetEntity(Entity entity)
        {
            _entity = entity;
        }

        // can't reflect invoke on Span returns...
        public void Render()
        {
            foreach (var type in _componentTypes)
            {
                CallGenericWrappedMethod(type, "CallAllComponentMethods", null);
            }
        }

        protected void CallAllComponentMethods<TComponent>() where TComponent : struct, IComponent
        {
            ReadEntity<TComponent>();
            ReadEntities<TComponent>();
            ReadComponent<TComponent>();
            ReadComponents<TComponent>();
            GetComponent<TComponent>(_entity);
            HasComponent<TComponent>(_entity);
            SomeComponent<TComponent>();
        }

        private void CallGenericWrappedMethod(Type type, string methodName, object[] parameters)
        {
            var readComponentMethod = typeof(UberRenderer).GetMethod(methodName, BindingFlags.NonPublic | BindingFlags.Instance);
            var genericReadComponentMethod = readComponentMethod.MakeGenericMethod(type);
            genericReadComponentMethod.Invoke(this, parameters);
        }
    }
}
