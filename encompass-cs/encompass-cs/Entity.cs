﻿using System;

namespace Encompass
{
    /// <summary>
    /// An Entity is a structure composed of a unique ID and a collection of Components.
    /// An Entity may only have a single Component of any particular Type.
    /// </summary>
    public struct Entity : IEquatable<Entity>
    {
        public readonly int ID;

        internal Entity(int id)
        {
            ID = id;
        }

        public override bool Equals(object obj)
        {
            return obj is Entity entity && Equals(entity);
        }

        public bool Equals(Entity other)
        {
            return other.ID == ID;
        }

        public static bool operator ==(Entity one, Entity two)
        {
            return one.Equals(two);
        }

        public static bool operator !=(Entity one, Entity two)
        {
            return !one.Equals(two);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(ID);
        }
    }
}
