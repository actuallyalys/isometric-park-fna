﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using Encompass.Exceptions;
using MoonTools.Core.Graph;
using MoonTools.Core.Graph.Extensions;

namespace Encompass
{
    /// <summary>
    /// WorldBuilder is used to construct a World from Engines, Renderers, and an initial state of Entities, Components, and Messages.
    /// </summary>
    /// <remarks>
    /// WorldBuilder enforces certain rules about Engine structure. It is forbidden to have messages create cycles between Engines,
    /// and no Component may be written by more than one Engine.
    /// The WorldBuilder uses Engines and their Message read/emit information to determine a valid ordering of the Engines, which is given to the World.
    /// </remarks>
    public class WorldBuilder
    {
        private readonly int _entityCapacity;
        private readonly List<Engine> _engines = new List<Engine>();
        private readonly DirectedGraph<Engine, Unit> _engineGraph = GraphBuilder.DirectedGraph<Engine>();
        private readonly ComponentStore _startingExistingComponentStore;
        private readonly ComponentStore _startingUpToDateComponentStore;

        private readonly ComponentManager _componentManager;
        private readonly EntityManager _entityManager;
        private readonly MessageManager _messageManager;
        private readonly TimeManager _timeManager;
        private readonly DrawLayerManager _drawLayerManager;
        private readonly RenderManager _renderManager;
        private readonly TrackingManager _trackingManager;

        private readonly Dictionary<Type, HashSet<Engine>> _typeToReaders = new Dictionary<Type, HashSet<Engine>>();
        private readonly HashSet<Engine> _senders = new HashSet<Engine>();
        private readonly HashSet<Type> _componentTypesToPreload = new HashSet<Type>();
        private readonly HashSet<Type> _messageTypes = new HashSet<Type>();
        private readonly Dictionary<Type, int> _typeToIndex = new Dictionary<Type, int>();

        private bool _rendererRegistered = false;

        public WorldBuilder(int entityCapacity = 32768)
        {
            _entityCapacity = entityCapacity;
            _drawLayerManager = new DrawLayerManager();
            _timeManager = new TimeManager();
            _trackingManager = new TrackingManager();
            _componentManager = new ComponentManager(_drawLayerManager, _typeToIndex);
            _messageManager = new MessageManager(_timeManager);
            _entityManager = new EntityManager(_componentManager, entityCapacity);
            _renderManager = new RenderManager(_entityManager, _drawLayerManager);

            _startingExistingComponentStore = new ComponentStore(_typeToIndex);
            _startingUpToDateComponentStore = new ComponentStore(_typeToIndex);
        }

        /// <summary>
        /// Creates and returns a new empty Entity.
        /// </summary>
        public Entity CreateEntity()
        {
            return _entityManager.CreateEntity();
        }

        /// <summary>
        /// Specifies that the given Message should be sent immediately on the first World Update.
        /// </summary>
        public void SendMessage<TMessage>(in TMessage message) where TMessage : struct, IMessage
        {
            _messageManager.AddMessage(message);
        }

        /// <summary>
        /// Specifies that the given Message should be sent after the specified number of seconds after the first World Update.
        /// </summary>
        public void SendMessage<TMessage>(in TMessage message, double time) where TMessage : struct, IMessage
        {
            _messageManager.AddMessage<TMessage>(message, time);
        }

        /// <summary>
        /// Sets Component data for the specified Component Type on the specified Entity.
        /// </summary>
        public void SetComponent<TComponent>(Entity entity, in TComponent component) where TComponent : struct
        {
            RegisterComponentType<TComponent>();
            _startingExistingComponentStore.Set(entity.ID, component);
            _startingUpToDateComponentStore.Set(entity.ID, component);

            if (component is IDrawableComponent drawableComponent)
            {
                _componentManager.RegisterDrawableComponent<TComponent>(entity.ID, drawableComponent.Layer);
                _drawLayerManager.RegisterOrderedDrawable<TComponent>();
            }
        }

        internal void RegisterComponentType<TComponent>() where TComponent : struct
        {
            if (!_typeToIndex.ContainsKey(typeof(TComponent)))
            {
                _typeToIndex.Add(typeof(TComponent), _typeToIndex.Count);
                _componentTypesToPreload.Add(typeof(TComponent));
                _componentManager.RegisterComponentType<TComponent>();
                _startingExistingComponentStore.RegisterComponentType<TComponent>();
                _startingUpToDateComponentStore.RegisterComponentType<TComponent>();
            }
        }

        internal void RegisterMessageTypes(IEnumerable<Type> types)
        {
            _messageTypes.UnionWith(types);
        }

        /// <summary>
        /// Adds the specified Engine to the World.
        /// </summary>
        /// <param name="engine">An instance of an Engine.</param>
        public Engine AddEngine<TEngine>(TEngine engine) where TEngine : Engine
        {
            engine.AssignEntityManager(_entityManager);
            engine.AssignComponentManager(_componentManager);
            engine.AssignMessageManager(_messageManager);
            engine.AssignTimeManager(_timeManager);
            engine.AssignTrackingManager(_trackingManager);

            _engines.Add(engine);
            _engineGraph.AddNode(engine);

            var messageReceiveTypes = engine.ReceiveTypes;
            var messageSendTypes = engine.SendTypes;

            RegisterMessageTypes(engine.ReceiveTypes.Union(engine.SendTypes));

            foreach (var writeImmediateType in engine.WriteImmediateTypes.Intersect(engine.ReadImmediateTypes))
            {
                throw new EngineSelfCycleException("Engine {0} both writes and reads immediate Component {1}", engine.GetType().Name, writeImmediateType.Name);
            }

            foreach (var messageType in messageReceiveTypes.Intersect(messageSendTypes))
            {
                throw new EngineSelfCycleException("Engine {0} both receives and sends Message {1}", engine.GetType().Name, messageType.Name);
            }

            if (messageSendTypes.Count > 0 || engine.WriteImmediateTypes.Count > 0)
            {
                _senders.Add(engine);
            }

            foreach (var componentType in engine.QueryWithTypes.Union(engine.QueryWithoutTypes))
            {
                _trackingManager.RegisterComponentTypeToEngine(componentType, engine);
                if (engine.ReadImmediateTypes.Contains(componentType))
                {
                    _trackingManager.RegisterImmediateComponentTypeToEngine(componentType, engine);
                }
            }

            foreach (var receiveType in engine.ReceiveTypes.Union(engine.ReadImmediateTypes))
            {
                if (!_typeToReaders.ContainsKey(receiveType))
                {
                    _typeToReaders.Add(receiveType, new HashSet<Engine>());
                }

                _typeToReaders[receiveType].Add(engine);
            }

            return engine;
        }

        /// <summary>
        /// Registers a draw layer. This must be done before any Renderers are registered.
        /// </summary>
        /// <param name="layer">The draw layer to register.</param>
        public void RegisterDrawLayer(int layer)
        {
            if (_rendererRegistered)
            {
                throw new IllegalDrawLayerException("Cannot register a draw layer after a Renderer has been registered.");
            }
            _drawLayerManager.RegisterDrawLayer(layer);
        }

        /// <summary>
        /// Adds the specified OrderedRenderer to the World.
        /// </summary>
        public OrderedRenderer<TComponent> AddOrderedRenderer<TComponent>(OrderedRenderer<TComponent> renderer) where TComponent : struct, IComponent, IDrawableComponent
        {
            RegisterComponentType<TComponent>();
            renderer.AssignEntityManager(_entityManager);
            renderer.AssignComponentManager(_componentManager);
            _renderManager.RegisterOrderedRenderer<TComponent>(renderer.InternalRender);
            _rendererRegistered = true;
            return renderer;
        }

        /// <summary>
        /// Adds the specified GeneralRenderer to the World at the specified layer.
        /// Higher layer numbers draw on top of lower layer numbers.
        /// </summary>
        /// <param name="renderer">An instance of a GeneralRenderer.</param>
        /// <param name="layer">The layer at which the GeneralRenderer should render. Higher numbers draw over lower numbers.</param>
        public TRenderer AddGeneralRenderer<TRenderer>(TRenderer renderer, int layer) where TRenderer : GeneralRenderer
        {
            renderer.AssignEntityManager(_entityManager);
            renderer.AssignComponentManager(_componentManager);
            _renderManager.RegisterGeneralRendererWithLayer(renderer, layer);
            _rendererRegistered = true;
            return renderer;
        }

        private void BuildEngineGraph()
        {
            foreach (var senderEngine in _senders)
            {
                foreach (var messageType in senderEngine.SendTypes.Union(senderEngine.WriteImmediateTypes))
                {
                    if (_typeToReaders.ContainsKey(messageType))
                    {
                        foreach (var readerEngine in _typeToReaders[messageType])
                        {
                            if (senderEngine != readerEngine)
                            {
                                if (!_engineGraph.Exists(senderEngine, readerEngine))
                                {
                                    _engineGraph.AddEdge(senderEngine, readerEngine);
                                }
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Builds the World out of the state specified on the WorldBuilder.
        /// Validates and constructs an ordering of the given Engines.
        /// </summary>
        /// <returns>An instance of World.</returns>
        public World Build()
        {
            BuildEngineGraph();

            if (_engineGraph.Cyclic())
            {
                var cycles = _engineGraph.SimpleCycles();
                var errorString = "Cycle(s) found in Engines: ";
                foreach (var cycle in cycles)
                {
                    var reversed = cycle.Reverse();
                    errorString += "\n" +
                    string.Join(" -> ", reversed.Select((engine) => engine.GetType().Name)) +
                    " -> " +
                    reversed.First().GetType().Name;
                }
                throw new EngineCycleException(errorString);
            }

            var writtenComponentTypesWithoutPriority = new HashSet<Type>();
            var writtenComponentTypesWithPriority = new HashSet<Type>();
            var duplicateWritesWithoutPriority = new List<Type>();
            var duplicateWritesWithSamePriority = new List<Type>();
            var writePriorities = new Dictionary<Type, HashSet<int>>();
            var writeMessageToEngines = new Dictionary<Type, List<Engine>>();

            foreach (var engine in _engines)
            {
                if (engine.GetType().GetCustomAttribute<IgnoresTimeDilation>() != null)
                {
                    engine._usesTimeDilation = false;
                }

                var defaultWritePriorityAttribute = engine.GetType().GetCustomAttribute<DefaultWritePriority>(false);

                foreach (var writeType in engine.WriteTypes)
                {
                    int? priority = null;
                    if (engine.WritePriorities.ContainsKey(writeType))
                    {
                        priority = engine.WritePriorities[writeType];
                    }
                    else if (defaultWritePriorityAttribute != null)
                    {
                        priority = defaultWritePriorityAttribute.WritePriority;
                    }

                    if (priority.HasValue)
                    {
                        writtenComponentTypesWithPriority.Add(writeType);

                        if (!writePriorities.ContainsKey(writeType))
                        {
                            writePriorities[writeType] = new HashSet<int>();
                        }

                        if (writePriorities[writeType].Contains(priority.Value))
                        {
                            duplicateWritesWithSamePriority.Add(writeType);
                        }
                        else if (writtenComponentTypesWithoutPriority.Contains(writeType))
                        {
                            duplicateWritesWithoutPriority.Add(writeType);
                        }
                        else
                        {
                            writePriorities[writeType].Add(priority.Value);
                        }
                    }
                    else
                    {
                        if (writtenComponentTypesWithoutPriority.Contains(writeType) || writtenComponentTypesWithPriority.Contains(writeType))
                        {
                            duplicateWritesWithoutPriority.Add(writeType);
                        }
                        else
                        {
                            writtenComponentTypesWithoutPriority.Add(writeType);
                        }
                    }

                    if (!writeMessageToEngines.ContainsKey(writeType))
                    {
                        writeMessageToEngines[writeType] = new List<Engine>();
                    }

                    writeMessageToEngines[writeType].Add(engine);
                }
            }

            if (duplicateWritesWithoutPriority.Count > 0)
            {
                var errorString = "Multiple Engines write the same Component without declaring priority: ";
                foreach (var componentType in duplicateWritesWithoutPriority)
                {
                    errorString += "\n" +
                    componentType.Name + " written by: " +
                    string.Join(", ", writeMessageToEngines[componentType].Select((engine) => engine.GetType().Name));
                }
                errorString += "\nTo resolve the conflict, add priority arguments to the Writes declarations or use a DefaultWritePriority attribute.";

                throw new EngineWriteConflictException(errorString);
            }

            if (duplicateWritesWithSamePriority.Count > 0)
            {
                var errorString = "Multiple Engines write the same Component with the same priority: ";
                foreach (var componentType in duplicateWritesWithSamePriority)
                {
                    errorString += "\n" +
                    componentType.Name + " written by: " +
                    string.Join(", ", writeMessageToEngines[componentType].Select(engine => engine.GetType().Name));
                }
                errorString += "\nTo resolve the conflict, add priority arguments to the Writes declarations or use a DefaultWritePriority attribute.";

                throw new EngineWriteConflictException(errorString);
            }

            PreloadJIT(_messageTypes);

            var engineOrder = new List<Engine>();

            foreach (var engine in _engineGraph.TopologicalSort())
            {
                engineOrder.Add(engine);
                engine.BuildEntityQuery();
            }

            var world = new World(
                engineOrder,
                _entityManager,
                _componentManager,
                _trackingManager,
                _messageManager,
                _timeManager,
                _renderManager
            );

            _componentManager.SetExistingComponentStore(_startingExistingComponentStore);
            _componentManager.SetUpToDateComponentStore(_startingUpToDateComponentStore);

            _trackingManager.InitializeTracking(_entityManager.EntityIDs);

            return world;
        }

        /// <summary>
        /// This is necessary because Encompass heavily uses generic methods with value types,
        /// so the first time any code path runs the JIT gets smashed. This method warms up the runtime.
        /// It does so by grabbing all component and message types known to the WorldBuilder and
        /// executing every possible generic method that could be executed with those types.
        /// </summary>
        private void PreloadJIT(IEnumerable<Type> messageTypes)
        {
            var dummyTimeManager = new TimeManager();
            var dummyMessageManager = new MessageManager(dummyTimeManager);
            var dummyDrawLayerManager = new DrawLayerManager();
            var dummyTrackingManager = new TrackingManager();
            var dummyComponentManager = new ComponentManager(dummyDrawLayerManager, _typeToIndex);
            var dummyEntityManager = new EntityManager(dummyComponentManager, _entityCapacity);
            var dummyRenderManager = new RenderManager(dummyEntityManager, dummyDrawLayerManager);

            // doing reflection to grab all component types, because not all writes need to be declared
            foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                foreach (var componentType in assembly.GetTypes())
                {
                    if (typeof(IComponent).IsAssignableFrom(componentType) && componentType.IsValueType && !componentType.IsEnum && !componentType.IsPrimitive)
                    {
                        var method = typeof(WorldBuilder).GetMethod("RegisterComponentType", BindingFlags.NonPublic | BindingFlags.Instance);
                        var generic = method.MakeGenericMethod(componentType);
                        generic.Invoke(this, null);

                        var dummyRegisterMethod = typeof(ComponentManager).GetMethod("RegisterComponentType", BindingFlags.Public | BindingFlags.Instance);
                        var dummyGeneric = dummyRegisterMethod.MakeGenericMethod(componentType);
                        dummyGeneric.Invoke(dummyComponentManager, null);
                    }

                    if (componentType.GetInterface("IDrawableComponent") != null)
                    {
                        // register draw layer using property value
                        var instance = Activator.CreateInstance(componentType);
                        var layerPropertyInfo = componentType.GetProperty("Layer");
                        dummyDrawLayerManager.RegisterDrawLayer((int)layerPropertyInfo.GetValue(instance));

                        var drawLayerManagerRegisterMethod = typeof(DrawLayerManager).GetMethod("RegisterOrderedDrawable");
                        var drawLayerManagerRegisterGenericMethod = drawLayerManagerRegisterMethod.MakeGenericMethod(componentType);
                        drawLayerManagerRegisterGenericMethod.Invoke(dummyDrawLayerManager, null);
                    }
                }
            }

            var prepEngineOrder = new List<Engine>();

            var uberEngine = new UberEngine(_componentTypesToPreload, messageTypes);

            uberEngine.AssignEntityManager(dummyEntityManager);
            uberEngine.AssignComponentManager(dummyComponentManager);
            uberEngine.AssignMessageManager(dummyMessageManager);
            uberEngine.AssignTimeManager(dummyTimeManager);
            uberEngine.AssignTrackingManager(dummyTrackingManager);

            var uberRenderer = new UberRenderer(_componentTypesToPreload);
            uberRenderer.AssignComponentManager(dummyComponentManager);
            uberRenderer.AssignEntityManager(dummyEntityManager);

            prepEngineOrder.Add(uberEngine);

            var dummyWorld = new World(
                prepEngineOrder,
                dummyEntityManager,
                dummyComponentManager,
                dummyTrackingManager,
                dummyMessageManager,
                dummyTimeManager,
                dummyRenderManager
             );

            uberEngine.Write();
            dummyComponentManager.WriteComponents();

            dummyWorld.Update(1);

            uberEngine.Write();
            dummyComponentManager.WriteComponents();

            uberRenderer.SetEntity(uberEngine.Entity);
            uberRenderer.Render();
        }
    }
}
