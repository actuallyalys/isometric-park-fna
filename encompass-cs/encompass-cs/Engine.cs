﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using Encompass.Exceptions;
using MoonTools.FastCollections;

namespace Encompass
{
    /// <summary>
    /// Engines are the Encompass notion of an ECS System.
    /// They are responsible for reading the World state, reading messages, emitting messages, and creating or mutating Entities and Components.
    /// Engines run once per World Update.
    /// </summary>
    public abstract class Engine : IEquatable<Engine>
    {
        internal Guid _id;

        internal readonly HashSet<Type> ReadTypes = new HashSet<Type>();
        internal readonly HashSet<Type> ReadImmediateTypes = new HashSet<Type>();
        internal readonly HashSet<Type> SendTypes = new HashSet<Type>();
        internal readonly HashSet<Type> ReceiveTypes = new HashSet<Type>();
        internal readonly HashSet<Type> WriteTypes = new HashSet<Type>();
        internal readonly HashSet<Type> WriteImmediateTypes = new HashSet<Type>();
        internal readonly HashSet<Type> QueryWithTypes = new HashSet<Type>();
        internal readonly HashSet<Type> QueryWithoutTypes = new HashSet<Type>();
        internal readonly Dictionary<Type, int> WritePriorities = new Dictionary<Type, int>();
        internal readonly int DefaultWritePriority = 0;

        /// <summary>
        /// If false, the Engine will ignore time dilation.
        /// </summary>
        internal bool _usesTimeDilation = true;
        public bool TimeDilationActive { get => _usesTimeDilation && _timeManager.TimeDilationActive; }

        private EntityManager _entityManager;
        private MessageManager _messageManager;
        private ComponentManager _componentManager;
        private TimeManager _timeManager;
        private TrackingManager _trackingManager;

        private EntitySetQuery _entityQuery;

        private readonly HashSet<int> _trackedEntities = new HashSet<int>();
        protected IEnumerable<Entity> TrackedEntities
        {
            get
            {
                foreach (var entityID in _trackedEntities)
                {
                    yield return _entityManager.GetEntity(entityID);
                }
            }
        }

        private readonly HashSet<int> _newlyCreatedEntities = new HashSet<int>();

        protected Engine()
        {
            _id = Guid.NewGuid();

            var sendsAttribute = GetType().GetCustomAttribute<Sends>(false);
            if (sendsAttribute != null)
            {
                SendTypes = sendsAttribute.SendTypes;
            }

            var activatesAttribute = GetType().GetCustomAttribute<WritesImmediate>(false);
            if (activatesAttribute != null)
            {
                WriteImmediateTypes = activatesAttribute.WriteImmediateTypes;
            }

            var defaultWritePriorityAttribute = GetType().GetCustomAttribute<DefaultWritePriority>(false);

            if (defaultWritePriorityAttribute != null)
            {
                DefaultWritePriority = defaultWritePriorityAttribute.WritePriority;
            }

            foreach (var writesAttribute in GetType().GetCustomAttributes<Writes>(false))
            {
                WriteTypes.UnionWith(writesAttribute.WriteTypes);
                WritePriorities = new Dictionary<Type, int>[2] { WritePriorities, writesAttribute.Priorities }.SelectMany(dict => dict).ToDictionary(pair => pair.Key, pair => pair.Value);
            }

            var receivesAttribute = GetType().GetCustomAttribute<Receives>(false);
            if (receivesAttribute != null)
            {
                ReceiveTypes = receivesAttribute.ReceiveTypes;
            }

            var readsAttribute = GetType().GetCustomAttribute<Reads>(false);
            if (readsAttribute != null)
            {
                ReadTypes = readsAttribute.ReadTypes;
            }

            var readsImmediateAttribute = GetType().GetCustomAttribute<ReadsImmediate>(false);
            if (readsImmediateAttribute != null)
            {
                ReadImmediateTypes = readsImmediateAttribute.ReadImmediateTypes;
            }

            var queryWithAttribute = GetType().GetCustomAttribute<QueryWith>(false);
            if (queryWithAttribute != null)
            {
                QueryWithTypes = queryWithAttribute.QueryWithTypes;
            }

            foreach (var queryType in QueryWithTypes)
            {
                ReadTypes.Add(queryType);
            }

            var queryWithoutAttribute = GetType().GetCustomAttribute<QueryWithout>(false);
            if (queryWithoutAttribute != null)
            {
                QueryWithoutTypes = queryWithoutAttribute.QueryWithoutTypes;
            }

            foreach (var queryType in QueryWithoutTypes)
            {
                ReadTypes.Add(queryType);
            }
        }

        public override bool Equals(object obj)
        {
            if (obj is Engine engine)
            {
                return Equals(engine);
            }

            return false;
        }

        public bool Equals(Engine other)
        {
            return other._id == _id;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(_id);
        }

        internal void AssignEntityManager(EntityManager entityManager)
        {
            _entityManager = entityManager;
        }

        internal void AssignComponentManager(ComponentManager componentManager)
        {
            _componentManager = componentManager;
        }

        internal void AssignMessageManager(MessageManager messageManager)
        {
            _messageManager = messageManager;
        }

        internal void AssignTimeManager(TimeManager timeManager)
        {
            _timeManager = timeManager;
        }

        internal void AssignTrackingManager(TrackingManager trackingManager)
        {
            _trackingManager = trackingManager;
        }

        internal void CheckMessageRead<TMessage>() where TMessage : struct, IMessage
        {
            if (!ReceiveTypes.Contains(typeof(TMessage)))
            {
                throw new IllegalReadException("Engine {0} tried to read undeclared Message {1}", this.GetType().Name, typeof(TMessage).Name);
            }
        }

        private bool EntityCreatedThisFrame(int entityID)
        {
            return _newlyCreatedEntities.Contains(entityID);
        }

        internal void ClearNewlyCreatedEntities()
        {
            _newlyCreatedEntities.Clear();
        }

        /// <summary>
        /// Runs once per World update with the calculated delta-time.
        /// </summary>
        /// <param name="dt">The time in seconds that has elapsed since the previous frame.</param>
        public abstract void Update(double dt);

        /// <summary>
        /// Creates and returns a new empty Entity.
        /// </summary>
        protected Entity CreateEntity()
        {
            var entity = _entityManager.CreateEntity();
            _newlyCreatedEntities.Add(entity.ID);
            return entity;
        }

        /// <summary>
        /// Returns true if an Entity with the specified ID exists.
        /// </summary>
        protected bool EntityExists(in Entity entity)
        {
            return _entityManager.EntityExists(entity.ID);
        }

        /// <summary>
        /// Returns true if an Entity with the specified ID exists.
        /// </summary>
        protected bool EntityExists(int entityID)
        {
            return _entityManager.EntityExists(entityID);
        }

        /// <summary>
        /// Returns all Entities containing the specified Component type.
        /// </summary>
        protected Span<Entity> ReadEntities<TComponent>() where TComponent : struct, IComponent
        {
            var immediateRead = ReadImmediateTypes.Contains(typeof(TComponent));
            var existingRead = ReadTypes.Contains(typeof(TComponent));
            if (existingRead && immediateRead)
            {
                return _componentManager.GetExistingAndImmediateEntities<TComponent>();
            }
            else if (existingRead)
            {
                return _componentManager.GetExistingEntities<TComponent>();
            }
            else if (immediateRead)
            {
                return _componentManager.GetImmediateEntities<TComponent>();
            }
            else
            {
                throw new IllegalReadException("Engine {0} tried to read undeclared Component {1}", GetType().Name, typeof(TComponent).Name);
            }
        }

        /// <summary>
        /// Returns an Entity containing the specified Component type.
        /// </summary>
        protected ref readonly Entity ReadEntity<TComponent>() where TComponent : struct, IComponent
        {
            var immediateRead = ReadImmediateTypes.Contains(typeof(TComponent));
            var existingRead = ReadTypes.Contains(typeof(TComponent));
            if (existingRead && immediateRead)
            {
                return ref _componentManager.ExistingOrImmediateSingularEntity<TComponent>();
            }
            else if (existingRead)
            {
                return ref _componentManager.ExistingSingularEntity<TComponent>();
            }
            else if (immediateRead)
            {
                return ref _componentManager.ImmediateSingularEntity<TComponent>();
            }
            else
            {
                throw new IllegalReadException("Engine {0} tried to read undeclared Component {1}", GetType().Name, typeof(TComponent).Name);
            }
        }

        /// <summary>
        /// Returns all of the Components with the specified Component Type.
        /// </summary>
        protected Span<TComponent> ReadComponents<TComponent>() where TComponent : struct, IComponent
        {
            var immediateRead = ReadImmediateTypes.Contains(typeof(TComponent));
            var existingRead = ReadTypes.Contains(typeof(TComponent));
            if (existingRead && immediateRead)
            {
                return _componentManager.ReadExistingAndImmediateComponentsByType<TComponent>();
            }
            else if (existingRead)
            {
                return _componentManager.GetExistingComponents<TComponent>();
            }
            else if (immediateRead)
            {
                return _componentManager.ReadImmediateComponentsByType<TComponent>();
            }
            else
            {
                throw new IllegalReadException("Engine {0} tried to read undeclared Component {1}", GetType().Name, typeof(TComponent).Name);
            }
        }

        /// <summary>
        /// Returns a Component with the specified Component Type. If multiples exist, an arbitrary Component is returned.
        /// </summary>
        protected ref readonly TComponent ReadComponent<TComponent>() where TComponent : struct, IComponent
        {
            var immediateRead = ReadImmediateTypes.Contains(typeof(TComponent));
            var existingRead = ReadTypes.Contains(typeof(TComponent));
            if (existingRead && immediateRead)
            {
                return ref _componentManager.ExistingOrImmediateSingular<TComponent>();
            }
            else if (existingRead)
            {
                return ref _componentManager.ExistingSingular<TComponent>();
            }
            else if (immediateRead)
            {
                return ref _componentManager.ImmediateSingular<TComponent>();
            }
            else
            {
                throw new IllegalReadException("Engine {0} tried to read undeclared Component {1}", GetType().Name, typeof(TComponent).Name);
            }
        }

        /// <summary>
        /// Returns true if any Component with the specified Component Type exists.
        /// </summary>
        protected bool SomeComponent<TComponent>() where TComponent : struct, IComponent
        {
            var immediateRead = ReadImmediateTypes.Contains(typeof(TComponent));
            var existingRead = ReadTypes.Contains(typeof(TComponent));
            if (existingRead && immediateRead)
            {
                return _componentManager.SomeExistingOrImmediateComponent<TComponent>();
            }
            else if (existingRead)
            {
                return _componentManager.SomeExistingComponent<TComponent>();
            }
            else if (immediateRead)
            {
                return _componentManager.SomeImmediateComponent<TComponent>();
            }
            else
            {
                throw new IllegalReadException("Engine {0} tried to read undeclared Component {1}", GetType().Name, typeof(TComponent).Name);
            }
        }

        private ref TComponent GetComponentHelper<TComponent>(int entityID) where TComponent : struct, IComponent
        {
            var immediateRead = ReadImmediateTypes.Contains(typeof(TComponent));
            var existingRead = ReadTypes.Contains(typeof(TComponent));
            if (existingRead && immediateRead)
            {
                return ref _componentManager.ReadImmediateOrExistingComponentByEntityAndType<TComponent>(entityID);
            }
            else if (existingRead)
            {
                return ref _componentManager.ReadExistingComponentByEntityAndType<TComponent>(entityID);
            }
            else if (immediateRead)
            {
                return ref _componentManager.ReadImmediateComponentByEntityAndType<TComponent>(entityID);
            }
            else
            {
                throw new IllegalReadException("Engine {0} tried to read undeclared Component {1}", GetType().Name, typeof(TComponent).Name);
            }
        }

        /// <summary>
        /// Returns a Component with the specified Type that exists on the Entity.
        /// </summary>
        /// <exception cref="Encompass.Exceptions.NoComponentOfTypeOnEntityException">
        /// Thrown when the Entity does not have a Component of the specified Type
        /// </exception>
        /// <exception cref="Encompass.Exceptions.IllegalReadException">
        /// Thrown when the Engine does not declare that it reads the given Component Type.
        /// </exception>
        protected ref readonly TComponent GetComponent<TComponent>(in Entity entity) where TComponent : struct, IComponent
        {
            return ref GetComponentHelper<TComponent>(entity.ID);
        }

        /// <summary>
        /// Returns true if the Entity has a Component of the given Type.
        /// </summary>
        /// <exception cref="Encompass.Exceptions.IllegalReadException">
        /// Thrown when the Engine does not declare that is Reads the given Component Type.
        /// </exception>
        protected bool HasComponent<TComponent>(in Entity entity) where TComponent : struct, IComponent
        {
            var immediateRead = ReadImmediateTypes.Contains(typeof(TComponent));
            var existingRead = ReadTypes.Contains(typeof(TComponent));

            if (immediateRead && existingRead)
            {
                return _componentManager.HasExistingOrImmediateComponent<TComponent>(entity.ID);
            }
            else if (existingRead)
            {
                return _componentManager.HasExistingComponent<TComponent>(entity.ID);
            }
            else if (immediateRead)
            {
                return _componentManager.HasImmediateComponent<TComponent>(entity.ID);
            }
            else
            {
                throw new IllegalReadException("Engine {0} tried to read undeclared Component {1}", GetType().Name, typeof(TComponent).Name);
            }
        }

        /// <summary>
        /// Returns true if the Entity has a Component of the given Type.
        /// </summary>
        /// <exception cref="Encompass.Exceptions.IllegalReadException">
        /// Thrown when the Engine does not declare that is Reads the given Component Type.
        /// </exception>
        protected bool HasComponent(in Entity entity, Type type)
        {
            var immediateRead = ReadImmediateTypes.Contains(type);
            var existingRead = ReadTypes.Contains(type);

            if (immediateRead && existingRead)
            {
                return _componentManager.HasExistingOrImmediateComponent(entity.ID, type);
            }
            else if (existingRead)
            {
                return _componentManager.HasExistingComponent(entity.ID, type);
            }
            else if (immediateRead)
            {
                return _componentManager.HasImmediateComponent(entity.ID, type);
            }
            else
            {
                throw new IllegalReadException("Engine {0} tried to read undeclared Component {1}", GetType().Name, type.Name);
            }
        }

        /// <summary>
        /// Sets Component data for the specified Component Type on the specified Entity. If Component data for this Type already existed on the Entity, the component data is overwritten.
        /// </summary>
        /// <exception cref="Encompass.Exceptions.IllegalWriteException">
        /// Thrown when the Engine does not declare that it Writes the given Component Type.
        /// </exception>
        protected void SetComponent<TComponent>(in Entity entity, in TComponent component) where TComponent : struct, IComponent
        {
            var priority = WritePriorities.ContainsKey(typeof(TComponent)) ? WritePriorities[typeof(TComponent)] : DefaultWritePriority;

            if (!WriteTypes.Contains(typeof(TComponent)))
            {
                throw new IllegalWriteException("Engine {0} tried to update undeclared Component {1}", GetType().Name, typeof(TComponent).Name);
            }

            bool written;
            if (WriteImmediateTypes.Contains(typeof(TComponent)))
            {
                written = _componentManager.AddImmediateComponent(entity.ID, component, priority);
                if (written)
                {
                    _trackingManager.ImmediateUpdateTracking(entity.ID, typeof(TComponent));
                }
            }
            else
            {
                written = _componentManager.UpdateComponent(entity.ID, component, priority);
            }

            if (!_componentManager.HasExistingComponent<TComponent>(entity.ID))
            {
                _trackingManager.RegisterAddition(entity.ID, typeof(TComponent));
            }

            if (written && component is IDrawableComponent drawableComponent)
            {
                _componentManager.RegisterDrawableComponent<TComponent>(entity.ID, drawableComponent.Layer);
            }
        }

        /// <summary>
        /// An alternative to SetComponent that can be used for new Entities and does not require setting write priority.
        /// </summary>
        /// <exception cref="Encompass.Exceptions.IllegalWriteException">
        /// Thrown when the Engine does not declare that it Writes the given Component Type.
        /// </exception>
        protected void AddComponent<TComponent>(in Entity entity, in TComponent component) where TComponent : struct, IComponent
        {
            if (!EntityCreatedThisFrame(entity.ID))
            {
                throw new IllegalWriteException("AddComponent used on Entity that was not created in this context. Use SetComponent instead.");
            }

            if (WriteImmediateTypes.Contains(typeof(TComponent)))
            {
                _componentManager.AddImmediateComponent(entity.ID, component);
                _trackingManager.ImmediateUpdateTracking(entity.ID, typeof(TComponent));
            }
            else
            {
                _componentManager.AddComponent(entity.ID, component);
            }

            _trackingManager.RegisterAddition(entity.ID, typeof(TComponent));

            if (component is IDrawableComponent drawableComponent)
            {
                _componentManager.RegisterDrawableComponent<TComponent>(entity.ID, drawableComponent.Layer);
            }
        }

        /// <summary>
        /// Sends a Message.
        /// </summary>
        /// <exception cref="Encompass.Exceptions.IllegalSendException">
        /// Thrown when the Engine does not declare that it Sends the Message Type.
        /// </exception>
        protected void SendMessage<TMessage>(in TMessage message) where TMessage : struct, IMessage
        {
            if (!SendTypes.Contains(typeof(TMessage)))
            {
                throw new IllegalSendException("Engine {0} tried to send undeclared Message {1}", GetType().Name, typeof(TMessage).Name);
            }

            _messageManager.AddMessage(message);
        }

        /// <summary>
        /// Sends a message after the specified number of seconds, respecting time dilation.
        /// </summary>
        /// <param name="time">The time in seconds that will elapse before the message is sent.</param>
        protected void SendMessage<TMessage>(in TMessage message, double time) where TMessage : struct, IMessage
        {
            _messageManager.AddMessage(message, time);
        }

        /// <summary>
        /// Sends a message after the specified number of seconds, ignoring time dilation.
        /// </summary>
        /// <param name="time">The time in seconds that will elapse before the message is sent.</param>
        protected void SendMessageIgnoringTimeDilation<TMessage>(in TMessage message, double time) where TMessage : struct, IMessage
        {
            _messageManager.AddMessageIgnoringTimeDilation(message, time);
        }

        /// <summary>
        /// Reads all messages of the specified Type.
        /// </summary>
        /// <exception cref="Encompass.Exceptions.IllegalReadException">
        /// Thrown when the Engine does not declare that it Receives the specified Message Type.
        /// </exception>
        protected Span<TMessage> ReadMessages<TMessage>() where TMessage : struct, IMessage
        {
            CheckMessageRead<TMessage>();
            return _messageManager.GetMessagesByType<TMessage>();
        }

        /// <summary>
        /// Reads an arbitrary message of the specified Type.
        /// </summary>
        /// <exception cref="Encompass.Exceptions.IllegalReadException">
        /// Thrown when the Engine does not declare that it Receives the specified Message Type.
        /// </exception>
        protected ref readonly TMessage ReadMessage<TMessage>() where TMessage : struct, IMessage
        {
            CheckMessageRead<TMessage>();
            return ref _messageManager.First<TMessage>();
        }

        /// <summary>
        /// Returns true if a Message of the specified Type has been sent this frame.
        /// </summary>
        /// <exception cref="Encompass.Exceptions.IllegalReadException">
        /// Thrown when the Engine does not declare that it Receives the specified Message Type.
        /// </exception>
        protected bool SomeMessage<TMessage>() where TMessage : struct, IMessage
        {
            CheckMessageRead<TMessage>();
            return _messageManager.Any<TMessage>();
        }

        /// <summary>
        /// Destroys the specified Entity. This also removes all of the Components associated with the Entity.
        /// Entity destruction takes place after all the Engines have been processed by World Update.
        /// </summary>
        protected void Destroy(in Entity entity)
        {
            _entityManager.MarkForDestroy(entity.ID);
        }

        /// <summary>
        /// Destroys an arbitrary Entity containing a Component of the specified Type.
        /// Entity destruction takes place after all the Engines have been processed by World Update.
        /// </summary>
        protected void DestroyWith<TComponent>() where TComponent : struct, IComponent
        {
            Destroy(ReadEntity<TComponent>());
        }

        /// <summary>
        /// Destroys all Entities containing a Component of the specified Type.
        /// Entity destruction takes place after all the Engines have been processed by World Update.
        /// </summary>
        protected void DestroyAllWith<TComponent>() where TComponent : struct, IComponent
        {
            foreach (var entity in ReadEntities<TComponent>())
            {
                Destroy(entity);
            }
        }

        /// <summary>
        /// Removes a Component with the specified type from the given Entity.
        /// Note that the Engine must Read the Component type that is being removed.
        /// If a Component with the specified type does not exist on the Entity, returns false and does not mutate the Entity.
        /// </summary>
        protected void RemoveComponent<TComponent>(in Entity entity) where TComponent : struct, IComponent
        {
            var priority = WritePriorities.ContainsKey(typeof(TComponent)) ? WritePriorities[typeof(TComponent)] : DefaultWritePriority;

            if (!WriteTypes.Contains(typeof(TComponent)))
            {
                throw new IllegalWriteException("Engine {0} tried to remove undeclared Component {1}. Declare with Writes attribute.", GetType().Name, typeof(TComponent).Name);
            }

            if (WriteImmediateTypes.Contains(typeof(TComponent)))
            {
                if (_componentManager.RemoveImmediate<TComponent>(entity.ID, priority))
                {
                    _trackingManager.ImmediateUpdateTracking(entity.ID, typeof(TComponent));
                }
            }
            else
            {
                _componentManager.Remove<TComponent>(entity.ID, priority);
            }

            if (_componentManager.HasExistingComponent<TComponent>(entity.ID))
            {
                _trackingManager.RegisterRemoval(entity.ID, typeof(TComponent));
            }
        }

        /// <summary>
        /// Activates the Encompass time dilation system.
        /// Engines that have the IgnoresTimeDilation property will ignore all time dilation.
        /// If multiple time dilations are active they will be averaged.
        /// </summary>
        /// <param name="factor">The time dilation factor, which is multiplied by real delta time.</param>
        /// <param name="easeInTime">The time that will elapse before time is fully dilated, in real time.</param>
        /// <param name="activeTime">The length of real time that time will be fully dilated.</param>
        /// <param name="easeOutTime">The time that will elapse before time is fully undilated.</param>
        protected void ActivateTimeDilation(double factor, double easeInTime, double activeTime, double easeOutTime)
        {
            _timeManager.ActivateTimeDilation(factor, easeInTime, activeTime, easeOutTime);
        }

        /// <summary>
        /// Activates the Encompass time dilation system.
        /// Engines that have the IgnoresTimeDilation property will ignore all time dilation.
        /// If multiple time dilations are active they will be averaged.
        /// </summary>
        /// <param name="factor">The time dilation factor, which is multiplied by real delta time.</param>
        /// <param name="easeInTime">The time that will elapse before time is fully dilated, in real time.</param>
        /// <param name="easeInFunction">An easing function for the easing in of time dilation.</param>
        /// <param name="activeTime">The length of real time that time will be fully dilated.</param>
        /// <param name="easeOutTime">The time that will elapse before time is fully undilated.</param>
        protected void ActivateTimeDilation(double factor, double easeInTime, System.Func<double, double, double, double, double> easeInFunction, double activeTime, double easeOutTime)
        {
            _timeManager.ActivateTimeDilation(factor, easeInTime, easeInFunction, activeTime, easeOutTime);
        }

        /// <summary>
        /// Activates the Encompass time dilation system.
        /// Engines that have the IgnoresTimeDilation property will ignore all time dilation.
        /// If multiple time dilations are active they will be averaged.
        /// </summary>
        /// <param name="factor">The time dilation factor, which is multiplied by real delta time.</param>
        /// <param name="easeInTime">The time that will elapse before time is fully dilated, in real time.</param>
        /// <param name="activeTime">The length of real time that time will be fully dilated.</param>
        /// <param name="easeOutTime">The time that will elapse before time is fully undilated.</param>
        /// <param name="easeOutFunction">An easing function for the easing out of time dilation.</param>
        protected void ActivateTimeDilation(double factor, double easeInTime, double activeTime, double easeOutTime, System.Func<double, double, double, double, double> easeOutFunction)
        {
            _timeManager.ActivateTimeDilation(factor, easeInTime, activeTime, easeOutTime, easeOutFunction);
        }

        /// <summary>
        /// Activates the Encompass time dilation system.
        /// Engines that have the IgnoresTimeDilation property will ignore all time dilation.
        /// If multiple time dilations are active they will be averaged.
        /// </summary>
        /// <param name="factor">The time dilation factor, which is multiplied by real delta time.</param>
        /// <param name="easeInTime">The time that will elapse before time is fully dilated, in real time.</param>
        /// <param name="easeInFunction">An easing function for the easing in of time dilation.</param>
        /// <param name="activeTime">The length of real time that time will be fully dilated.</param>
        /// <param name="easeOutTime">The time that will elapse before time is fully undilated.</param>
        /// <param name="easeOutFunction">An easing function for the easing out of time dilation.</param>
        protected void ActivateTimeDilation(double factor, double easeInTime, System.Func<double, double, double, double, double> easeInFunction, double activeTime, double easeOutTime, System.Func<double, double, double, double, double> easeOutFunction)
        {
            _timeManager.ActivateTimeDilation(factor, easeInTime, easeInFunction, activeTime, easeOutTime, easeOutFunction);
        }

        /// <summary>
        /// Efficiently reads Messages of a given type that all reference the given Entity.
        /// </summary>
        /// <typeparam name="TMessage">The Message subtype.</typeparam>
        /// <param name="entity">The entity that all messages in the IEnumerable refer to.</param>
        /// <returns></returns>
        protected IEnumerable<TMessage> ReadMessagesWithEntity<TMessage>(in Entity entity) where TMessage : struct, IMessage, IHasEntity
        {
            CheckMessageRead<TMessage>();
            return _messageManager.WithEntity<TMessage>(entity.ID);
        }

        /// <summary>
        /// Efficiently reads a single Message of a given type that references a given Entity.
        /// It is recommended to use this method in conjunction with SomeMessageWithEntity to prevent errors.
        /// </summary>
        protected ref readonly TMessage ReadMessageWithEntity<TMessage>(in Entity entity) where TMessage : struct, IMessage, IHasEntity
        {
            CheckMessageRead<TMessage>();
            return ref _messageManager.WithEntitySingular<TMessage>(entity.ID);
        }

        /// <summary>
        /// Efficiently checks if any Message of a given type referencing a given Entity exists.
        /// </summary>
        protected bool SomeMessageWithEntity<TMessage>(in Entity entity) where TMessage : struct, IMessage, IHasEntity
        {
            CheckMessageRead<TMessage>();
            return _messageManager.SomeWithEntity<TMessage>(entity.ID);
        }

        internal void CheckAndUpdateTracking(int entityID)
        {
            if (_trackedEntities.Contains(entityID) && !_entityQuery.CheckEntity(entityID, _componentManager.ExistingBits))
            {
                _trackedEntities.Remove(entityID);
            }
            else if (!_trackedEntities.Contains(entityID) && _entityQuery.CheckEntity(entityID, _componentManager.ExistingBits))
            {
                _trackedEntities.Add(entityID);
            }
        }

        internal void ImmediateCheckAndUpdateTracking(int entityID)
        {
            if (_trackedEntities.Contains(entityID) && !_entityQuery.ImmediateCheckEntity(entityID, _componentManager.ImmediateBits, _componentManager.ExistingBits))
            {
                _trackedEntities.Remove(entityID);
            }
            else if (!_trackedEntities.Contains(entityID) && _entityQuery.ImmediateCheckEntity(entityID, _componentManager.ImmediateBits, _componentManager.ExistingBits))
            {
                _trackedEntities.Add(entityID);
            }
        }

        /// <summary>
        /// Returns an empty EntitySetQuery. Can be modified and iterated over to obtain Entities that fit the given criteria.
        /// </summary>
        internal void BuildEntityQuery()
        {
            var withMask = BitSet512.Zero;
            foreach (var type in QueryWithTypes)
            {
                if (!_componentManager.TypeToIndex.ContainsKey(type)) { _componentManager.TypeToIndex.Add(type, _componentManager.TypeToIndex.Count); }
                withMask = withMask.Set(_componentManager.TypeToIndex[type]);
            }

            var withoutMask = BitSet512.Zero;
            foreach (var type in QueryWithoutTypes)
            {
                if (!_componentManager.TypeToIndex.ContainsKey(type)) { _componentManager.TypeToIndex.Add(type, _componentManager.TypeToIndex.Count); }
                withoutMask = withoutMask.Set(_componentManager.TypeToIndex[type]);
            }

            var immediateMask = BitSet512.Zero;
            foreach (var type in ReadImmediateTypes)
            {
                if (!_componentManager.TypeToIndex.ContainsKey(type)) { _componentManager.TypeToIndex.Add(type, _componentManager.TypeToIndex.Count); }
                immediateMask = immediateMask.Set(_componentManager.TypeToIndex[type]);
            }

            var existingMask = BitSet512.Zero;
            foreach (var type in ReadTypes)
            {
                if (!_componentManager.TypeToIndex.ContainsKey(type)) { _componentManager.TypeToIndex.Add(type, _componentManager.TypeToIndex.Count); }
                existingMask = existingMask.Set(_componentManager.TypeToIndex[type]);
            }

            _entityQuery = new EntitySetQuery(
                withMask & immediateMask,
                withMask & existingMask,
                withoutMask & immediateMask,
                withoutMask & existingMask,
                ~withMask
            );
        }

        internal void RegisterDestroyedEntity(int entityID)
        {
            _trackedEntities.Remove(entityID);
        }
    }
}
