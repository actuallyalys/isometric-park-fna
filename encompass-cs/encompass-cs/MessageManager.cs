using System;
using System.Collections.Generic;

namespace Encompass
{
    internal class MessageManager
    {
        private readonly TimeManager _timeManager;
        private readonly MessageStore _messageStore = new MessageStore();

        public MessageManager(TimeManager timeManager)
        {
            _timeManager = timeManager;
        }

        internal void AddMessage<TMessage>(in TMessage message) where TMessage : struct, IMessage
        {
            _messageStore.AddMessage(message);
        }

        internal void AddMessage<TMessage>(in TMessage message, double time) where TMessage : struct, IMessage
        {
            _messageStore.AddMessage(message, time);
        }

        internal void AddMessageIgnoringTimeDilation<TMessage>(in TMessage message, double time) where TMessage : struct, IMessage
        {
            _messageStore.AddMessageIgnoringTimeDilation(message, time);
        }

        internal void ClearMessages()
        {
            _messageStore.ClearAll();
        }

        internal void ProcessDelayedMessages(double dt)
        {
            _messageStore.ProcessDelayedMessages(dt * _timeManager.TimeDilationFactor, dt);
        }

        internal Span<TMessage> GetMessagesByType<TMessage>() where TMessage : struct, IMessage
        {
            return _messageStore.All<TMessage>();
        }

        internal bool Any<TMessage>() where TMessage : struct, IMessage
        {
            return _messageStore.Any<TMessage>();
        }

        internal ref readonly TMessage First<TMessage>() where TMessage : struct, IMessage
        {
            return ref _messageStore.First<TMessage>();
        }

        internal IEnumerable<TMessage> WithEntity<TMessage>(int entityID) where TMessage : struct, IMessage, IHasEntity
        {
            return _messageStore.WithEntity<TMessage>(entityID);
        }

        internal ref readonly TMessage WithEntitySingular<TMessage>(int entityID) where TMessage : struct, IMessage, IHasEntity
        {
            return ref _messageStore.FirstWithEntity<TMessage>(entityID);
        }

        internal bool SomeWithEntity<TMessage>(int entityID) where TMessage : struct, IMessage, IHasEntity
        {
            return _messageStore.SomeWithEntity<TMessage>(entityID);
        }
    }
}
