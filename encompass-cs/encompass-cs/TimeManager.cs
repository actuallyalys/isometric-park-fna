using System.Collections.Generic;

namespace Encompass
{
    internal class TimeManager
    {
        private readonly List<TimeDilationData> _timeDilationDatas = new List<TimeDilationData>(32);

        private double Linear(double t, double b, double c, double d)
        {
            return (c * t / d) + b;
        }

        public double TimeDilationFactor
        {
            get
            {
                if (_timeDilationDatas.Count == 0) { return 1; }
                var average = 0.0;
                foreach (var data in _timeDilationDatas)
                {
                    average += data.Factor;
                }
                return average / _timeDilationDatas.Count;
            }
        }

        public bool TimeDilationActive
        {
            get => _timeDilationDatas.Count != 0;
        }

        public void Update(double dt)
        {
            for (var i = _timeDilationDatas.Count - 1; i >= 0; i--)
            {
                var data = _timeDilationDatas[i];

                data.ElapsedTime += dt;

                if (data.ElapsedTime > data.EaseInTime + data.ActiveTime + data.EaseOutTime)
                {
                    _timeDilationDatas.RemoveAt(i);
                }
                else
                {
                    _timeDilationDatas[i] = data;
                }
            }
        }

        public void ActivateTimeDilation(double factor, double easeInTime, double activeTime, double easeOutTime)
        {
            ActivateTimeDilation(factor, easeInTime, Linear, activeTime, easeOutTime, Linear);
        }

        public void ActivateTimeDilation(double factor, double easeInTime, System.Func<double, double, double, double, double> easeInFunction, double activeTime, double easeOutTime)
        {
            ActivateTimeDilation(factor, easeInTime, easeInFunction, activeTime, easeOutTime, Linear);
        }

        public void ActivateTimeDilation(double factor, double easeInTime, double activeTime, double easeOutTime, System.Func<double, double, double, double, double> easeOutFunction)
        {
            ActivateTimeDilation(factor, easeInTime, Linear, activeTime, easeOutTime, easeOutFunction);
        }

        public void ActivateTimeDilation(double factor, double easeInTime, System.Func<double, double, double, double, double> easeInFunction, double activeTime, double easeOutTime, System.Func<double, double, double, double, double> easeOutFunction)
        {
            _timeDilationDatas.Add(new TimeDilationData
            (
                factor,
                easeInTime,
                easeInFunction,
                activeTime,
                easeOutTime,
                easeOutFunction
            ));
        }
    }
}
