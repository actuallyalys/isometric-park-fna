﻿using System;
using System.Collections.Generic;

namespace Encompass
{
    internal class RenderManager
    {
        private readonly EntityManager _entityManager;
        private readonly DrawLayerManager _drawLayerManager;

        private readonly Dictionary<Type, Action<Entity>> _drawComponentTypeToOrderedRenderer = new Dictionary<Type, Action<Entity>>(256);

        public RenderManager(EntityManager entityManager, DrawLayerManager drawLayerManager)
        {
            _entityManager = entityManager;
            _drawLayerManager = drawLayerManager;
        }

        public void RegisterOrderedRenderer<TComponent>(Action<Entity> renderAction) where TComponent : struct
        {
            _drawComponentTypeToOrderedRenderer.Add(typeof(TComponent), renderAction);
            _drawLayerManager.RegisterOrderedDrawable<TComponent>();
        }

        public void RegisterGeneralRendererWithLayer(GeneralRenderer renderer, int layer)
        {
            _drawLayerManager.RegisterGeneralRendererWithLayer(renderer, layer);
        }

        public void Draw()
        {
            foreach (var layer in _drawLayerManager.LayerOrder)
            {
                var generalRendererSet = _drawLayerManager.GeneralRenderersByLayer(layer);

                foreach (var (entityID, componentType) in _drawLayerManager.AllInLayer(layer))
                {
                    if (_drawComponentTypeToOrderedRenderer.ContainsKey(componentType))
                    {
                        var internalRenderAction = _drawComponentTypeToOrderedRenderer[componentType];
                        internalRenderAction(_entityManager.GetEntity(entityID));
                    }
                }

                foreach (var generalRenderer in generalRendererSet)
                {
                    generalRenderer.Render();
                }
            }
        }
    }
}
