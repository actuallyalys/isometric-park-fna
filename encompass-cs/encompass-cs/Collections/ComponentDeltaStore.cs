﻿using System;
using System.Collections.Generic;

namespace Encompass
{
    internal class ComponentDeltaStore
    {
        private readonly ComponentStore _store;
        private readonly Dictionary<Type, Replayer> _replayers = new Dictionary<Type, Replayer>();
        private readonly HashSet<Replayer> _currentReplayers = new HashSet<Replayer>();

        public IEnumerable<Replayer> CurrentReplayers { get { return _currentReplayers; } }

        public ComponentDeltaStore(Dictionary<Type, int> typeToIndex)
        {
            _store = new ComponentStore(typeToIndex);
        }

        public void RegisterComponentType<TComponent>() where TComponent : struct
        {
            _store.RegisterComponentType<TComponent>();
            if (!_replayers.ContainsKey(typeof(TComponent)))
            {
                _replayers.Add(typeof(TComponent), new Replayer<TComponent>(this));
            }
        }

        public ref readonly TComponent GetComponent<TComponent>(int entityID) where TComponent : struct
        {
            return ref _store.Get<TComponent>(entityID);
        }

        public void Set<TComponent>(int entityID, in TComponent component) where TComponent : struct
        {
            _store.Set(entityID, component);
            RegisterComponentType<TComponent>();
            var replayer = _replayers[typeof(TComponent)];
            _currentReplayers.Add(replayer);
            replayer.UnMarkRemoval(entityID);
        }

        public bool Set<TComponent>(int entityID, TComponent component, int priority) where TComponent : struct
        {
            var result = _store.Set(entityID, component, priority);
            if (result)
            {
                RegisterComponentType<TComponent>();
                var replayer = _replayers[typeof(TComponent)];
                _currentReplayers.Add(replayer);
                replayer.UnMarkRemoval(entityID);
            }
            return result;
        }

        public bool Remove<TComponent>(int entityID, int priority) where TComponent : struct
        {
            var result = _store.Remove<TComponent>(entityID, priority);
            if (result)
            {
                RegisterComponentType<TComponent>();
                var replayer = _replayers[typeof(TComponent)];
                _currentReplayers.Add(replayer);
                replayer.MarkRemoval(entityID);
            }
            return result;
        }

        public void Remove(int entityID)
        {
            _store.Remove(entityID);
            foreach (var replayer in CurrentReplayers)
            {
                replayer.MarkRemoval(entityID);
            }
        }

        public Span<Entity> AllEntities<TComponent>() where TComponent : struct
        {
            return _store.AllEntities<TComponent>();
        }

        public void ClearAll()
        {
            _store.ClearAll();
            foreach (var replayer in _currentReplayers)
            {
                replayer.Clear();
            }
            _currentReplayers.Clear();
        }
    }
}
