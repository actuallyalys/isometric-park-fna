﻿using System.Collections.Generic;

namespace Encompass
{
    internal abstract class Replayer
    {
        public abstract void Replay(ComponentStore store);
        public abstract void MarkRemoval(int entityID);
        public abstract void UnMarkRemoval(int entityID);
        public abstract void Clear();
    }

    internal class Replayer<TComponent> : Replayer where TComponent : struct
    {
        private readonly ComponentDeltaStore _deltaStore;
        private readonly HashSet<int> _removals = new HashSet<int>();

        public Replayer(ComponentDeltaStore componentStore)
        {
            _deltaStore = componentStore;
        }

        public override void Replay(ComponentStore store)
        {
            foreach (ref readonly var entity in _deltaStore.AllEntities<TComponent>())
            {
                ref readonly var component = ref _deltaStore.GetComponent<TComponent>(entity.ID);
                store.Set(entity.ID, component);
            }

            foreach (var entityID in _removals)
            {
                store.ForceRemove<TComponent>(entityID);
            }
        }

        public override void Clear()
        {
            _removals.Clear();
        }

        public override void MarkRemoval(int entityID)
        {
            _removals.Add(entityID);
        }

        public override void UnMarkRemoval(int entityID)
        {
            _removals.Remove(entityID);
        }
    }
}
