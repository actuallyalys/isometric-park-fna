using MoonTools.FastCollections;
using System;
using System.Collections.Generic;

namespace Encompass
{
    internal class ComponentBitSet
    {
        private readonly Dictionary<int, BitSet512> _entities = new Dictionary<int, BitSet512>();
        private readonly Dictionary<Type, int> _typeToIndex;

        public ComponentBitSet(Dictionary<Type, int> typeToIndex)
        {
            _typeToIndex = typeToIndex;
        }

        public void Clear()
        {
            _entities.Clear();
        }

        public void AddEntity(int entityID)
        {
            _entities.Add(entityID, BitSet512.Zero);
        }

        public void Set<TComponent>(int entityID) where TComponent : struct
        {
            if (!_entities.ContainsKey(entityID)) { AddEntity(entityID); }
            _entities[entityID] = _entities[entityID].Set(_typeToIndex[typeof(TComponent)]);
        }

        public void RemoveComponent<TComponent>(int entityID) where TComponent : struct
        {
            if (_entities.ContainsKey(entityID))
            {
                _entities[entityID] = _entities[entityID].UnSet(_typeToIndex[typeof(TComponent)]);
            }
        }

        public void RemoveEntity(int entityID)
        {
            if (_entities.ContainsKey(entityID))
            {
                _entities.Remove(entityID);
            }
        }

        public BitSet512 EntityBitArray(int entityID)
        {
            return _entities.ContainsKey(entityID) ? _entities[entityID] : BitSet512.Zero;
        }
    }
}
