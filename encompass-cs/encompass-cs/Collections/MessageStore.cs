using System;
using System.Collections.Generic;
using Encompass.Exceptions;

namespace Encompass
{
    internal class MessageStore
    {
        private readonly Dictionary<Type, TypedMessageStore> _stores = new Dictionary<Type, TypedMessageStore>(512);

        private void RegisterMessageType<TMessage>() where TMessage : struct, IMessage
        {
            _stores.Add(typeof(TMessage), new TypedMessageStore<TMessage>());
        }

        private TypedMessageStore<TMessage> Lookup<TMessage>() where TMessage : struct, IMessage
        {
            if (!_stores.ContainsKey(typeof(TMessage))) { RegisterMessageType<TMessage>(); }
            return _stores[typeof(TMessage)] as TypedMessageStore<TMessage>;
        }

        public void AddMessage<TMessage>(in TMessage message) where TMessage : struct, IMessage
        {
            Lookup<TMessage>().Add(message);
        }

        public void AddMessage<TMessage>(in TMessage message, double time) where TMessage : struct, IMessage
        {
            Lookup<TMessage>().Add(message, time);
        }

        public void AddMessageIgnoringTimeDilation<TMessage>(in TMessage message, double time) where TMessage : struct, IMessage
        {
            Lookup<TMessage>().AddIgnoringTimeDilation(message, time);
        }

        public ref readonly TMessage First<TMessage>() where TMessage : struct, IMessage
        {
            if (!Any<TMessage>()) { throw new NoMessageOfTypeException("No Message of type {0} exists", typeof(TMessage).Name); }

            return ref Lookup<TMessage>().First();
        }

        public Span<TMessage> All<TMessage>() where TMessage : struct, IMessage
        {
            return Lookup<TMessage>().All();
        }

        public bool Any<TMessage>() where TMessage : struct, IMessage
        {
            return Lookup<TMessage>().Any();
        }

        public IEnumerable<TMessage> WithEntity<TMessage>(int entityID) where TMessage : struct, IMessage, IHasEntity
        {
            return Lookup<TMessage>().WithEntity(entityID);
        }

        public ref readonly TMessage FirstWithEntity<TMessage>(int entityID) where TMessage : struct, IMessage
        {
            return ref Lookup<TMessage>().FirstWithEntity(entityID);
        }

        public bool SomeWithEntity<TMessage>(int entityID) where TMessage : struct, IMessage, IHasEntity
        {
            return Lookup<TMessage>().SomeWithEntity(entityID);
        }

        public void ProcessDelayedMessages(double dilatedDelta, double realtimeDelta)
        {
            foreach (var store in _stores.Values)
            {
                store.ProcessDelayedMessages(dilatedDelta, realtimeDelta);
            }
        }

        public void ClearAll()
        {
            foreach (var store in _stores.Values)
            {
                store.Clear();
            }
        }
    }
}
