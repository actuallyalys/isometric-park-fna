using System;
using System.Collections.Generic;
using System.Linq;
using Encompass.Exceptions;

namespace Encompass
{
    [AttributeUsage(AttributeTargets.Class)]
    public class Receives : Attribute
    {
        public readonly HashSet<Type> ReceiveTypes;

        public Receives(params Type[] receiveTypes)
        {
            foreach (var receiveType in receiveTypes)
            {
                var isMessage = receiveType.GetInterfaces().Contains(typeof(IMessage));
                if (!isMessage)
                {
                    throw new IllegalSendTypeException("{0} must be a Message", receiveType.Name);
                }
            }

            ReceiveTypes = new HashSet<Type>(receiveTypes);
        }
    }
}
