using System;
using System.Collections.Generic;

namespace Encompass
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    public class Writes : Attribute
    {
        public readonly HashSet<Type> WriteTypes = new HashSet<Type>();
        public readonly Dictionary<Type, int> Priorities = new Dictionary<Type, int>();

        public Writes(params Type[] writeTypes)
        {
            foreach (var writeType in writeTypes)
            {
                WriteTypes.Add(writeType);
            }
        }

        public Writes(Type writeType, int priority)
        {
            WriteTypes.Add(writeType);
            Priorities.Add(writeType, priority);
        }
    }
}
