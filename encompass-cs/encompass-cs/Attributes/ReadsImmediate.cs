﻿using System;
using System.Collections.Generic;

namespace Encompass
{
    [AttributeUsage(AttributeTargets.Class)]
    public class ReadsImmediate : Attribute
    {
        public readonly HashSet<Type> ReadImmediateTypes = new HashSet<Type>();

        public ReadsImmediate(params Type[] readImmediateTypes)
        {
            foreach (var readImmediateType in readImmediateTypes)
            {
                ReadImmediateTypes.Add(readImmediateType);
            }
        }
    }
}
