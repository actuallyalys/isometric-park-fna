using System;

namespace Encompass
{
    [AttributeUsage(AttributeTargets.Class)]
    public class DefaultWritePriority : Attribute
    {
        public int WritePriority { get; }

        public DefaultWritePriority(int writePriority)
        {
            WritePriority = writePriority;
        }
    }
}
