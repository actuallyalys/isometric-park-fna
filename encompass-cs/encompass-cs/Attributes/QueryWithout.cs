﻿using System;
using System.Collections.Generic;

namespace Encompass
{
    [AttributeUsage(AttributeTargets.Class)]
    public class QueryWithout : Attribute
    {
        public readonly HashSet<Type> QueryWithoutTypes = new HashSet<Type>();

        public QueryWithout(params Type[] queryWithoutTypes)
        {
            foreach (var type in queryWithoutTypes)
            {
                QueryWithoutTypes.Add(type);
            }
        }
    }
}
