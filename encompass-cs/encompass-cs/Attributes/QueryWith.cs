﻿using Encompass.Exceptions;
using System;
using System.Collections.Generic;

namespace Encompass
{
    [AttributeUsage(AttributeTargets.Class)]
    public class QueryWith : Attribute
    {
        public readonly HashSet<Type> QueryWithTypes = new HashSet<Type>();

        public QueryWith(params Type[] queryWithTypes)
        {
            foreach (var queryWithType in queryWithTypes)
            {
                QueryWithTypes.Add(queryWithType);
            }
        }
    }
}
