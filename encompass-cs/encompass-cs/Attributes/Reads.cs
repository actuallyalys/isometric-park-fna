using System;
using System.Collections.Generic;

namespace Encompass
{
    [AttributeUsage(AttributeTargets.Class)]
    public class Reads : Attribute
    {
        public readonly HashSet<Type> ReadTypes = new HashSet<Type>();

        public Reads(params Type[] readTypes)
        {
            foreach (var readType in readTypes)
            {
                ReadTypes.Add(readType);
            }
        }
    }
}
