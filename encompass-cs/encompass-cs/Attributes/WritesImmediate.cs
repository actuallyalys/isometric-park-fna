﻿using System;
using System.Collections.Generic;

namespace Encompass
{
    [AttributeUsage(AttributeTargets.Class)]
    public class WritesImmediate : Attribute
    {
        public readonly HashSet<Type> WriteImmediateTypes = new HashSet<Type>();

        public WritesImmediate(params Type[] writeImmediateTypes)
        {
            foreach (var writeImmediateType in writeImmediateTypes)
            {
                WriteImmediateTypes.Add(writeImmediateType);
            }
        }
    }
}
