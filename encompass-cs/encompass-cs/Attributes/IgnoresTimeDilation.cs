using System;

namespace Encompass
{
    [AttributeUsage(AttributeTargets.Class)]
    public class IgnoresTimeDilation : Attribute { }
}
