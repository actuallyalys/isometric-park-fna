using System;
using System.Collections.Generic;
using System.Linq;
using Encompass.Exceptions;

namespace Encompass
{
    [AttributeUsage(AttributeTargets.Class)]
    public class Sends : Attribute
    {
        public readonly HashSet<Type> SendTypes;

        public Sends(params Type[] sendTypes)
        {
            foreach (var sendType in sendTypes)
            {
                var isMessage = sendType.GetInterfaces().Contains(typeof(IMessage));
                if (!isMessage)
                {
                    throw new IllegalSendTypeException("{0} must be a Message", sendType.Name);
                }
            }

            this.SendTypes = new HashSet<Type>(sendTypes);
        }
    }
}
