namespace Encompass
{
    /// <summary>
    /// Structs that implement IMessage are considered to be Messages.
    /// </summary>
    public interface IMessage { }
}
