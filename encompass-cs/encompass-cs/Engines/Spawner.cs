﻿using System.Reflection;

namespace Encompass
{
    /// <summary>
    /// A Spawner is a special type of Engine that runs a Spawn method in response to each Message it receives.
    /// Spawners are useful for organizing the building of new Entities in your game.
    /// </summary>
    public abstract class Spawner<TMessage> : Engine where TMessage : struct, IMessage
    {
        protected Spawner() : base()
        {
            var readsAttribute = GetType().GetCustomAttribute<Reads>(false);
            if (readsAttribute != null)
            {
                readsAttribute.ReadTypes.Add(typeof(TMessage));
            }

            ReceiveTypes.Add(typeof(TMessage));
        }

        public override void Update(double dt)
        {
            foreach (ref readonly var message in ReadMessages<TMessage>())
            {
                Spawn(message);
            }
        }

        protected abstract void Spawn(in TMessage message);
    }
}
