using System.Collections.Generic;

namespace Encompass
{
    /// <summary>
    /// The World is a collection of Engines, Renderers, Entities, Components, and Messages that compose the simulation.
    /// </summary>
    public class World
    {
        private readonly List<Engine> _enginesInOrder;
        private readonly EntityManager _entityManager;
        private readonly ComponentManager _componentManager;
        private readonly TrackingManager _trackingManager;
        private readonly MessageManager _messageManager;
        private readonly TimeManager _timeManager;
        private readonly RenderManager _renderManager;

        internal World(
            List<Engine> enginesInOrder,
            EntityManager entityManager,
            ComponentManager componentManager,
            TrackingManager trackingManager,
            MessageManager messageManager,
            TimeManager timeManager,
            RenderManager renderManager
        )
        {
            _enginesInOrder = enginesInOrder;
            _entityManager = entityManager;
            _componentManager = componentManager;
            _trackingManager = trackingManager;
            _messageManager = messageManager;
            _timeManager = timeManager;
            _renderManager = renderManager;
        }

        /// <summary>
        /// Drives the simulation. Should be called from your game engine's update loop.
        /// </summary>
        /// <param name="dt">The time in seconds that has passed since the previous frame.</param>
        public void Update(double dt)
        {
            _trackingManager.UpdateTracking();
            _messageManager.ProcessDelayedMessages(dt);
            _timeManager.Update(dt);

            foreach (var engine in _enginesInOrder)
            {
                if (engine._usesTimeDilation)
                {
                    engine.Update(dt * _timeManager.TimeDilationFactor);
                }
                else
                {
                    engine.Update(dt);
                }

                engine.ClearNewlyCreatedEntities();
            }

            _messageManager.ClearMessages();
            _entityManager.PruneEmptyEntities();
            _entityManager.DestroyMarkedEntities(_enginesInOrder);

            _componentManager.RemoveMarkedComponents();
            _componentManager.WriteComponents();
        }

        /// <summary>
        /// Causes the Renderers to draw.
        /// </summary>
        public void Draw()
        {
            _renderManager.Draw();
        }
    }
}
