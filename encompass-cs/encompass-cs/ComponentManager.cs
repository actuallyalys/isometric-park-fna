using System;
using System.Collections.Generic;

namespace Encompass
{
    internal class ComponentManager
    {
        private readonly DrawLayerManager _drawLayerManager;

        private readonly ComponentStore _existingComponentStore;
        private readonly ComponentStore _immediateComponentStore;
        private readonly ComponentDeltaStore _replayStore;
        private readonly ComponentStore _upToDateComponentStore;

        public Dictionary<Type, int> TypeToIndex { get; }

        private readonly HashSet<int> _entitiesMarkedForRemoval = new HashSet<int>();

        internal ComponentBitSet ImmediateBits { get { return _immediateComponentStore.ComponentBitSet; } }
        internal ComponentBitSet ExistingBits { get { return _existingComponentStore.ComponentBitSet; } }

        public ComponentManager(DrawLayerManager drawLayerManager, Dictionary<Type, int> typeToIndex)
        {
            this._drawLayerManager = drawLayerManager;
            _existingComponentStore = new ComponentStore(typeToIndex);
            _immediateComponentStore = new ComponentStore(typeToIndex);
            _replayStore = new ComponentDeltaStore(typeToIndex);
            _upToDateComponentStore = new ComponentStore(typeToIndex);
            TypeToIndex = typeToIndex;
        }

        public void RegisterComponentType<TComponent>() where TComponent : struct
        {
            _existingComponentStore.RegisterComponentType<TComponent>();
            _immediateComponentStore.RegisterComponentType<TComponent>();
            _replayStore.RegisterComponentType<TComponent>();
            _upToDateComponentStore.RegisterComponentType<TComponent>();
        }

        internal void SetExistingComponentStore(ComponentStore componentStore)
        {
            _existingComponentStore.SwapWith(componentStore);
        }

        internal void SetUpToDateComponentStore(ComponentStore componentStore)
        {
            _upToDateComponentStore.SwapWith(componentStore);
        }

        internal void RegisterDrawableComponent<TComponent>(int entityID, int layer) where TComponent : struct
        {
            _drawLayerManager.RegisterComponentWithLayer<TComponent>(entityID, layer);
        }

        internal void WriteComponents()
        {
            _existingComponentStore.UpdateUsing(_replayStore);
            _existingComponentStore.ClearAllPriorities();
            _upToDateComponentStore.ClearAllPriorities();
            _immediateComponentStore.ClearAll();
            _replayStore.ClearAll();
        }

        internal bool AddImmediateComponent<TComponent>(int entityID, in TComponent component, int priority) where TComponent : struct
        {
            if (_immediateComponentStore.Set(entityID, component, priority))
            {
                _replayStore.Set(entityID, component);
                _upToDateComponentStore.Set(entityID, component);
                return true;
            }

            return false;
        }

        internal void AddImmediateComponent<TComponent>(int entityID, in TComponent component) where TComponent : struct
        {
            _immediateComponentStore.Set(entityID, component);
            _replayStore.Set(entityID, component);
            _upToDateComponentStore.Set(entityID, component);
        }

        internal bool UpdateComponent<TComponent>(int entityID, in TComponent component, int priority) where TComponent : struct
        {
            var result = _upToDateComponentStore.Set(entityID, component, priority);
            if (result)
            {
                _replayStore.Set(entityID, component);
            }
            return result;
        }

        internal void AddComponent<TComponent>(int entityID, in TComponent component) where TComponent : struct
        {
            _upToDateComponentStore.Set(entityID, component);
            _replayStore.Set(entityID, component);
        }

        // existing or immediate reads

        internal Span<TComponent> ReadExistingAndImmediateComponentsByType<TComponent>() where TComponent : struct
        {
            return _upToDateComponentStore.All<TComponent>();
        }

        internal ref readonly TComponent ExistingOrImmediateSingular<TComponent>() where TComponent : struct
        {
            return ref _upToDateComponentStore.Singular<TComponent>();
        }

        internal Span<Entity> GetExistingAndImmediateEntities<TComponent>() where TComponent : struct
        {
            return _upToDateComponentStore.AllEntities<TComponent>();
        }

        internal ref readonly Entity ExistingOrImmediateSingularEntity<TComponent>() where TComponent : struct
        {
            return ref _upToDateComponentStore.SingularEntity<TComponent>();
        }

        internal bool SomeExistingOrImmediateComponent<TComponent>() where TComponent : struct
        {
            return _upToDateComponentStore.Any<TComponent>();
        }

        // existing reads

        internal Span<TComponent> GetExistingComponents<TComponent>() where TComponent : struct
        {
            return _existingComponentStore.All<TComponent>();
        }

        internal ref readonly TComponent ExistingSingular<TComponent>() where TComponent : struct
        {
            return ref _existingComponentStore.Singular<TComponent>();
        }

        internal Span<Entity> GetExistingEntities<TComponent>() where TComponent : struct
        {
            return _existingComponentStore.AllEntities<TComponent>();
        }

        internal ref readonly Entity ExistingSingularEntity<TComponent>() where TComponent : struct
        {
            return ref _existingComponentStore.SingularEntity<TComponent>();
        }

        internal bool SomeExistingComponent<TComponent>() where TComponent : struct
        {
            return _existingComponentStore.Any<TComponent>();
        }

        // immediate reads

        internal Span<TComponent> ReadImmediateComponentsByType<TComponent>() where TComponent : struct
        {
            return _immediateComponentStore.All<TComponent>();
        }

        internal ref readonly TComponent ImmediateSingular<TComponent>() where TComponent : struct
        {
            return ref _immediateComponentStore.Singular<TComponent>();
        }

        internal Span<Entity> GetImmediateEntities<TComponent>() where TComponent : struct
        {
            return _immediateComponentStore.AllEntities<TComponent>();
        }

        internal ref readonly Entity ImmediateSingularEntity<TComponent>() where TComponent : struct
        {
            return ref _immediateComponentStore.SingularEntity<TComponent>();
        }

        internal bool SomeImmediateComponent<TComponent>() where TComponent : struct
        {
            return _immediateComponentStore.Any<TComponent>();
        }

        // component getters

        internal ref TComponent ReadImmediateOrExistingComponentByEntityAndType<TComponent>(int entityID) where TComponent : struct
        {
            return ref _upToDateComponentStore.Get<TComponent>(entityID);
        }

        internal ref TComponent ReadExistingComponentByEntityAndType<TComponent>(int entityID) where TComponent : struct
        {
            return ref _existingComponentStore.Get<TComponent>(entityID);
        }

        internal ref TComponent ReadImmediateComponentByEntityAndType<TComponent>(int entityID) where TComponent : struct
        {
            return ref _immediateComponentStore.Get<TComponent>(entityID);
        }

        // has checkers

        internal bool HasExistingOrImmediateComponent<TComponent>(int entityID) where TComponent : struct
        {
            return _upToDateComponentStore.Has<TComponent>(entityID);
        }

        internal bool HasExistingOrImmediateComponent(int entityID, Type type)
        {
            return _upToDateComponentStore.Has(type, entityID);
        }

        internal bool HasExistingComponent<TComponent>(int entityID) where TComponent : struct
        {
            return _existingComponentStore.Has<TComponent>(entityID);
        }

        internal bool HasExistingComponent(int entityID, Type type)
        {
            return _existingComponentStore.Has(type, entityID);
        }

        internal bool HasImmediateComponent<TComponent>(int entityID) where TComponent : struct
        {
            return _immediateComponentStore.Has<TComponent>(entityID);
        }

        internal bool HasImmediateComponent(int entityID, Type type)
        {
            return _immediateComponentStore.Has(type, entityID);
        }

        internal Span<TComponent> GetComponentsByType<TComponent>() where TComponent : struct
        {
            return _existingComponentStore.All<TComponent>();
        }

        internal ref readonly TComponent GetComponentByEntityAndType<TComponent>(int entityID) where TComponent : struct
        {
            return ref _existingComponentStore.Get<TComponent>(entityID);
        }

        internal bool EntityHasComponentOfType<TComponent>(int entityID) where TComponent : struct
        {
            return _existingComponentStore.Has<TComponent>(entityID);
        }

        internal void MarkAllComponentsOnEntityForRemoval(int entityID)
        {
            _entitiesMarkedForRemoval.Add(entityID);
        }

        internal void RemoveMarkedComponents()
        {
            foreach (var entityID in _entitiesMarkedForRemoval)
            {
                _existingComponentStore.Remove(entityID);
                _immediateComponentStore.Remove(entityID);
                _replayStore.Remove(entityID);
                _upToDateComponentStore.Remove(entityID);
                _drawLayerManager.UnRegisterEntityWithLayer(entityID);
            }

            _entitiesMarkedForRemoval.Clear();
        }

        public bool RemoveImmediate<TComponent>(int entityID, int priority) where TComponent : struct
        {
            if (_immediateComponentStore.Remove<TComponent>(entityID, priority))
            {
                _replayStore.Remove<TComponent>(entityID, priority);
                _upToDateComponentStore.Remove<TComponent>(entityID, priority);
                _drawLayerManager.UnRegisterComponentWithLayer<TComponent>(entityID);
                return true;
            }
            return false;
        }

        public void Remove<TComponent>(int entityID, int priority) where TComponent : struct
        {
            if (_upToDateComponentStore.Remove<TComponent>(entityID, priority))
            {
                _replayStore.Remove<TComponent>(entityID, priority);
                _drawLayerManager.UnRegisterComponentWithLayer<TComponent>(entityID);
            }
        }

        public bool UpToDateEntityIsEmpty(int entityID)
        {
            return _upToDateComponentStore.EntityBitArray(entityID).AllFalse();
        }
    }
}
