﻿using System;
using System.Collections.Generic;

namespace Encompass
{
    public abstract class Renderer
    {
        internal EntityManager _entityManager;
        internal ComponentManager _componentManager;

        internal void AssignEntityManager(EntityManager entityManager)
        {
            _entityManager = entityManager;
        }

        internal void AssignComponentManager(ComponentManager componentManager)
        {
            _componentManager = componentManager;
        }

        protected Span<Entity> ReadEntities<TComponent>() where TComponent : struct, IComponent
        {
            return _componentManager.GetExistingEntities<TComponent>();
        }

        protected ref readonly Entity ReadEntity<TComponent>() where TComponent : struct, IComponent
        {
            return ref _componentManager.ExistingSingularEntity<TComponent>();
        }

        protected Span<TComponent> ReadComponents<TComponent>() where TComponent : struct, IComponent
        {
            return _componentManager.GetComponentsByType<TComponent>();
        }

        protected ref readonly TComponent ReadComponent<TComponent>() where TComponent : struct, IComponent
        {
            return ref _componentManager.ExistingSingular<TComponent>();
        }

        protected ref readonly TComponent GetComponent<TComponent>(Entity entity) where TComponent : struct, IComponent
        {
            return ref _componentManager.GetComponentByEntityAndType<TComponent>(entity.ID);
        }

        protected bool HasComponent<TComponent>(Entity entity) where TComponent : struct, IComponent
        {
            return _componentManager.EntityHasComponentOfType<TComponent>(entity.ID);
        }

        protected bool SomeComponent<TComponent>() where TComponent : struct, IComponent
        {
            return _componentManager.SomeExistingComponent<TComponent>();
        }
    }
}
