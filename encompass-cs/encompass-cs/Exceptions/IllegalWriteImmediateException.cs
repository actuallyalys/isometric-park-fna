﻿using System;

namespace Encompass.Exceptions
{
    public class IllegalWriteImmediateException : Exception
    {
        public IllegalWriteImmediateException(
            string format,
            params object[] args
        ) : base(string.Format(format, args)) { }
    }
}
