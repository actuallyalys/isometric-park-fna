
using System;

namespace Encompass.Exceptions
{
    public class IllegalDrawLayerException : Exception
    {
        public IllegalDrawLayerException(
            string format,
            params object[] args
        ) : base(string.Format(format, args)) { }
    }
}
