
using System;

namespace Encompass.Exceptions
{
    public class IllegalEngineAttributesException : Exception
    {
        public IllegalEngineAttributesException(
            string format,
            params object[] args
        ) : base(string.Format(format, args)) { }
    }
}
