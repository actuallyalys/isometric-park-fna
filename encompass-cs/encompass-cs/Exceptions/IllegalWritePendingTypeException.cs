using System;

namespace Encompass.Exceptions
{
    public class IllegalWriteImmediateTypeException : Exception
    {
        public IllegalWriteImmediateTypeException(
            string format,
            params object[] args
        ) : base(string.Format(format, args)) { }
    }
}
