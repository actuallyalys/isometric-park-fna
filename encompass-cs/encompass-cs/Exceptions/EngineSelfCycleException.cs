using System;

namespace Encompass.Exceptions
{
    public class EngineSelfCycleException : Exception
    {
        public EngineSelfCycleException(
            string format,
            params object[] args
        ) : base(string.Format(format, args)) { }
    }
}
