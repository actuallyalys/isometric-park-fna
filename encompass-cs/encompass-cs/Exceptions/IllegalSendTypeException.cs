using System;

namespace Encompass.Exceptions
{
    public class IllegalSendTypeException : Exception
    {
        public IllegalSendTypeException(
            string format,
            params object[] args
        ) : base(string.Format(format, args)) { }
    }
}
