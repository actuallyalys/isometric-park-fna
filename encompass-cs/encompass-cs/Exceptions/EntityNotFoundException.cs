using System;

namespace Encompass.Exceptions
{
    public class EntityNotFoundException : Exception
    {
        public EntityNotFoundException(
            string format,
            params object[] args
        ) : base(string.Format(format, args)) { }
    }
}
