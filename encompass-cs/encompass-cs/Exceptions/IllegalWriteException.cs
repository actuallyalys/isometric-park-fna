using System;

namespace Encompass.Exceptions
{
    public class IllegalWriteException : Exception
    {
        public IllegalWriteException(
            string format,
            params object[] args
        ) : base(string.Format(format, args)) { }
    }
}
