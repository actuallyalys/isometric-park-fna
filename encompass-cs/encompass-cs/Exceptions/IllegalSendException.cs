using System;

namespace Encompass.Exceptions
{
    public class IllegalSendException : Exception
    {
        public IllegalSendException(
            string format,
            params object[] args
        ) : base(string.Format(format, args)) { }
    }
}
