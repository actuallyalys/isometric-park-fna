using System;

namespace Encompass.Exceptions
{
    public class NoComponentOfTypeOnEntityException : Exception
    {
        public NoComponentOfTypeOnEntityException(
            string format,
            params object[] args
        ) : base(string.Format(format, args)) { }
    }
}
