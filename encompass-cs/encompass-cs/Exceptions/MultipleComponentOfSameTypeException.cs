using System;

namespace Encompass.Exceptions
{
    public class MultipleComponentOfSameTypeException : Exception
    {
        public MultipleComponentOfSameTypeException(
            string format,
            params object[] args
        ) : base(string.Format(format, args)) { }
    }
}
