using System;

namespace Encompass.Exceptions
{
    public class IllegalWriteTypeException : Exception
    {
        public IllegalWriteTypeException(
            string format,
            params object[] args
        ) : base(string.Format(format, args)) { }
    }
}
