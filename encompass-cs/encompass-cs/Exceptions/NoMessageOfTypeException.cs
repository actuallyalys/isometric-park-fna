using System;

namespace Encompass.Exceptions
{
    public class NoMessageOfTypeException : Exception
    {
        public NoMessageOfTypeException(
            string format,
            params object[] args
        ) : base(string.Format(format, args)) { }
    }
}
