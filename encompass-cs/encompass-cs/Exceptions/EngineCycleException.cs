
using System;

namespace Encompass.Exceptions
{
    public class EngineCycleException : Exception
    {
        public EngineCycleException(
            string format,
            params object[] args
        ) : base(string.Format(format, args)) { }
    }
}
