using System;

namespace Encompass.Exceptions
{
    public class NoComponentOfTypeException : Exception
    {
        public NoComponentOfTypeException(
            string format,
            params object[] args
        ) : base(string.Format(format, args)) { }
    }
}
