using System;

namespace Encompass.Exceptions
{
    public class EngineWriteConflictException : Exception
    {
        public EngineWriteConflictException(
            string format,
            params object[] args
        ) : base(string.Format(format, args)) { }
    }
}
