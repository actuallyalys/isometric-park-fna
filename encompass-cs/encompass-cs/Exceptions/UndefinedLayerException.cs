using System;

namespace Encompass.Exceptions
{
    public class UndefinedLayerException : Exception
    {
        public UndefinedLayerException(
            string format,
            params object[] args
        ) : base(string.Format(format, args)) { }
    }
}
