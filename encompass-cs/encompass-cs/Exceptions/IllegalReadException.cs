using System;

namespace Encompass.Exceptions
{
    public class IllegalReadException : Exception
    {
        public IllegalReadException(
            string format,
            params object[] args
        ) : base(string.Format(format, args)) { }
    }
}
