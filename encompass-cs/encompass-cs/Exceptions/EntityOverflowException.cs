using System;

namespace Encompass.Exceptions
{
    public class EntityOverflowException : Exception
    {
        public EntityOverflowException(
            string format,
            params object[] args
        ) : base(string.Format(format, args)) { }
    }
}
