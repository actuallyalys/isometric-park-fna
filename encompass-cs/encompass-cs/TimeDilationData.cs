using System;

namespace Encompass
{
    internal struct TimeDilationData
    {
        private readonly double _factor;

        public double Factor
        {
            get
            {
                if (ElapsedTime < EaseInTime)
                {
                    return EaseInFunction(ElapsedTime, 1, _factor - 1, EaseInTime);
                }
                else if (ElapsedTime < EaseInTime + ActiveTime)
                {
                    return _factor;
                }
                else if (ElapsedTime < EaseInTime + ActiveTime + EaseOutTime)
                {
                    var elapsedOutTime = ElapsedTime - EaseInTime - ActiveTime;
                    return EaseOutFunction(elapsedOutTime, _factor, 1 - _factor, EaseOutTime);
                }

                return 1;
            }
        }

        public double ElapsedTime { get; set; }
        public double EaseInTime { get; }
        public Func<double, double, double, double, double> EaseInFunction { get; }
        public double ActiveTime { get; }
        public double EaseOutTime { get; }
        public Func<double, double, double, double, double> EaseOutFunction { get; }

        public TimeDilationData(
            double factor,
            double easeInTime,
            Func<double, double, double, double, double> easeInfunction,
            double activeTime,
            double easeOutTime,
            Func<double, double, double, double, double> easeOutFunction
        )
        {
            _factor = factor;
            EaseInTime = easeInTime;
            EaseInFunction = easeInfunction;
            ActiveTime = activeTime;
            EaseOutTime = easeOutTime;
            EaseOutFunction = easeOutFunction;
            ElapsedTime = 0;
        }
    }
}
